#
#
#
from iLift.interface.lifting_tool import Lifting_Tool
from iLift.anaStruct.fem.system import SystemElements as anaStruct


__all__ = "interface"


# constants

__major__ = 0.   # for major interface/format changes
__minor__ = 25   # for minor interface/format changes
__release__ = 2  # for tweaks, bug-fixes, or development

__version__ = '%d.%d.%d' % (__major__, __minor__, __release__)

__author__ = 'Salvador Vargas-Ortega'
__license__ = 'MIT'
__author_email__ = 'svortega@gmail.com'
__maintainer_email__ = 'svortega@gmail.com'
#__url__ = 'http://iLift.readthedocs.org'
__downloadUrl__ = "http://bitbucket.org/svortega/iLift/downloads"
#

# header
print('{:}'.format(52*'-'))
print('{:}   iLift'.format(18*' '))
print('{:}   Version {:}'
      .format(14*' ',__version__))
print('{:}'.format(52*'-'))
#
