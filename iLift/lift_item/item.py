# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports
#from dataclasses import dataclass
from typing import NamedTuple, ClassVar, List, Dict

# package imports



class ItemAreas(NamedTuple):
    """
    Lifting item areas 
    """
    lateral:float
    horizontal:float
#
#
#
class LiftingItem:
    """ """
    __slots__ = ('_width', '_length', '_height', '_weight',
                  '_shape_factor', 
                 '_units', '_item_report') 
                # 'Xe','_coordinates')
    
    def __init__(self, units:ClassVar):
        """
        """
        self._units:ClassVar = units
        #self._item = ItemSize()
        #
        self._width: ClassVar = 1.50 * units.m
        self._length: ClassVar = 1.50 * units.m
        self._height: ClassVar = 1.50 * units.m
        self._weight: ClassVar = 0 * units.kg
        #self.Xe = 0 * units.m
        # The wind shape coefficients from DNV-RP-C205
        self._shape_factor:float = 1.0
        #self._coordinates:List = [self.Xe, 0 * units.m]
        #
        self._item_report: Dict = {'size':None, 'weight':None}
    #
    @property
    def weight(self) -> ClassVar:
        """
        """
        return self._weight
    @weight.setter
    def weight(self, value:ClassVar) -> None:
        """
        """
        self._weight = value
        self._item_report['weight'] = "{:1.3}".format(self._weight.convert('kilogram').value)
    #
    @property
    def length(self) -> ClassVar:
        """
        """
        return self._length
    @length.setter
    def length(self, value:ClassVar) -> None:
        """
        """
        self._length = value    
    #
    @property
    def height(self) -> ClassVar:
        """
        """
        return self._height
    @height.setter
    def height(self, value:ClassVar) -> None:
        """
        """
        self._height = value
    #
    @property
    def width(self) -> ClassVar:
        """
        """
        return self._width
    @width.setter
    def width(self, value:ClassVar) -> None:
        """
        """
        self._width = value    
    #
    #
    #@property
    #def distance_to_beam_end_1(self) -> float:
    #    """
    #    Xe : distance from end 1 of the member to the centreline of the item
    #    """
    #    return self.Xe
    #@distance_to_beam_end_1.setter
    #def distance_to_beam_end_1(self, value:float) -> None:
    #    """
    #    Xe : distance from end 1 of the member to the centreline of the item
    #    """
    #    self.Xe = value    
    #
    @property
    def area(self):
        """
        """
        #
        #   self._item_report['size'] = {'height':  "{:1.3f}".format(1),
        #                                'width': "{:1.3f}".format(1),
        #                                'shape_factor' : "{:1.3f}".format(self._shape_factor),
        #                                'area' : "{:1.3f}".format(_area.convert('metre^2').value)}            
        #
        return ItemAreas(lateral= self.width * self.height * self.shape_factor,
                         horizontal = self.length * self.height * self.shape_factor)
    #
    @property
    def shape_factor(self):
        """
        """
        return self._shape_factor
    @shape_factor.setter
    def shape_factor(self, value):
        """
        """
        self._shape_factor = value
    #
    def file_out(self):
        """
        printing lifting item details
        """
        return self._item_report
#
#
class Rigging:
    """ 
    """
    __slots__ = ('_weight', '_units',
                 '_item_number', '_item_report')
    
    def __init__(self, units):
        """
        """
        #self._width:float = 1.0
        #self._length:float = 1.0
        #self._height:float = 1.0
        #
        self._units = units
        self._weight = 0 * units.kg
        # The wind shape coefficients from DNV-RP-C205
        #self._shape_factor:float = 1.0
        #
        self._item_number = 0
        self._item_report = {}
    #
    @property
    def weight(self):
        """
        """
        return self._weight
    @weight.setter
    def weight(self, value:float):
        """
        """
        self._weight += value
        self._item_number += 1
        self._item_report[self._item_number] = value        
    #
    def file_out(self):
        """
        printing lifting item details
        """
        return self._item_report    
#
