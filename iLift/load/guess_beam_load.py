# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports

# package imports


def udl_simply_supported(UR, L, Z, Fy):
    """
    """
    return UR * (0.60 * Fy ) * (8 * Z) / L**2
#
def udl_fixed_both_ends(UR, L, Z, Fy):
    """
    """
    return UR * (0.60 * Fy ) * (24 * Z) / L**2
#
#
def udl_fixed_and_pinned(UR, L, Z, Fy):
    """
    """
    Ma = UR * (0.60 * Fy ) * (8 * Z) / L**2
    Mx = UR * (0.60 * Fy ) * (128 * Z) / (9 * L**2)
    return max(Ma, Mx)
#
def udl_cantilever(UR, L, Z, Fy):
    """
    """
    return UR * (0.60 * Fy ) * (2 * Z) / L**2
#
#
# -------------------------------------------
#
def point_simply_supported(UR, L, x, Z, Fy):
    """
    """
    a = x
    b = L - x
    return UR * (0.60 * Fy ) * (L * Z) / (a * b)
#
def point_fixed_both_ends(UR, L, x, Z, Fy):
    """
    """
    a = x
    b = L - x
    Ma = UR * (0.60 * Fy ) * (L**2 * Z) / (a * b**2)
    Mb = UR * (0.60 * Fy ) * (L**2 * Z) / (a**2 * b)
    Mx = UR * (0.60 * Fy ) * (L**3 * Z) / 2* (a**2 * b**2)
    return max(Ma, Mb, Mx)
#
#
def point_fixed_and_pinned(UR, L, x, Z, Fy):
    """
    """
    a = x
    b = L - x    
    Ma = UR * (0.60 * Fy ) * (2 * L**2 * Z) / (a * b * (L + b))
    Mx = (UR * (0.60 * Fy ) * (2 * L**3 * Z) / (a**2 * (b + 2 * L))) / b
    return max(Ma, Mx)
#
def point_cantilever(UR, L, x, Z, Fy):
    """
    """
    a = x
    b = L - x    
    return UR * 0.60 * Fy  *  Z / a
#