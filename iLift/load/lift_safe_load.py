# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports
from math import isclose, copysign
from typing import NamedTuple, ClassVar, List
#

# package imports
#

#
class SafeLoadOutput(NamedTuple):
    """
    Force & bending moments
    """
    lift_safe_weight:float
    beam_global_point_load:float
    beam_global_udl:float 
    UR_global:float
    UR_flange:float
    UR_web_bottom:float
    UR_web_top:float
#
class LiftingSafeLoad:
    
    def __init__(self, cls):
        """
        """
        self.cls = cls
        # Set beam IR to zero
        self.cls.beam.UR = 0.0
        # reset udl vertical load. Note this is a quick hack but need fixing
        self.cls.load.udl.vertical = 0 * self.cls.units.N / self.cls.units.m
        self.cls.load.udl.lateral = 0 * self.cls.units.N / self.cls.units.m
        # print('-->')
    #
    def get_local_safe_load(self, lift_weight:ClassVar, 
                            allowable_UR:float = 1.0, 
                            actual_UR:float = 0.8,
                            udl_load=None):
        """
        """
        print('**')
        print('** Start Iterations : Flange & Web Checks UR < {:1.2f}'
              .format(allowable_UR))        
        #
        if not udl_load:
            self.cls.load.udl_vertical = self.cls.beam.get_udl_based_on_UR(actual_UR)
        #
        _local_UR = actual_UR
        _load_step = lift_weight
        _global_load_out = _load_step * self.cls.units.gravity
        # find flange capacity starting from global max load
        _delta = [0]
        while True:
            # update global check with new local load
            self.cls.item.weight = _load_step
            _beam  = self.cls.beam
            _beam_forces, _beam_stress, _displacements = _beam._analysis()
            _beam.local_code_check()
            #
            # get flange
            _flanges = _beam.flange
            _flange_UR = [_local_UR]
            _flange_Leff = min([_item.properties.bottom_flange.Leff 
                                for _item in _flanges.values()])
            #
            if _flange_Leff < self.cls.beam.L:
                _flange_UR = [max(_item.code_check.bottom.bending[:2])
                              for _item in _flanges.values()]
            #
            # include von-mises 
            _vm_flange = ([_item.code_check.bottom.total.UR
                           for _item in _flanges.values()])
            #
            # get web
            _webs = _beam.web
            _web_top_UR = _flange_UR
            _web_top_Leff = min([_item.properties.top_web.Leff 
                                 for _item in _webs.values()])
            #
            if _web_top_Leff < self.cls.beam.L:
                _web_top_UR = [_item.code_check.top.total.UR
                               for _item in _webs.values()]
            #
            _web_bottom_UR = _flange_UR
            _web_bottom_Leff = min([_item.properties.bottom_web.Leff 
                                    for _item in _webs.values()])
            #
            if _web_bottom_Leff < self.cls.beam.L:
                _web_bottom_UR = [_item.code_check.bottom.total.UR
                                  for _item in _webs.values()]
            #
            _von_mises = max(_vm_flange)
            if _von_mises > 1.0:
                print('Warning Von Mises [{:1.3f}] > 1.0'.format(_von_mises))
            #
            _local_UR = max(_flange_UR + _web_bottom_UR + _web_top_UR) # + _vm_flange
            # check if local IR close to allowable
            if isclose(_local_UR, allowable_UR, abs_tol=0.01) or _local_UR < allowable_UR:
                break
            #
            # update local load
            _delta.append(round(allowable_UR / _local_UR, 4))
            _load_step = _load_step * _delta[-1]
            #if _delta[-1] == _delta[-2]:
            #    raise InterruptedError('** error: beam local load not converging')
        #
        return SafeLoadOutput(lift_safe_weight = _load_step,
                              beam_global_point_load = _global_load_out,
                              beam_global_udl = self.cls.load.udl.vertical,
                              UR_global = _local_UR, 
                              UR_flange = max(_flange_UR),
                              UR_web_bottom = max(_web_bottom_UR), 
                              UR_web_top = max(_web_top_UR))
    #
    def _global_code_check(self, load):
        """
        """
        print('=======> Beam Global Check')
        self.cls.item.weight = load
        beam_forces, beam_stress, displacements = self.cls.beam._analysis()
        self.cls.beam._get_code_check(beam_forces, beam_stress, displacements)
        _total_UR = max([_item.code_check.total.UR
                         for _item in self.cls.beam.elements.values()])
        return _total_UR
    #
    def get_safe_lift_load(self, allowable_UR:float = 1.0, 
                           actual_UR:float = 0.8): 
        """
        """
        print('*** Start Safe Load Calculation ***')
        print('***********************************')
        print('** Intial Guess Load')
        #
        # Find udl that product of current UR
        self.cls.load.udl.vertical = self.cls.beam.get_udl_based_on_UR(actual_UR)
        #self.cls.load.udl.vertical /= self.cls.factors.DAF.vertical
        # Guess vertical load product of the remaining beam capacity
        _remain_UR = round(allowable_UR - actual_UR, 3)
        _load_step = self.cls.beam.get_vertical_load_based_on_UR(_remain_UR)
        # convert load to mass
        _load_step /= (self.cls.units.gravity * self.cls.factors.DAF.vertical)
        #
        # set initial values
        print('**')
        print('** Start Iterations : Beam Global Checks UR < {:1.2f}'
              .format(allowable_UR))        
        #
        #self.cls.beam.model.nodes = self.cls.beam.get_beam_nodes()
        #
        _power = 2
        _delta = [0]
        while True:
            #self.cls.item.weight = _load_step
            # global check
            #try:
            _total_UR = self._global_code_check(_load_step)
            #except Warning as exception:
            #    print(exception)
            #    print('** beam length exceed slenderness limit')
            #    return SafeLoadOutput(lift_safe_weight = _load_step * 0,
            #                          beam_global_point_load = _load_step * 0,
            #                          beam_global_udl = _load_step * 0,
            #                          UR_global = 1.0, 
            #                          UR_flange = 1.0,
            #                          UR_web_bottom = 1.0, 
            #                          UR_web_top = 1.0)
            #
            # check if global IR close to allowable
            if isclose(_total_UR, allowable_UR, abs_tol=0.01):
                _global_load_out = _load_step * self.cls.units.gravity
                print('** Global Lifting Load = {:1.2f} kN'
                      .format(_global_load_out.convert('kilonewton').value))
                #
                _results_local = self.get_local_safe_load(lift_weight = _load_step, 
                                                          allowable_UR  = allowable_UR, 
                                                          actual_UR = actual_UR,
                                                          udl_load = self.cls.load.udl.vertical)
                print('**')
                print('** Calculate Final Global Beam Check')
                _total_UR = self._global_code_check(_results_local.lift_safe_weight)
                #
                load_out = self.cls.item.weight * self.cls.units.gravity
                print('**')
                _final_UR = max(_total_UR, _results_local.UR_global)
                _diff_UR = _final_UR - actual_UR
                print('** Final Safe Lifting Load (UR:{:1.2f}) = {:1.2f} kN [{:1.3f} T]'
                      .format(_diff_UR, load_out.convert('kilonewton').value, 
                              self.cls.item.weight.convert('megagram').value))
                print('** Estimated UDL Beam Load (UR:{:1.2f}) = {:1.2f} kN/m'
                      .format(actual_UR,
                              self.cls.load.udl.vertical.convert('kilonewton/metre').value))
                print('** UR Summary  Global:{:1.2f}, Local:{:1.2f}'
                      .format(_total_UR, _results_local.UR_global))             
                #
                break
            #
            # update global load
            _delta.append(round(allowable_UR / _total_UR**_power, 4))
            #print('++++> delta ',_delta[-1] - _delta[-2])
            if _delta[-1] == _delta[-2]:
                print('**')
                print('** error: beam lifting load not converging')
                print('**')
                return SafeLoadOutput(lift_safe_weight = _load_step * 0,
                                      beam_global_point_load = _load_step * 0,
                                      beam_global_udl = self.cls.load.udl.vertical,
                                      UR_global = 1.0, 
                                      UR_flange = 1.0,
                                      UR_web_bottom = 1.0, 
                                      UR_web_top = 1.0)
            elif (_delta[-1] - _delta[-2]) < 0.0:
                _power = 1
            #
            _load_step = _load_step * _delta[-1]
        #
        # reset beam UR 
        self.cls.beam.UR = _total_UR
        return SafeLoadOutput(lift_safe_weight = _results_local.lift_safe_weight,
                              beam_global_point_load = _global_load_out,
                              beam_global_udl = self.cls.load.udl.vertical,
                              UR_global = _total_UR, 
                              UR_flange = _results_local.UR_flange,
                              UR_web_bottom = _results_local.UR_web_bottom, 
                              UR_web_top = _results_local.UR_web_top)
    #
    #
    #
    def get_global_safe_load(self, allowable_UR:float = 1.0, 
                             actual_UR:float = 0.8, 
                             lift_weight = None, udl_load=None):
        """
        """
        #
        if not udl_load:
            self.cls.load.udl_vertical = self.cls.beam.get_udl_based_on_UR(actual_UR)
        #
        # Guess vertical load product of the remaining beam capacity
        if lift_weight:
            _global_load_out = lift_weight
            _global_load_out =  lift_weight / self.cls.units.gravity
            print('** Estimated Vertical Load = {: 1.2f} kN'
                  .format(_global_load_out.convert('kilonewton').value))            
        else:
            _remain_UR = round(1 - actual_UR, 3)
            lift_weight = self.cls.beam.get_vertical_load_based_on_UR(_remain_UR)
            _global_load_out =  lift_weight / self.cls.units.gravity
        #
        # convert load to mass
        _load_step = lift_weight
        # set initial values
        print('**')
        print('** Start Iterations : Beam Global Checks UR < {:1.2f}'
              .format(allowable_UR))        
        #
        while True:
            self.cls.item.weight = _load_step
            # global check
            self.cls.global_check()
            _total_UR = max([_item.code_check.UR
                             for _item in self.cls.beam.elements.values()])
            # check if global IR close to allowable
            if isclose(_total_UR, allowable_UR, abs_tol=0.01):
                break
            # update global load
            _delta = round(allowable_UR / _total_UR, 3)
            _load_step = _load_step * _delta
        #
        return SafeLoadOutput(lift_safe_weight = _load_step,
                              beam_global_point_load = _global_load_out,
                              beam_global_udl = self.cls.load.udl.vertical,
                              UR_global = _total_UR, 
                              UR_flange = None,
                              UR_web_bottom = None, 
                              UR_web_top = None)
    #    
    def get_safe_lift_load_beam_length(self, allowable_UR:float = 1.0, 
                                       actual_UR:float = 0.8, 
                                       length_start = 1, length_end=10, step=1):
        """
        """
        #
        lift_load, udl_load = self._guess_lift_load(allowable_UR, actual_UR)
        #
        # convert load to mass
        #_load_step = lift_weight
        # set initial values
        print('**')
        print('** Start Iterations : Beam Global Checks UR < {:1.2f}'
              .format(allowable_UR))
        #
        _results_global = self.get_local_safe_load(allowable_UR = allowable_UR, 
                                                   actual_UR = actual_UR,
                                                   lift_weight = lift_load,
                                                   udl_load = udl_load)
    #
    def _guess_lift_load(self, allowable_UR, actual_UR):
        """
        """
        print('** Intial Guess Load')
        # Find udl that product of current UR
        udl_vertical = self.cls.beam.get_udl_based_on_UR(actual_UR)
        #
        _remain_UR = round(allowable_UR - actual_UR, 3)
        _point_vertical = self.cls.beam.get_vertical_load_based_on_UR(_remain_UR)
        #
        return _point_vertical, udl_vertical
    #
#
#
#