# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports
import math
from typing import NamedTuple, ClassVar, List

# package imports
# from iLift.beam.assessment.formulas.analysis import BeamFormulas
from iLift.load.process.actions import Actions
from iLift.load.process.factors import (FactorItems, 
                                        DAFFactors)


#
class Factors:
    """ """
    __slots__ = ('_daf', '_item_report')
    
    def __init__(self, units):
        """
        """
        self._daf = DAFFactors()
        self._item_report = {'daf':None}
    #
    @property
    def DAF(self):
        """
        """
        return self._daf
    #
    def file_out(self):
        """
        printing lifting item details
        """
        #daf_factor = self._daf.file_out()
        self._item_report['daf'] = self._daf.file_out()
        return self._item_report
#
#
#
class LoadCases(NamedTuple):
    """
    Global load cases
    """
    inertia:float
    from_angle:float
    from_wind:float
    factor:float
    total:float
#
#
class Loading:
    __slots__ = ('udl', '_point_load', '_vessel',
                 '_rigging', '_lifting_item', '_cranes',
                 '_units', '_metocean', '_lift_angle',
                 '_item_report', '_factors',)
    
    def __init__(self, item, rigging, cranes, factors, metocean, vessel, units):
        """
        """
        self._units = units
        #
        self._factors = factors
        self._rigging = rigging
        self._lifting_item = item
        self._cranes = cranes
        self._metocean = metocean
        self._vessel = vessel
        #
        self._lift_angle = FactorItems(lateral = 15 * units.deg,  
                                       horizontal = 15 * units.deg,
                                       vertical = 15 * units.deg)
        #
        #self._lift = FactorItems(lateral = 0 * units.N,
        #                         horizontal = 0 * units.N,
        #                         vertical = 0 * units.N)
        #
        #
        self.udl = FactorItems(lateral = 0 * units.N / units.m,
                               horizontal = 0 * units.N / units.m,
                               vertical = 0 * units.N / units.m)
        #
        #
        #  ----- element Forces -----
        self._point_load = Actions(self._units)
        #
        #
        self._item_report = {'lifting_angle': [],
                             'uniform_distributed': [],
                             'beam' : {'selfweight' : 0 * units.N / units.m, 
                                       'wind_pressure': 0 * units.N / units.m}, 
                             'lift_load': {'lateral' : None,
                                           'horizontal' : None,
                                           'vertical' : None}}
    #
    @property
    def lift_angle(self):
        """
        """
        return self._lift_angle
    #
    @property
    def lift_load(self):
        """
        """
        return FactorItems(lateral = self._lift_lateral,
                           horizontal = self._lift_horizontal,
                           vertical = self._lift_vertical)
        #return self._lift
    #
    @property
    def uniform_distributed(self):
        """
        udl : vertical and lateral (load/length-unit)
        """
        return self.udl
    
    #
    @property
    def point_load(self):
        """
        """
        return self._point_load
    #
    #
    #
    def beam_wind_pressure(self, beam_section):
        """
        """
        _wind = self._metocean.wind
        _udl = (_wind.pressure * beam_section.height 
                * math.sin(_wind.direction.value))
        #
        #DAF_lateral = max(1, self._factors.DAF.lateral)
        self._item_report['beam']['wind_pressure'] = _udl #* DAF_lateral
        return _udl #* DAF_lateral
    #
    #
    def beam_selfweight(self, beam_section):
        """
        """
        #DAF_vertical = max(1, self._factors.DAF.vertical)
        _udl = (beam_section.section_mass #* DAF_vertical *
                * (self._units.gravity + self._vessel.acceleration.vertical))
        #self.udl.vertical += _udl
        self._item_report['beam']['selfweight'] = _udl
        return _udl
    #
    @property
    def wind_load(self):
        """
        return wind load on lifting item
        """
        # Wind load calculation
        #_area = self._lifting_item.area * self._lifting_item.shape_factor
        #_wind = self.wind.pressure * _area
        #self._item_report['wind'] = [_area, self.wind.pressure, _wind]
        #return _wind
        #print('wind')
        _wind = self._metocean.wind
        return FactorItems(lateral= (self._lifting_item.area.lateral 
                                     * _wind.pressure * math.sin(_wind.direction.value)),  
                           horizontal= (self._lifting_item.area.horizontal 
                                        * _wind.pressure * math.cos(_wind.direction.value)),
                           vertical= self._lifting_item.area.lateral * _wind.pressure * 0)
    #
    @property
    def _lift_vertical(self):
        """
        """
        DAF_vertical = max(1, self._factors.DAF.vertical)
        # weight always positive (downwards) when calculating vertical load
        _case_1 = ((abs(self._rigging.weight) + abs(self._lifting_item.weight) * DAF_vertical) 
                  * (self._units.gravity + self._vessel.acceleration.vertical))
        #
        self._item_report['lift_load']['vertical'] = LoadCases(inertia = _case_1, 
                                                               from_angle = _case_1 * 0, 
                                                               from_wind = self.wind_load.vertical,
                                                               factor = DAF_vertical,
                                                               total = _case_1)
        
        return _case_1 
    
    @property
    def _lift_lateral(self):
        """
        """
        _vertical_load = (abs(self._rigging.weight) + abs(self._lifting_item.weight)) 
        # Case 1 - Vessel lateral acceleration
        _case_1 = (_vertical_load * self._vessel.acceleration.lateral)
        #
        # Case 2 - Vertical load lift angle
        _case_2 = _vertical_load * (self._units.gravity + self._vessel.acceleration.vertical)
        _case_2 *= math.sin(self.lift_angle.lateral.value)
        # 
        _total_lat = _case_1
        if _case_1 < _case_2:
            _total_lat = _case_2
        
        DAF_lateral = max(1, self._factors.DAF.lateral)
        _total_lat *= DAF_lateral
        # add wind load
        # FIXME: wind load sign 
        _total_lat += self.wind_load.lateral
        #
        self._item_report['lift_load']['lateral'] = LoadCases(inertia = _case_1, 
                                                              from_angle = _case_2, 
                                                              from_wind = self.wind_load.lateral,
                                                              factor = DAF_lateral,
                                                              total = _total_lat)
        #
        return _total_lat
    
    @property
    def _lift_horizontal(self):             # @hami2230 - updated to include two lift angles
        """
        """
        _vertical_load = (abs(self._rigging.weight) + abs(self._lifting_item.weight)) 
        # Case 1 - Vessel horizontal acceleration
        _case_1 = (_vertical_load * self._vessel.acceleration.horizontal)        
        #
        # Case 2 - Vertical load lift angle
        _case_2 = _vertical_load * (self._units.gravity + self._vessel.acceleration.vertical)
        _case_2 *= math.sin(self.lift_angle.horizontal.value) 
        # 
        _total_ax = _case_1
        if _case_1 < _case_2:
            _total_ax = _case_2
        
        DAF_horizontal = max(1, self._factors.DAF.horizontal)
        _total_ax *= DAF_horizontal
        # add wind load
        # FIXME: wind load sign 
        _total_ax += self.wind_load.horizontal
        #
        self._item_report['lift_load']['horizontal'] = LoadCases(inertia = _case_1, 
                                                                 from_angle = _case_2, 
                                                                 from_wind = self.wind_load.horizontal,
                                                                 factor = DAF_horizontal,
                                                                 total = _total_ax)
        #
        return _total_ax
    
    #
    #
    @property
    def file_out(self):
        """
        printing lifting item details
        """
        return self._item_report
    
    #
    #
    def update_crane_load(self, crane, #design_actions, 
                           Isection, load_distribution):    # @hami2230 - updated to include two lift angles
        """
        update crane load if missing
        load_distribution: load / number of cranes
        """
        # global load
        _lifting_load = self.lift_load
        _load_lateral = _lifting_load.lateral * load_distribution
        _load_vertical = _lifting_load.vertical * load_distribution
        _load_axial = _lifting_load.horizontal * load_distribution
        _flange_factor = max(load_distribution , (1 - load_distribution))
        # torsion
        P_rem = round(2 * crane.load_split - 1.0, 3)                      # @hami2230 - v0.5 update
        Mbx = (_load_lateral * (crane.eccentricity + Isection.Zc)         # @hami2230 - set to zero until torsion is added 
               + P_rem * _flange_factor * _load_vertical * Isection.bf * 0.5)
        #        
        # in-plane bending (i.e horizontal cross-hauling)
        Mby = _load_axial * (crane.eccentricity + Isection.Zc)       # @hami2230 - set to zero until torsion is added
        Mbz = Mby * 0
        #
        #DAF_vertical = max(1, self._factors.DAF.vertical)
        _load_vertical += (crane.weight   # * DAF_vertical 
                           * (self._units.gravity + self._vessel.acceleration.vertical))
        #
        # update crane loading 
        crane.load = [_load_axial, _load_lateral, _load_vertical,
                      Mbx, Mby, Mbz]
    #
    def update_point_load(self):
        """
        """
        #TODO: maxDAF is needed? 
        _lifting_load = self.lift_load
        DAF_lateral = max(1, self._factors.DAF.lateral)
        DAF_horizontal = max(1, self._factors.DAF.horizontal)
        DAF_vertical = max(1, self._factors.DAF.vertical)
        DAF_max = max(DAF_horizontal, DAF_lateral, DAF_vertical)
        return [self._point_load.Fx * DAF_max + _lifting_load.horizontal, 
                self._point_load.Fy * DAF_max + _lifting_load.lateral, 
                self._point_load.Fz * DAF_max + _lifting_load.vertical,
                self._point_load.Mx * DAF_max, 
                self._point_load.My * DAF_max, 
                self._point_load.Mz * DAF_max]
    #
    # 