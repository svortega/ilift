# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports
#from typing import NamedTuple, ClassVar, List

# package imports


#
class FactorItems:
    """ """
    __slots__ = ('_vertical', '_lateral', 
                 '_horizontal','_item_report')
    
    def __init__(self, vertical, lateral, horizontal):
        """
        """
        self._vertical = vertical
        self._lateral = lateral
        self._horizontal = horizontal
        self._item_report = {'vertical': vertical,
                             'lateral': lateral,
                             'horizontal': horizontal}
    #
    @property
    def horizontal(self):
        """
        """
        return self._horizontal
    @horizontal.setter
    def horizontal(self, value):
        """
        """
        self._horizontal = value
        self._item_report['horizontal'] = value
    #
    @property
    def longitudinal(self):
        """
        """
        return self._horizontal
    @longitudinal.setter
    def longitudinal(self, value):
        """
        """
        self._horizontal = value
        self._item_report['horizontal'] = value    
    #
    #
    @property
    def vertical(self):
        """
        """
        return self._vertical
    @vertical.setter
    def vertical(self, value):
        """
        """
        self._vertical = value
        self._item_report['vertical'] = value
    #
    #
    @property
    def lateral(self):
        """
        """
        return self._lateral
    @lateral.setter
    def lateral(self, value):
        """
        """
        self._lateral = value
        self._item_report['lateral'] = value
    #
    @property
    def transverse(self):
        """
        """
        return self._lateral
    @transverse.setter
    def transverse(self, value):
        """
        """
        self._lateral = value
        self._item_report['lateral'] = value    
    #
    @property
    def file_out(self):
        """
        printing lifting item details
        """
        return self._item_report     
#
class DAFFactors(FactorItems):
    """ """
    #__slots__ = ('_vertical', '_lateral')
    
    def __init__(self):
        """
        """
        # Dinamic Amplification Factor
        #self._vertical = 1.0
        #self._lateral = 1.0
        super().__init__(vertical = 1.0, lateral = 1.0,
                         horizontal = 1.0)
#
#