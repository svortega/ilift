# 
# Copyright (c) 2019 iLift
#

# Python stdlib imports
#from dataclasses import dataclass
from typing import NamedTuple, Tuple, List, ClassVar

# package imports
from iLift.process.io_module.text import match_line

#
def find_force_item(word_in):
    """
    """
    _key = {"Fx": r"\b((f|axial)(\_)?(x)?)\b",
            "Fy": r"\b((f|v|shear)(\_)?(y|i(n)?(\_)?p(lane)?))\b",
            "Fz": r"\b((f|v|shear)(\_)?(z|o(ut)?(\_)?p(lane)?))\b",
            #
            "Mx": r"\b((bending(\_)?)?m(oment)?(\_)?(x|t(orsion(al)?)?))\b",
            "My": r"\b((bending(\_)?)?m(oment)?(\_)?(y|i(n)?(\_)?p(lane)?))\b",
            "Mz": r"\b((bending(\_)?)?m(oment)?(\_)?(z|o(ut)?(\_)?p(lane)?))\b"}
    
    _match = match_line(word_in, _key)
    if not _match:
        raise IOError('  ** item {:} not recognized'.format(word_in))
    return _match
#
# FIXME: badly done to assign forces
def assign_force_item(self, mat_items):
    """
    Assign material's input data to a dictionary by name/number
    """
    #
    #if not self.actions:
    #    self.actions = Actions(Fx = 0 * self._units.N, 
    #                           Fy = 0 * self._units.N, 
    #                           Fz = 0 * self._units.N, 
    #                           Mx = 0 * self._units.N * self._units.m, 
    #                           My = 0 * self._units.N * self._units.m, 
    #                           Mz = 0 * self._units.N * self._units.m)
    #
    for key, value in mat_items.items():
        _item = find_force_item(key)
        #
        if _item == 'Fx':
            self.actions.Fx = value
        elif _item == 'Fy':
            self.actions.Fy = value        
        elif _item == 'Fz':
            self.actions.Fz = value
        #
        elif _item == 'Mx':
            self.actions.Mx = value        
        elif _item == 'My':
            self.actions.My = value
        elif _item == 'Mz':
            self.actions.Mz = value
        else:
            raise IOError('error Force item : {:} not recognized'
                          .format(key))
    #
    #self.actions = Actions(_P, _Vy, _Vx, _Mt, _Mx, _My)
    #print('ok')
#
#
#@dataclass
class Actions:
    """
    Force & bending moments
    """
    __slots__ = ('Fx', 'Fy', 'Fz', 'Mx', 'My', 'Mz',
                 'Xe', '_units')
    
    def __init__(self, units):
        """
        """
        self._units = units
        self.Fx: ClassVar = 0 * self._units.N
        self.Fy: ClassVar = 0 * self._units.N
        self.Fz: ClassVar = 0 * self._units.N
        self.Mx: ClassVar = 0 * self._units.N * self._units.m
        self.My: ClassVar = 0 * self._units.N * self._units.m
        self.Mz: ClassVar = 0 * self._units.N * self._units.m
        #
        # ----- Load offsets -----
        self.Xe = 0 * units.m # load offset from support 1
    
    #
    #def __setitem__(self, **kwargs):
    #    """
    #    """
    #    print('here')
    def member_actions(self, **kwargs):
        """
        """
        assign_force_item(self, kwargs)
    #
    @property
    def distance_to_beam_end_1(self) -> ClassVar:
        """
        Xe : distance from end 1 of the member to the action load
        """
        return self.Xe
    @distance_to_beam_end_1.setter
    def distance_to_beam_end_1(self, value:ClassVar) -> None:
        """
        Xe : distance from end 1 of the member to the action load
        """
        self.Xe = value    
#