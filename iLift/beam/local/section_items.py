# 
# Copyright (c) 2019 iLift
#

# Python stdlib imports
#import math
from typing import NamedTuple, Tuple

# package imports
from iLift.beam.local.process.web import WebBottom, WebTop, WebSections
from iLift.beam.local.process.flanges import FlangeSections
from iLift.beam.local.clamp.flange import ClampFlangeBottom, ClampFlangeTop
from iLift.beam.local.trolley.flange import TrolleyFlangeBottom, TrolleyFlangeTop

#
#
class LocalElement(NamedTuple):
    """
    """
    actions:Tuple
    stress:Tuple
    code_check:Tuple
    properties:Tuple
#
class FlangeItems():
    """ """

    def __init__(self, cls_beam):
        """
        """
        self._beam = cls_beam
        self.bottom_flange = None
        self.top_flange = None
        self._crane = None
    #
    def check(self):
        """
        """
        #print('')
        #print('--------------------------------------')
        #print(' Check Flange in Local Bending')
        #print('')
        print('+++++++> Local Check Flange')
        #
        if not self._crane:
            raise IOError('Crane data not provided')
        #
        return FlangeSections(top=self.top_flange.check(self._crane),
                              bottom=self.bottom_flange.check(self._crane))
    #    
    def crane(self, item):
        """
        """
        self._crane = item
        #
        if 'clamp' in self._crane._type :
            self.bottom_flange = ClampFlangeBottom(self._beam)
            self.top_flange = ClampFlangeTop(self._beam)
        else:
            self.bottom_flange = TrolleyFlangeBottom(self._beam)
            self.top_flange = TrolleyFlangeTop(self._beam)
    #
    def stress(self):
        """
        """
        if not self._crane:
            raise IOError('Crane data not provided')
        #
        return FlangeSections(top=self.top_flange.stress,
                              bottom=self.bottom_flange.stress)
    #
    #
    def actions(self):
        """
        """
        if not self._crane:
            raise IOError('Crane data not provided')
        #
        return FlangeSections(top=self.top_flange.actions,
                              bottom=self.bottom_flange.actions)    
    #
    @property
    def bottom(self):
        """
        """
        return self.bottom_flange
    #
    @property
    def top(self):
        """
        """
        return self.top_flange
#
class WebItems:
    
    def __init__(self, cls_beam):
        """
        """
        self._beam = cls_beam
        self.bottom_web = None
        self.top_web = None
        self.middle_web = None
        self._crane = None
    #
    def check(self):
        """
        """
        #print('')
        #print('--------------------------------------')
        #print(' Check Web in Local Bending')
        #print(' Web Bottom Flange')
        #self.bottom_flange.check()
        print('+++++++> Local Check Web')
        #
        if not self._crane:
            raise IOError('Crane data not provided')
        #
        return WebSections(top = self.top_web.check(self._crane),
                           bottom = self.bottom_web.check(self._crane),
                           middle = self.middle_web)
    #
    def stress(self):
        """
        """
        if not self._crane:
            raise IOError('Crane data not provided')
        #
        return WebSections(top = self.top_web.stress,
                           bottom = self.bottom_web.stress,
                           middle = self.middle_web)
    #
    def actions(self):
        """
        """
        if not self._crane:
            raise IOError('Crane data not provided')
        #
        return WebSections(top = self.top_web.actions,
                           bottom = self.bottom_web.actions,
                           middle = self.middle_web)
    #    
    def crane(self, item):
        """
        """
        self._crane = item
        # 
        self.bottom_web = WebBottom(self._beam)
        self.top_web = WebTop(self._beam)        
    #
    @property
    def bottom(self):
        """
        """
        return self.bottom_web
    #
    @property
    def top(self):
        """
        """
        return self.top_web
    #
    @property
    def middle(self):
        """
        """
        return self.middle_web    
#
#
class BeamComponents:
    """ """

    def __init__(self, cls_beam):
        """
        """
        self._beam = cls_beam
        #
        self._web = WebItems(self._beam)
        self._flange = FlangeItems(self._beam)
        self._crane = None
    #
    @property
    def crane(self):
        """
        """
        return self._crane
    
    @crane.setter
    def crane(self, item):
        """
        """
        self._crane = item
        
        # self._web = WebItems(self._beam)
        self._web.crane(self._crane)
        
        #if 'clamp' in self._crane._type :
        #    self._flange = ClampFlange(self._beam)
        #else:
        #    self._flange = TrolleyFlange(self._beam)
        
        self._flange.crane(self._crane)
    #
    @property
    def flange(self):
        """
        """
        return self._flange

    @property
    def web(self):
        """
        """
        return self._web

