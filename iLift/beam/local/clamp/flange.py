# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports
import math
from typing import NamedTuple

# package imports
from iLift.beam.section.plate.plate import Plate
#from iLift.beam.section.plate.Levy_Zhiling import Timoshenko_Zhiling
from iLift.codes.process.process import (CodeResults, 
                                         ChapterResults, 
                                         SummaryResults,
                                         AxialResults)
#
from iLift.beam.local.process.flanges import FlangeMaster
#
#
class ClampFlangeTop(FlangeMaster):

    def __init__(self, cls_beam):
        """
        """
        FlangeMaster.__init__(self, cls_beam)
        self._type = 'flange_Top'
    #
    def check(self, clamp):
        """
        """
        # clamp = self._crane
        print('Top Flange')
        #print('--------------------------------------')
        #print(' Check Flanges for Transverse Bending')
        #clamp = self._flange.cls
        units = self._beam.units
        section = self._beam.Isection
        material = self._beam.material
        #
        # define flange section
        self.plate.d = section.tft
        self.plate.w = self.Leff 
        self.plate._get_properties()
        # 
        Fx = 0 * units.N
        Fy = 0 * units.N
        Fz = 0 * units.N
        Mbx = 0 * units.N * units.m
        Mby = 0 * units.N * units.m
        Mbz = 0 * units.N * units.m
        #
        self.design.member_length = self.la
        self.design.lateral_braced_length = self.la
        #
        self.design.actions.Fx = Fx
        self.design.actions.Fy = Fy
        self.design.actions.Fz = Fz
        self.design.actions.Mx = Mbx
        self.design.actions.My = Mby
        self.design.actions.Mz = Mbz
        #
        #
        self.plate.stress(actions = self.design.actions,
                          stress = self.design.stress)
        #
        #_von_mises = self.plate
        #print(self.design.member_actions)
        #print(self.design.actions)
        self._code_check =  self.design.AISC_WSD()
        #print('-->')
        return CodeResults(axial = self._code_check.Axial_results,
                           shear = self._code_check.ChaperG_results,
                           bending = self._code_check.ChaperF_results,
                           combined = self._code_check.ChapterH_results,
                           total = self._code_check.UR_results,
                           report = self._code_check.file_out)
#
#
class ClampFlangeBottom(FlangeMaster):

    def __init__(self, cls_beam):
        """
        """
        FlangeMaster.__init__(self, cls_beam)
        self._type = 'flange_Bottom'
    #
    def check(self, clamp):
    #def rbell_method(self):
        """
        """
        print('Bottom Flange')
        # set beam data
        units = self._beam.units
        section = self._beam.Isection
        material = self._beam.material
        #
        # update Leff
        self.Leff = self.Leff + clamp.jaw_size
        #
        # Get moment from horizontal load
        BMe = clamp.load.Fy * clamp.eccentricity 
        # Couple forces from beam clamb
        Cbc = BMe / section.bfb
        # Set flange edge point load
        P_rem = max(1 - clamp.load_split, clamp.load_split)
        _point_load = (P_rem * clamp.load.Fz) + Cbc
        #
        # ------------------------------------------------
        #
        # Set load acting on the plane 
        self.design.actions.Fx = 0 * units.N
        self.design.actions.Fy = 0 * units.N
        self.design.actions.Fz = _point_load
        self.design.actions.Mx = 0 * units.N * units.m
        self.design.actions.My = _point_load * self.la
        self.design.actions.Mz = 0 * units.N * units.m        
        #
        # extrac stress from global analysis and set to local plane
        _global_stress = clamp._stress        
        #
        # ------------------------------------------------
        # define dummy plate en the xz plane of the flange
        _plane_xz = Plate(material, units)
        # define cross section
        _plane_xz.plate.d = section.tfb
        _plane_xz.plate.w = self.Leff
        _plane_xz.plate._get_properties()
        # set plate span
        _plane_xz.design.member_length = self.la
        _plane_xz.design.lateral_braced_length = self.la        
        #
        # set xz stress
        _plane_xz.plate.stress(actions = self.design.actions,
                               stress = _plane_xz.design.stress)
        #
        #_plane_xz.design.stress.sigma_z = [_global_stress.sigma_y[6] 
        #                                   for _item in _global_stress.sigma_y]
        #
        #_plane_xz.design.stress.tau_y = [_global_stress.tau_y[6]  
        #                                 for _item in _global_stress.tau_y]
        # code check
        #result_xz = _plane_xz.design.AISC_WSD()        
        #
        # -------------------------------------------------
        # Set main plate (no code check, just to carry info)
        # define cross section
        self.plate.d = section.tfb
        self.plate.w = section.bfb / 2.0
        #self.plate.d = section.tfb
        #self.plate.w = self.Leff
        self.plate._get_properties()
        #
        # set plate span
        self.design.member_length = self.la
        self.design.lateral_braced_length = self.la
        #
        # set stress
        #self.plate.stress(actions = self.design.actions,
        #                  stress = self.design.stress)
        #
        # add global stress
        #
        _mid_stress = (_global_stress.sigma_x[5] + _global_stress.sigma_x[7]) * 0.50
        self.design.stress.sigma_x = [_global_stress.sigma_x[5], 
                                      _global_stress.sigma_x[5], 
                                      _global_stress.sigma_x[5],
                                      _mid_stress, _mid_stress, _mid_stress,
                                      _global_stress.sigma_x[7], 
                                      _global_stress.sigma_x[7], 
                                      _global_stress.sigma_x[7]]
        #
        _mid_stress = (_global_stress.sigma_y[5] + _global_stress.sigma_y[7]) * 0.50
        self.design.stress.sigma_y = [_global_stress.sigma_y[5], 
                                      _global_stress.sigma_y[5], 
                                      _global_stress.sigma_y[5],
                                      _mid_stress, _mid_stress, _mid_stress,
                                      _global_stress.sigma_y[7], 
                                      _global_stress.sigma_y[7], 
                                      _global_stress.sigma_y[7]]
        #
        _mid_stress = (_global_stress.tau_x[5] + _global_stress.tau_x[7]) * 0.50
        self.design.stress.tau_x = [_global_stress.tau_x[5], 
                                    _global_stress.tau_x[5], 
                                    _global_stress.tau_x[5],
                                    _mid_stress, _mid_stress, _mid_stress,
                                    _global_stress.tau_x[7], 
                                    _global_stress.tau_x[7], 
                                    _global_stress.tau_x[7]]
        #
        _mid_stress = (_global_stress.tau_y[5] + _global_stress.tau_y[7]) * 0.50
        self.design.stress.tau_y = [_global_stress.tau_y[5], 
                                    _global_stress.tau_y[5], 
                                    _global_stress.tau_y[5],
                                    _mid_stress, _mid_stress, _mid_stress,
                                    _global_stress.tau_y[7], 
                                    _global_stress.tau_y[7], 
                                    _global_stress.tau_y[7]]
        #
        # re-asign dummy plate stress to main plate
        self.design.stress.sigma_z = _plane_xz.design.stress.sigma_y
        #
        self.design.stress.tau_z = _plane_xz.design.stress.tau_z
        #
        # code check
        self._code_check = self.design.AISC_WSD()
        #
        # get von mises 
        _vonMises = self.design.stress.von_mises()
        _vonMises_UR = max([_item.value / (0.90 * material.Fy.value) 
                            for _item in _vonMises])
        _vonMises_status = 'PASS'
        if _vonMises_UR > 1.0:
            _vonMises_status = 'FAIL'
            print('Warning Von Mises [{:1.3f}] > 1.0'.format(_vonMises_UR))
        else:
            print('UR = {:1.3f}  (Von Mises)'.format(_vonMises_UR))
        #
        #
        return CodeResults(axial = self._code_check.Axial_results,
                           shear = self._code_check.ChaperG_results,
                           bending = self._code_check.ChaperF_results,
                           combined = self._code_check.ChapterH_results,
                           total = self._code_check.UR_results,
                           report = self._code_check.file_out)
    #
#
#
#
 