# 
# Copyright (c) 2019 iLift
#

# Python stdlib imports
import math
from typing import NamedTuple, Tuple, Dict

# package imports
from iLift.beam.section.plate.plate import Plate
from iLift.codes.process.process import CodeResults
from iLift.beam.section.plate.Levy_Zhiling import Timoshenko_Zhiling

#
#
#
class FlangeSections(NamedTuple):
    """
    Force & bending moments
    """
    top:Tuple
    bottom:Tuple
#
#
class FlangeMaster(Plate):

    def __init__(self, cls_beam):
        """
        """
        self._beam = cls_beam
        Plate.__init__(self, self._beam.material, 
                       self._beam.units)
        #
        self._angle = 45 * self._beam.units.deg
        self.l_eff = 0 * self._beam.units.m
        self.l_a = 0 * self._beam.units.m
        self._code_check: Dict = {}
    #
    def Timoshenko_method(self, Fz):
        """
        """
        #
        #Fzt = 10 * units.kN
        # Timoshenko_Zhiling(F_0=Fzt.convert('newton'), s = 3 * units.m,       
        #                  b = 2 * units.m, a = 3 * units.m, 
        #                  t = 0.01 * units.m, E = material.E.convert('pascal'))
        #
        units = self._beam.units
        section = self._beam.Isection
        material = self._beam.material
        #
        _tf = section.tft
        if self._type == 'flange_Bottom':
            _tf = section.tfb
        #
        Timoshenko_Zhiling(F_0 = Fz.convert('newton'), 
                           s= 0.1 * units.m,     # @hami2230 - added .convert('newton') and .convert('megapascal')
                           b = self.la, 
                           a = self.Leff, 
                           t = _tf, 
                           E = material.E.convert('pascal'))
        #      
    #
    @property
    def distribution_angle(self):
        """
        distribution angle to calculate equivalent width Leff
        """
        return self._angle
    
    @distribution_angle.setter
    def distribution_angle(self, value:float):
        """
        distribution angle to calculate equivalent width Leff
        """
        self._angle = value
    #
    @property
    def la(self):
        """
        lever arm to root radius
        """
        #if self.l_a.value == 0:
        section = self._beam.Isection
        _bf = section.bft
        if self._type == 'flange_Bottom':
            _bf = section.bfb
        
        self.l_a = ((_bf - section.tw) / 2.0 - section.r)
        return self.l_a
    
    #@la.setter
    #def la(self, value):
    #    """
    #    lever arm to root radius
    #    """
    #    self.l_a = value
    #
    #
    @property
    def actions(self):
        """
        """
        return self.design.actions    
    #
    @property
    def stress(self):
        """
        """
        return self.design.stress    
    #
    @property
    def Leff(self):
        """
        flange effective width
        """
        # TODO: is this usefull ?
        if self.l_eff.value == 0:
            self.l_eff = (2 * self.la 
                          * math.tan(self.distribution_angle.value))
        #
        return self.l_eff
    
    @Leff.setter
    def Leff(self, value):
        """
        flange effective width
        """
        self.l_eff = value
#
#