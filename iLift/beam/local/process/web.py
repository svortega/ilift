# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports
import math
from typing import NamedTuple, Tuple, Dict

# package imports
from iLift.beam.section.plate.plate import Plate
from iLift.codes.process.process import CodeResults

#
class WebSections(NamedTuple):
    """
    Force & bending moments
    """
    top:float
    bottom:float
    middle:float
#
#
class WebMaster(Plate):
    
    def __init__(self, cls_beam):
        """
        """
        self._beam = cls_beam
        Plate.__init__(self, self._beam.material, 
                       self._beam.units)
        
        self._angle = 45 * self._beam.units.deg
        self.l_eff = 0 * self._beam.units.m
        self.l_a = 0 * self._beam.units.m
        self._code_check: Dict = {}
    #
    @property
    def la(self):
        """
        lever arm to root radius
        """
        section = self._beam.Isection
        _bf = section.bft
        if self._type == 'web_bottom':
            _bf = section.bfb
        
        self.l_a = ((_bf - section.tw) / 2.0
                    - section.r)
        return self.l_a
    #
    @property
    def distribution_angle(self):
        """
        distribution angle to calculate equivalent width Leff
        """
        return self._angle
    
    @distribution_angle.setter
    def distribution_angle(self, value:float):
        """
        distribution angle to calculate equivalent width Leff
        """
        self._angle = value
    #
    @property
    def stress(self):
        """
        """
        return self.design.stress
    #
    @property
    def actions(self):
        """
        """
        return self.design.actions    
#
class WebBottom(WebMaster):

    def __init__(self, cls_beam):
        """
        """
        WebMaster.__init__(self, cls_beam)
        self._type = 'web_bottom'
    #    
    def check(self, crane):
        """
        """
        print('Bottom Web')
        #crane = self._crane
        #print('')
        #print('-------------------')
        #print(' Web Bottom Flange')
        #
        _units = self._beam.units
        section = self._beam.Isection
        material = self._beam.material
        #
        #print('web bottom eccentricity', crane.eccentricity.value)
        # ----------------------------------
        # check the web in local bending
        #
        # leverarm to root radious
        #print('Lever arm to root radious (la) = {: 1.1f} mm'
        #      .format(self.la.convert('millimetre').value))
        #
        #
        # Vertical load
        P_flange_edge = round(2 * crane.load_split - 1.0, 3) * crane.load.Fz
        #Mbf_bottom = P_flange_edge * self.la   # flange in cantilever
        Mbf_bottom = P_flange_edge * section.bf / 2.0   # @hami2230 - v0.5 update
        #
        # Horizontal load
        BMbottom = crane.load.Fy * (crane.eccentricity + section.tfb + section.r)
        #print('BMbottom', BMbottom)
        #
        Mby = BMbottom + Mbf_bottom
        #print('Local Bending Moment in the Web {: 1.3f} kN-m'
        #      .format(Mbz.convert('kilonewton * metre').value))
        # update Leff
        self.Leff = self.Leff + crane.jaw_size        
        #print('Width of bottom flange (Leff) = {: 1.1f} mm'
        #      .format(self.Leff.convert('millimetre').value))
        #
        self.plate.d = section.tw
        self.plate.w = self.Leff 
        self.plate._get_properties()
        #
        #L = section.tw
        Fx = 0 * _units.N
        Fy = 0 * _units.N
        P_rem = max(1 - crane.load_split, crane.load_split)
        Fz = P_rem * crane.load.Fz
        Mbx = 0 * _units.N * _units.m
        Mbz = 0 * _units.N * _units.m
        #
        #print('Applied Loads')
        #print('Fx {: 1.3f} kN'.format(Fx.convert('kilonewton').value))
        #print('Fy {: 1.3f} kN'.format(Fy.convert('kilonewton').value))
        #print('Fz {: 1.3f} kN'.format(Fz.convert('kilonewton').value))
        #print('Mx {: 1.3f} kN-m'.format(Mbx.convert('kilonewton * metre').value))
        #print('My {: 1.3f} kN-m'.format(Mby.convert('kilonewton * metre').value))
        #print('Mz {: 1.3f} kN-m'.format(Mbz.convert('kilonewton * metre').value))
        #
        self.design.member_length =  section.ho
        self.design.lateral_braced_length =  section.ho
        #
        self.design.actions.Fx = Fx
        self.design.actions.Fy = Fy
        self.design.actions.Fz = Fz
        self.design.actions.Mx = Mbx
        self.design.actions.My = Mby
        self.design.actions.Mz = Mbz
        #
        self.plate.stress(actions = self.design.actions, 
                          stress = self.design.stress) # @hami2230 - updated
        #
        #
        self._code_check =  self.design.AISC_WSD()
        #
        # get von mises 
        #_vonMises = self.design.stress.von_mises()
        #_vonMises_UR = max([_item.value / (0.90 * material.Fy.value) 
        #                    for _item in _vonMises])
        #if _vonMises_UR > 1.0:
        #    print('Warning Von Mises [{:1.3f}] > 1.0'.format(_vonMises_UR))
        #else:
        #    print('UR = {:1.3f}  (Von Mises)'.format(_vonMises_UR))        
        #
        return CodeResults(axial = self._code_check.Axial_results,
                           shear = self._code_check.ChaperG_results,
                           bending = self._code_check.ChaperF_results,
                           combined = self._code_check.ChapterH_results,
                           total = self._code_check.UR_results,
                           report = self._code_check.file_out)
    #
    @property
    def Leff(self):
        """
        flange effective width
        """
        #
        # TODO: is this usefull ?
        if self.l_eff.value == 0:        
            section = self._beam.Isection
            #  Length of flange adopted     # @hami2230 - to be discussed
            _b = (2 * math.tan(self.distribution_angle.value) 
                  * (self.la + section.r + 0.5 * section.tw))
            #
            L_bottom_web = (2 * ((self.la + 2 * section.r)
                                 * math.tan(self.distribution_angle.value)))
            #
            self.l_eff = min(_b, L_bottom_web)
        #
        return self.l_eff
    #
    @Leff.setter
    def Leff(self, value):
        """
        flange effective width
        """
        self.l_eff = value
    #
#
class WebTop(WebMaster):

    def __init__(self, cls_beam):
        """
        """
        WebMaster.__init__(self, cls_beam)
        self._type = 'web_top'
    #    
    def check(self, crane):
        """
        """
        print('Top Web')
        #crane = self._crane
        #print('')
        #print('----------------')
        #print('Web Top Flange')
        #
        _units = self._beam.units
        section = self._beam.Isection
        material = self._beam.material
        #
        # ----------------------------------
        # leverarm to root radious
        #print('Lever arm to root radious (la) = {:1.1f} mm'
        #      .format(self.la.convert('millimetre').value))
        # Top Flange
        # Vertical load   
        P_flange_edge = round(2 * crane.load_split - 1.0, 3) * crane.load.Fz
        Mbf_top = P_flange_edge * section.bf / 2.0   # @hami2230 - v0.5 update
        #
        # Calculate bending moment at top of web
        BMtop = crane.load.Fy * (crane.eccentricity 
                                 + section.d
                                 - section.tft  # top flange in case non symm
                                 - section.r)
        #print('BMtop', BMtop)
        #
        Mby = BMtop + Mbf_top
        #
        #print('Bending moment at top flange = {:1.1f} kN-m'
        #      .format(Mbz.convert('kilonewton * metre').value))
        #
        # update Leff
        self.Leff = self.Leff + crane.jaw_size
        #print('Width of top flange (Leff) = {:1.1f} mm'
        #      .format(self.Leff.convert('millimetre').value))
        #
        self.plate.d =  section.tw  
        self.plate.w = self.Leff # effective leng
        self.plate._get_properties()
        #                             # @hami2230 - note this ignores tension and shear in the web.
        #L = section.tw
        Fx = 0 * _units.N
        Fy = 0 * _units.N
        P_rem = max(1 - crane.load_split, crane.load_split)
        Fz = P_rem * crane.load.Fz
        Mbx = 0 * _units.N * _units.m
        Mbz = 0 * _units.N * _units.m
        #
        #print('Applied Loads')
        #print('Fx {: 1.3f} kN'.format(Fx.convert('kilonewton').value))
        #print('Fy {: 1.3f} kN'.format(Fy.convert('kilonewton').value))
        #print('Fz {: 1.3f} kN'.format(Fz.convert('kilonewton').value))
        #print('Mx {: 1.3f} kN-m'.format(Mbx.convert('kilonewton * metre').value))
        #print('My {: 1.3f} kN-m'.format(Mby.convert('kilonewton * metre').value))
        #print('Mz {: 1.3f} kN-m'.format(Mbz.convert('kilonewton * metre').value))
        #
        # assumed cantilever beam with span = flange - web thk
        self.design.member_length =  section.ho
        self.design.lateral_braced_length =  section.ho
        #
        self.design.actions.Fx = Fx 
        self.design.actions.Fy = Fy 
        self.design.actions.Fz = Fz
        self.design.actions.Mx = Mbx
        self.design.actions.My = Mby
        self.design.actions.Mz = Mbz
        #
        #self._get_global_stress(global_stress = crane._stress)
        #
        self.plate.stress(actions = self.design.actions, 
                          stress = self.design.stress)       
        #
        # self.design.stress = section.stress(self.design.actions)
        self._code_check =  self.design.AISC_WSD()
        #
        # get von mises 
        #_vonMises = self.design.stress.von_mises()
        #_vonMises_UR = max([_item.value / (0.90 * material.Fy.value) 
        #                    for _item in _vonMises])
        #if _vonMises_UR > 1.0:
        #    print('Warning Von Mises [{:1.3f}] > 1.0'.format(_vonMises_UR))
        #else:
        #    print('UR = {:1.3f}  (Von Mises)'.format(_vonMises_UR))
        #         
        return CodeResults(axial = self._code_check.Axial_results,
                           shear = self._code_check.ChaperG_results,
                           bending = self._code_check.ChaperF_results,
                           combined = self._code_check.ChapterH_results,
                           total = self._code_check.UR_results,
                           report = self._code_check.file_out)
        #       
    #
    #
    @property
    def Leff(self):
        """
        flange effective width
        """
        # TODO: is this usefull ?
        if self.l_eff.value == 0:        
            section = self._beam.Isection
            #  Length of flange adopted
            _b = (2 * math.tan(self.distribution_angle.value) 
                  * (self.la + section.r + 0.5 * section.tw) 
                  + (2 * (section.d - section.tfb - section.tft - section.r)))     # @hami2230 - updated
            #
            #
            # L_top_web = (2 * (self.la + 2 * section.r                                # @hami2230 - cant follow this working so blanked out
            #                  + ((section.hw - section.r) * math.tan(self._web.distribution_angle.value)))
            #             + jaw_size * 0)
            #
            self.l_eff = _b # min(_b, L_top_web)                          # @hami2230 - updated, can change back if L_top_web
        #
        return self.l_eff
    
    @Leff.setter
    def Leff(self, value):
        """
        flange effective width
        """
        self.l_eff = value
    #    
#
#