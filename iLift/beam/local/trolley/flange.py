# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports
#import math
from typing import NamedTuple

# package imports
from iLift.beam.section.plate.plate import Plate
from iLift.codes.process.process import (CodeResults, 
                                         ChapterResults, 
                                         SummaryResults,
                                         AxialResults)
from iLift.codes.EN1993.April2007.EN1993_6_2007 import table_6_2
#
from iLift.beam.local.process.flanges import FlangeMaster


#
#
class TrolleyFlangeTop(FlangeMaster):
    
    def __init__(self, cls_beam):
        """
        """
        FlangeMaster.__init__(self, cls_beam)
        self._type = 'flange_bottom'
    #
    def check(self, trolley):
        """
        """
        print('')
        print('-------------------------------------------')
        print(' Resistance of Top flange to wheel loads')        
        #
        #trolley = self._flange.cls
        units = self._beam.units
        section = self._beam.Isection
        load = self._beam._load
        eccentricity = trolley.eccentricity
        #
        # define flange section
        self.plate.d = section.tft
        self.plate.w = self.Leff 
        self.plate._get_properties()
        # 
        Fx = 0 * units.N
        Fy = 0 * units.N
        Fz = 0 * units.N
        Mbx = 0 * units.N * units.m
        Mby = 0 * units.N * units.m
        Mbz = 0 * units.N * units.m
        #
        self.design.member_length = self.la
        self.design.lateral_braced_length = self.la
        #
        self.design.actions.Fx = Fx
        self.design.actions.Fy = Fy
        self.design.actions.Fz = Fz
        self.design.actions.Mx = Mbx
        self.design.actions.My = Mby
        self.design.actions.Mz = Mbz
        #
        #
        self.plate.stress(actions = self.design.actions,
                          stress = self.design.stress)
        #
        #_von_mises = self.plate
        #print(self.design.member_actions)
        #print(self.design.actions)
        self._code_check =  self.design.AISC_WSD()
        #print('-->')
        return CodeResults(axial = self._code_check.Axial_results,
                           shear = self._code_check.ChaperG_results,
                           bending = self._code_check.ChaperF_results,
                           combined = self._code_check.ChapterH_results,
                           total = self._code_check.UR_results,
                           report = self._code_check.file_out)        

#
#
#
class TrolleyFlangeBottom(FlangeMaster):
    
    def __init__(self, cls_beam):
        """
        """
        FlangeMaster.__init__(self, cls_beam)
        self._type = 'flange_bottom'
    #
    def check(self, trolley):
        """
        """
        print('')
        print('-------------------------------------------')
        print(' Resistance of bottom flange to wheel loads')        
        #
        units = self._beam.units
        section = self._beam.Isection
        #
        # Assuming defined % of the load goes to the front wheels
        # Front wheels take half of the load (is this correct?)   
        #_point_load = 2 * (trolley.load_split * trolley.load.Fz) / trolley.number_wheels
        
        _point_load = trolley.load_split * trolley.load.Fz                     # @hami2230 - this has 60/40 load split hard wired for trolley
        print('vertical (unfactored)  = {: 1.3f} kN'.format(_point_load.convert('kilonewton').value)) # @hami2230 - 21.02.19 updated
        #
        #
        self.design.actions.Fx= 0 * units.kN
        self.design.actions.Fy= 0 * units.kN
        self.design.actions.Fz= _point_load
        self.design.actions.Mx= 0 * units.kN * units.m
        self.design.actions.My= 0 * units.kN * units.m
        self.design.actions.Mz= 0 * units.kN * units.m
        #
        #
        # Get global stress
        _global_stress = trolley._stress                                       
        #
        self._get_global_stress(local_stress = self.design.stress,
                                global_stress = _global_stress)
        #
        # code check
        #
        _code = self.design.EN1993_6(trolley, section)
        #
        # TODO: report code check results
        #return CodeResults(axial = _code.Axial_results,
        #                   shear = _code.ChaperG_results,
        #                   bending = _code.ChaperF_results,
        #                   combined = _code.ChapterH_results,
        #                   total = _code.UR_results)
        #
        # define result format
        #_summary = CodeResults(axial = AxialResults(result_xz.Axial_results.URy, 
        #                                            result_yz.Axial_results.URy, 
        #                                            result_yz.Axial_results.UR_flag,
        #                                            result_xz.Axial_results.allowable),
        #                       shear = ChapterResults(result_xz.ChaperG_results.URz, 
        #                                              result_yz.ChaperG_results.URz,
        #                                              result_xz.ChaperG_results.UR_flag,
        #                                              result_xz.ChaperG_results.allowable_z,
        #                                              result_yz.ChaperG_results.allowable_z),
        #                       bending = ChapterResults(result_xz.ChaperF_results.URy, 
        #                                                result_yz.ChaperF_results.URy,
        #                                                result_xz.ChaperF_results.UR_flag,
        #                                                result_xz.ChaperF_results.allowable_y,
        #                                                result_yz.ChaperF_results.allowable_y),
        #                       combined = SummaryResults(_comb_UR, _comb_status, _comb_flag),
        #                       total = SummaryResults(_vonMises_UR, _vonMises_status,
        #                                              'VonMises'),
        #                       report = 'N/A')
        #return _summary
#
#
#