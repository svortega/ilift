# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports
from math import isclose, sin
from typing import ClassVar, List, Dict, Tuple # NamedTuple,


# package imports
from iLift.beam.section.isection.ibeam import Ibeam
from iLift.codes.design import DesignProperties, CodeResults
from iLift.beam.assessment.formulas.analysis import BeamFormulas
from iLift.beam.assessment.fea.analysis import BeamFEA, BeamElement
from iLift.beam.local.section_items import BeamComponents, LocalElement
from iLift.material.material import Material
#


#
class Beam:
    """ 
    """
    __slots__ = ('L', 'Lcantilever', 'a', '_shape_factor',
                 '_units', '_material', '_ibeam', '_design',
                 '_cranes', '_load', '_beam_supports',
                 '_fe_model', '_element', '_UR', '_cantilever', 
                 '_code_check','_local_check', '_flange', '_web')
    
    def __init__(self, cranes, load, units):
        """
        """
        self.L:ClassVar = None
        self.Lcantilever:ClassVar = None
        self.a:ClassVar = None
        self._cantilever:bool = False
        # design defaults
        self._units: ClassVar = units
        self._material: ClassVar = Material(self._units)
        # 
        self._ibeam: ClassVar = Ibeam(self._material, units)
        self._design: ClassVar = DesignProperties(units)
        self._design.section = self._ibeam
        self._design.material = self._material
        #
        self._cranes:Dict = cranes
        self._load: ClassVar = load
        #
        self._beam_supports:List = []
        self._fe_model: ClassVar = BeamFEA(self._ibeam, units)
        #
        self._element:Dict = {}
        #
        self._UR:float = 0.0
        self._shape_factor:float = 1.6
        #
        self._code_check = {}
        self._local_check: ClassVar = BeamComponents(self)
        self._flange:Dict = {}
        self._web:Dict = {}
    #
    @property
    def simply_suported(self):
        """
        """
        self._cantilever = False
        self._beam_supports = ['pinned', 'pinned']
    #
    @property
    def fixed_both_ends(self):
        """
        """
        self._cantilever = False
        self._beam_supports = ['fixed', 'fixed']
    #
    @property
    def cantilever(self):
        """
        """
        self._cantilever = True
        self._beam_supports = ['fixed', 'free']
    #
    @property
    def units(self):
        """
        """
        return self._units
    #
    @property
    def material(self):
        """
        """
        return self._material
    #
    @property
    def Isection(self):
        """
        """
        return self._ibeam
    #
    @property
    def cranes(self):
        """
        """
        return self._cranes
    #
    @property
    def model(self):
        """
        return FE model 
        """
        return self._fe_model    
    #
    @property
    def UR(self):
        """
        UR: beam initial utilization ratio 
        """
        return self._UR
    
    @UR.setter
    def UR(self, value:float):
        """
        UR: beam initial utilization ratio 
        """
        self._UR = value
    #
    @property
    def point_load(self):
        """
        """
        return self._load.point_load
    #
    @property
    def udl(self):
        """
        """
        return self._load.uniform_distributed
    #
    @property
    def design(self):
        """
        """
        return self._design
    #
    @property
    def span_between_supports(self) -> ClassVar:
        """
        """
        return self.L
    @span_between_supports.setter
    def span_between_supports(self, value:ClassVar) -> None:
        """
        """
        self.L = value
        self.design.member_length = self.L
        #self.design.laterally_unbraced_length = self.L
        #if self.design.Xe.value == 0:
        #    self.design.Xe = self.L * 0.50
        self.simply_suported
    #
    @property
    def span_cantilever(self) -> ClassVar:
        """
        """
        return self.L
    @span_cantilever.setter
    def span_cantilever(self, value:ClassVar) -> None:
        """
        Beam lenght 
        """
        self.L = value
        self.design.member_length = self.L
        #self.design.laterally_unbraced_length = self.L
        #if self.design.Xe.value == 0:
        #    self.design.Xe = self.L        
        self.cantilever
    #
    @property
    def shape_factor(self):
        """
        Beam shape factor for wind loading
        """
        return self._shape_factor
    @shape_factor.setter
    def shape_factor(self, value:float):
        """
        Beam shape factor for wind loading
        
        Parameters
        ----------
        value: float 
        """
        self._shape_factor = value    
    #
    #
    def get_udl_based_on_UR(self, UR:float):
        """
        Estimate beam udl based on Timoshenko formulas
        
        Parameters
        ----------
        UR: float 
            Beam utilization ration
        
        Returns
        -------
        udl : ClassVar
        """
        self.Isection._get_properties()
        _beam = BeamFormulas(self.units)
        _beam.UR = UR
        #_beam.load_factor = self._load._factors.DAF.vertical
        _beam.material = self.material
        _beam.section = self.Isection
        _beam.beam_span = self.L
        _beam.beam_support = self._beam_supports
        return _beam.get_udl
    #
    def get_vertical_load_based_on_UR(self, UR:float):
        """
        Estimate beam point load based on Timoshenko formulas
        
        Parameters
        ----------
        UR: float 
            Beam utilization ration
        
        Returns
        -------
        Point load : ClassVar
        """
        # TODO: add cranes
        #if self._cranes:
            #_Xe = self._cranes[1].Xe
            #if _Xe.value == 0:
            #    _Xe = self.L / 2.0
            #
        self.get_beam_nodes()
        _Xe = self.model.Xe
        #else:
            #_nodes = self._get_beam_nodes_actions()
            #print('fix direct load')
            #1/0
        #
        self.Isection._get_properties()
        _beam = BeamFormulas(self.units)
        _beam.UR = UR
        #_beam.load_factor = self._load._factors.DAF.vertical
        _beam.load_lateral_angle = self._load.lift_angle.lateral
        _beam.load_horizontal_angle = self._load.lift_angle.horizontal
        _beam.load_lateral_eccentricity = (self.Isection.Zc 
                                           + self._cranes[1].eccentricity_to_flangle)
        _beam.material = self.material
        _beam.section = self.Isection
        _beam.beam_span = self.L
        _beam.Xe = _Xe  # FIXME: define input 
        _beam.nodes = self.model.nodes
        _beam.beam_support = self._beam_supports
        return _beam.get_point_load 
    #    
    #
    def _get_beam_nodes_crane(self, cranes):
        """
        Define nodes along the beam based on crane position
        
        Parameters
        ----------
        Cranes: ClassVar 
                Cranes object
        """
        if not cranes:
            raise IOError('No clamp/trolley data found')
        
        elif len(cranes) == 1:
            _coord, cranes[1].Xe = self._get_beam_nodes_one_crane(cranes[1].Xe)
        
        elif len(cranes) == 2:
            (_coord, cranes[1].Xe, 
             cranes[2].Xe) = self._get_beam_nodes_two_cranes(cranes[1].Xe,
                                                             cranes[2].Xe)
            #1/0
            #_gap = abs(cranes[1].Xe - cranes[2].Xe)
            #if _gap.value == 0:
            #    _coord, cranes[1].Xe = self._get_beam_nodes_two_cranes(cranes[1].Xe)
            #    if self._cantilever:
            #        cranes[2].Xe = self.L
            #    else:
            #        cranes[2].Xe = _coord[3]
            #else:
            #    _xe = [cranes[1].Xe.value, cranes[2].Xe.value]
            #    1/0
            #    #if not all(_xe):
            #    # TODO: few options to be sorted
            #    _index = [i for i, x in enumerate(_xe) if x != 0]
            #    _index
            ##
            # TODO : adjust limits 
            #if cranes[2].Xe > self.L * (5/6.0):
            #    pass
            #elif cranes[1].Xe <= self.L  * (1/6.0):
            #    pass
            #else:
            #    pass
            #
            #if not cranes[1].load :
            #self._update_crane_load(cranes[1], self._load, 
            #                        load_distribution=self._load.load_distribution)
            #
            #if not cranes[2].load :
            #self._update_crane_load(cranes[2], self._load, 
            #                        load_distribution=(1 - self._load.load_distribution))
            #
        else:
            raise Warning('maximum two crane items allowed')
        #
        return _coord
    #
    def _get_beam_nodes_one_crane(self, Xe:ClassVar):
        """
        Define nodes along the beam based on load position
        
        Parameters
        ----------
        Xe: ClassVar 
            distance from beam end 1
        """
        #
        if Xe.value == 0: # default values
            if self._cantilever:
                Xe = self.L
            else:
                Xe = self.L / 2.0
        #
        _beam_height = self.Isection.height.value
        _base =  (self.L - Xe)
        _zero = 0 * self.L
        #
        # define beam's element coordinates
        if _base.value == 0: # cantilever
            _coord = [_zero, 
                      self.L * 0.25,
                      self.L * 0.50, 
                      self.L * 0.75,
                      self.L]
            print('   1     2     3     4  |')
            print('|=====+=====+=====+=====V')
        
        elif Xe > self.L * 0.60:
            _mid_span = self.L * 0.50
            _coord = [_zero, 
                      _mid_span * 0.50 ,
                      _mid_span, Xe,
                      self.L]
            print('   1     2     3  |  4')
            print('o=====+=====+=====V=====o')
        
        elif Xe < self.L * 0.30:
            _coord = [_zero, Xe,
                      self.L * 0.50, 
                      self.L * 0.75,
                      self.L]
            print('   1  |  2     3     4')
            print('o=====V=====+=====+=====o')
        
        else:
            _coord = [_zero, 
                      Xe * 0.50 , Xe,  
                      Xe + 0.50 * _base,
                      self.L]
            print('   1     2  |  3     4')
            print('o=====+=====V=====+=====o')
        #
        return _coord, Xe
    #
    def _get_beam_nodes_two_cranes(self, Xe1:ClassVar, Xe2:ClassVar):
        """
        Define nodes along the beam based on load position
        
        Parameters
        ----------
        Xe: ClassVar 
            distance from beam end 1
        """
        _zero = 0 * self.L
        _Xe = [Xe1.value, Xe2.value]
        if sum(_Xe) == 0: 
            _coord = [_zero, 
                      self.L * 0.25,
                      self.L * 0.50, 
                      self.L * 0.75,
                      self.L]            
            if self._cantilever:
                Xe1 = _coord[3]
                Xe2 = _coord[4]
                print('   1     2     3  |  4  |')
                print('|=====+=====+=====V=====V')
            else:
                Xe1 = _coord[2]
                Xe2 = _coord[3]
                print('   1     2  |  3  |  4')
                print('o=====+=====V=====V=====o')
        #elif any(_Xe):
        #    print('--l')
        else:
            raise NotImplemented(' error ')
        
            if _Xe[0] < _Xe[1]:
                print('--clamp1 < clamp2')
            else:
                print('--clamp2 < clamp1')
            #
            _gap = abs(Xe1 - Xe2)
            _beam_height = self.Isection.height.value
            _base =  (self.L - Xe)
            #
            # define beam's element coordinates
            if _base.value == 0: # cantilever
                _coord = [_zero, 
                          self.L * 0.25,
                          self.L * 0.50, 
                          self.L * 0.75,
                          self.L]
                print('   1     2     3     4  |')
                print('|=====+=====+=====+=====V')
            
            elif Xe > self.L * 0.60:
                _mid_span = self.L * 0.50
                _coord = [_zero, 
                          _mid_span * 0.50 ,
                          _mid_span, Xe,
                          self.L]
                print('   1     2     3  |  4')
                print('o=====+=====+=====V=====o')
            
            elif Xe < self.L * 0.30:
                _coord = [_zero, Xe,
                          self.L * 0.50, 
                          self.L * 0.75,
                          self.L]
                print('   1  |  2     3     4')
                print('o=====V=====+=====+=====o')
            
            else:
                _coord = [_zero, 
                          Xe * 0.50 , Xe,  
                          Xe + 0.50 * _base,
                          self.L]
                print('   1     2  |  3     4')
                print('o=====+=====V=====+=====o')
        #
        return _coord, Xe1, Xe2
    #    
    #
    def get_beam_nodes(self):
        """
        Process to define FE beam nodes
        """
        if self._cranes:
            _nodes = self._get_beam_nodes_crane(self._cranes)
            self.model.Xe = self._cranes[1].Xe
        else:
            #_Xe = self.design.Xe
            _Xe = self._load.point_load.Xe
            _nodes, _Xe = self._get_beam_nodes_one_crane(_Xe)
            self.model.Xe = _Xe
        #
        for x in range(1, len(_nodes)):
            _lenght = _nodes[x] - _nodes[x-1]
            print("L{:} = {:1.3f} m"
                  .format(x, _lenght.convert('metre').value))
        print("")
        self.model.nodes = _nodes
        #return _nodes
    #
    def _update_load(self):
        """
        update beam load based either in cranes or point load
        """
        if self._cranes:
            _number = len(self._cranes)
            for _crane in self._cranes.values():
                self._load.update_crane_load(crane =_crane,
                                             Isection = self.Isection, 
                                             load_distribution = 1.0/_number)
            self.model.cranes = self._cranes
        else:
            self.model.actions = self._load.update_point_load()
    #
    def _analysis(self):
        """
        FE analysis setup and solution
        """
        self.Isection._get_properties()
        self._update_load()
        # update vertical udl
        if self.UR > 0:
            udl = self.get_udl_based_on_UR(self.UR)
        else:
            udl = self._load.beam_selfweight(self.Isection)
        # additional udl is added
        #DAF_vertical = max(1, self._load._factors.DAF.vertical)
        #vertical_udl = self._load.uniform_distributed.vertical * DAF_vertical + udl
        vertical_udl = self._load.uniform_distributed.vertical + udl
        # update lateral udl
        udl_beam_wind = self._load.beam_wind_pressure(self.Isection) * self.shape_factor
        #DAF_lateral = max(1, self._load._factors.DAF.lateral)
        #udl_lateral = self._load.uniform_distributed.lateral * DAF_lateral + udl_wind
        udl_lateral = self._load.uniform_distributed.lateral + udl_beam_wind
        # set beam model
        self.model.supports = self._beam_supports
        self.model.uniform_load = [0 * udl_lateral, udl_lateral, vertical_udl]
        return self.model.solve()
    #
    def global_analysis(self):
        """
        return beam stress based in flexion + torsion
        """
        self.get_beam_nodes()
        return self._analysis()
    #
    def _get_code_check(self, beam_forces, beam_stress, displacements):
        """
        global code check performed and summary results extracted
        
        Parameters
        ----------
        beam_stress: Dict 
        
        displacements : List
        """
        for key, _stress in beam_stress.items():
            _node1 = self.model.nodes[key-1].value
            _node2 = self.model.nodes[key].value
            self.design.stress = _stress
            self._code_check[key] = self.design.AISC_WSD()
            _summary = CodeResults(axial = self._code_check[key].Axial_results,
                                   shear = self._code_check[key].ChaperG_results,
                                   bending = self._code_check[key].ChaperF_results,
                                   combined = self._code_check[key].ChapterH_results,
                                   total = self._code_check[key].UR_results,
                                   report = self._code_check[key].file_out)
            #
            self._element[key] = BeamElement(compactness=self._code_check[key].compactness,
                                             actions = beam_forces[key],
                                             stress = _stress,
                                             code_check = _summary,
                                             node1 = displacements[_node1],
                                             node2 = displacements[_node2])
    #
    def code_check(self):
        """
        code check beam elements 
        """
        if not self.L:
            raise IOError('beam span (L) not provided')
        elif self.L < 2 * self.Isection.d:
            print('short beam span (L={:1.2f}m < {:1.2f}m [2 x section height])'
                  .format(self.L.convert('metre').value, 
                          2 * self.Isection.d.convert('metre').value))
            self.L = 2 * self.Isection.d
        #
        beam_forces, beam_stress, displacements = self.global_analysis()
        print('')
        print('=======> Beam Global Check')
        self._get_code_check(beam_forces, beam_stress, displacements)
    #
    @property
    def elements(self):
        """
        Beam elements with summary results
        """
        if not self._element:
            self.code_check()
        return self._element
    #
    # ------------
    #
    @property
    def flange(self):
        """
        Beam flange top and bottom
        """
        if not self._flange:
            self.local_code_check()
        return self._flange
    #
    @property
    def web(self):
        """
        Beam web top, mid and bottom
        """
        if not self._web:
            self.local_code_check()        
        return self._web
    #
    def local_code_check(self):
        """
        code check beam elements
        """
        if not self._element:
            beam_stress, displacements = self.global_analysis()
        
        for key, _crane in self._cranes.items():
            print('')
            print('{:} number {:}'.format(_crane._type, key))
            self._local_check.crane = _crane
            _flanges = self._local_check.flange.check()
            _webs = self._local_check.web.check()
            #_stress = self._local_check.flange.stress()
            self._flange[key] = LocalElement(actions = self._local_check.flange.actions(),
                                             stress = self._local_check.flange.stress(),
                                             code_check = _flanges,
                                             properties = self._local_check.flange)
            
            self._web[key] = LocalElement(actions = self._local_check.web.actions(),
                                          stress = self._local_check.web.stress(),
                                          code_check = _webs,
                                          properties = self._local_check.web)
        #
        #print('-->')
        