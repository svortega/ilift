# 
# Copyright (c) 2019 iLift
# 

# Python stdlib imports


# package imports
import numpy as np


#
def Alpha_m(a, y, m):
    """
    """
    return m * np.pi * y / a


def Fm(F_0, s, a, m):
    """
    Eq B-20
    """
    #print('-->')
    return ((4 * F_0 / (s*m*np.pi)) * 
            np.sin(m*np.pi / 2.0) * 
            np.sin(m*np.pi*s / (2*a)))

def Fm_point(F_0, s, a, m):
    """
    Eq B-20
    """
    #print('-->')
    return 2 * F_0 / a * np.sin(m*np.pi / 2.0)

def Beta_m(a, b, m):
    """
    """
    #print('-->')
    return m * np.pi * b / a


def Gm(a, b, beta_m, nu=0.30):
    """
    Eq B-15
    """
    #beta_m = Beta_m(a, b, m)
    #sinh = np.sinh(beta_m) 
    #cosh = np.cosh(beta_m)
    #print('-->')
    return (((nu - 1) * beta_m * np.sinh(beta_m) - 2 * np.cosh(beta_m)) /
            ((nu - 1) * beta_m * np.cosh(beta_m) - (1 + nu) * np.sinh(beta_m)))


def Dm(F_0, s, a, b, beta_m, G_m, m, D, nu):
    """
    Eq B-22
    """
    F_m = Fm(F_0, s, a, m)
    #F_m = Fm_point(F_0, s, a, m)
    return (F_m / D * np.power(m*np.pi / a, -3) /
            (((3 - G_m * beta_m) - (2 - nu) * (1 - G_m * beta_m)) * 
             np.sinh(beta_m) +
             (((-2 * G_m + beta_m) - (2 - nu) * beta_m) *
              np.cosh(beta_m))))



def w_xy(x, y, F_0, s, b, a, t, E, nu, series_size=200):
    """
    B-24
    """
    w = 0
    sigma_x = 0
    sigma_y = 0
    sigma_xy = 0
    D = E * t**3 / (12* (1 - nu**2))
    for m in range(1, series_size):
        mpia = m * np.pi / a
        alpha_m = Alpha_m(a, y, m)
        beta_m = Beta_m(a, b, m)
        G_m = Gm(a, b, beta_m, nu)
        if np.isnan(G_m):
            break
        D_m = Dm(F_0, s, a, b, beta_m, G_m, m, D, nu)
        #
        w += ((-D_m * G_m * mpia * y * np.cosh(mpia*y) + 
               (G_m + mpia*y) * D_m * np.sinh(mpia*y)) * np.sin(mpia*x))
        #
        sigma_x += (D_m * np.power(mpia, 2) * np.sin(x * mpia) *
                    (np.cosh(alpha_m) * (G_m * alpha_m + 2 * nu - nu * alpha_m * G_m) +
                     np.sinh(alpha_m) * (-G_m - alpha_m - nu * G_m + nu * alpha_m)))
        #
        sigma_y += (D_m * np.power(mpia, 2) * np.sin(x * mpia) *
                    (np.sinh(alpha_m) * (-G_m + alpha_m - nu * G_m - nu * alpha_m) +
                     np.cosh(alpha_m) * (2 - alpha_m * G_m + nu * G_m * alpha_m)))
        #
        sigma_xy += (D_m * np.power(mpia, 2) * np.cos(x * mpia) *
                     (np.sinh(alpha_m) * (-G_m * alpha_m + 1.0)  + alpha_m * np.cosh(alpha_m)))
    #
    w *= D / (F_0 * b**2)                                                      # @hami2230 - not sure where D / (F.b^2) comes from
    sigma_x *=  (E * t / (2* (1 - nu**2)))
    sigma_y *=  (E * t / (2* (1 - nu**2)))
    sigma_xy *=  (E * t / (2* (1 - nu)))
    return w , sigma_x, sigma_y, sigma_xy
#
def Timoshenko_Zhiling(F_0, s, b, a, t, E, nu=0.30):
    """
    Solution for infinitely long plate fixed at one edge and
    free at the other edge with a udl load 
    at the free edge 
    """
    #pass
    #
    # Define "x" range.
    #F_0 = 1.0
    #s = 0.01  # mm
    #
    #E = 210_000
    #t = 10.
    #
    #b = 1000.0
    #a = 4000.0
    #
    # Loc 1 - Middle of Free Edge
    y1 = b                # @hami2230 - updated to evaluate at centre of free edge and fixed edge
    x1 = a /2.0
    #
    #nu = 0.30
    #
    W1, sigma_x1, sigma_y1, sigma_xy1 = w_xy(x1.value, y1.value, 
                                         F_0.value, s.value, 
                                         b.value, a.value, 
                                         t.value, E.value, nu)
    print("")
    print("Middle of Free Edge")
    print(' w = {:1.4e}'.format(W1))
    print(' sigma_x = {:1.4e}'.format(sigma_x1))
    print(' sigma_y = {:1.4e}'.format(sigma_y1))
    print(' sigma_xy = {:1.4e}'.format(sigma_xy1))
    #
    # Loc 2 - Middle of Fixed Edge
    y2 = 0 * b
    x2 = a /2.0
    #
    #nu = 0.30
    #
    W2, sigma_x2, sigma_y2, sigma_xy2 = w_xy(x2.value, y2.value, 
                                         F_0.value, s.value, 
                                         b.value, a.value, 
                                         t.value, E.value, nu)
    print("")
    print("Middle of Fixed Edge")
    print(' w = {:1.4e}'.format(W2))
    print(' sigma_x = {:1.4e}'.format(sigma_x2))
    print(' sigma_y = {:1.4e}'.format(sigma_y2))
    print(' sigma_xy = {:1.4e}'.format(sigma_xy2))