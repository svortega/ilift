# 
# Copyright (c) 2019 iLift
# 

# Python stdlib imports

# package imports
from iLift.beam.section.plate.solid import Rectangle
from iLift.codes.design import DesignProperties

#
class Plate:
    """ """
    
    def __init__(self, material, units):
        """
        """
        self.L = None
        #self._simple = SimpleSupported(self, material, units)
        #self._cantilever = Cantilevered(self, material, units)
        # design defaults
        self._plate = Rectangle(material, units)
        #self._ibeam = Ibeam(material, units)
        self._design = DesignProperties(units)
        self._design.section = self._plate
        self._design.material = material
        #
        #self._element:Dict = {}
    #
    @property
    def plate(self):
        """
        """
        return self._plate
    #
    #@property
    #def simple_suported(self):
    #    """
    #    """
    #    return self._simple
    #
    #@property
    #def cantilever(self):
    #    """
    #    """
    #    return self._cantilever
    #
    @property
    def design(self):
        """
        """
        return self._design
    #
    #def code_check(self):
    #    """
    #    code check beam elements 
    #    """
    #    #
    #    print('-->')
    #    self._element[self._type] = self.design.AISC_WSD()
    ##
    #@property
    #def elements(self):
    #    """
    #    """
    #    if not self._element:
    #        self.code_check()
    #    return self._element
#
#