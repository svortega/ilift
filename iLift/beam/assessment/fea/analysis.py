# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports
from math import isclose, sqrt, copysign #aldh6850
from typing import NamedTuple, List, Dict, Tuple


# package imports
from iLift.anaStruct.fem.system import SystemElements
from iLift.beam.assessment.torsion.analysis import TorsionAISC
from iLift.beam.section.process.stress import Stress
from iLift.beam.assessment.process.process import tau


#
#
class NodalDisplacement(NamedTuple):
    """
    nodal displacement
    """
    ux:float
    uy:float
    uz:float
    torsion:float
#
class UDL(NamedTuple):
    """
    Force & bending moments
    """
    udl_x:float
    udl_y:float
    udl_z:float
#
class BeamFEA:
    
    def __init__(self, section, units):
        """
        """
        self._section = section
        self._material = section.material
        self._units = units
        #
        self._nodes:List = []
        self._supports:List = []
        self._cranes = {}
        #
        self._actions = None
        self._q_load = None
        self.Xe = 0 * units.m
    #
    @property
    def nodes(self):
        """
        return nodes
        """
        return self._nodes
    
    @nodes.setter
    def nodes(self, nodes:List):
        """
        nodes : coordinates[horizontal, vertical]
        """
        self._nodes = nodes
    #
    @property
    def cranes(self):
        """
        crane coordinates [horizontal, vertical]
        """
        return self._cranes
    
    @cranes.setter
    def cranes(self, cranes):
        """
        crane coordinates [horizontal, vertical]
        """
        self._cranes = cranes
        #self._cranes.extend(coord)
    #
    @property
    def section(self):
        """
        """
        return self._section
    
    @section.setter
    def section(self, section):
        """
        """
        self._section = section # Properties(*values)
    #
    @property
    def material(self):
        """
        """
        return self._material
    
    @material.setter
    def material(self, material):
        """
        """
        self._material = material
    #
    @property
    def supports(self):
        """
        """
        return self._supports
    
    @supports.setter
    def supports(self, supports:List):
        """
        """
        self._supports = supports
    #
    def torsional_stress_crane(self, cranes, coord):
        """
        Mt :  torsional moment
        La : distance to the moment from end 1
        coord  : beam coord stress points
        """
        #        
        _torsion = TorsionAISC(self._section, self._material, self._units)
        _tau = []
        for _crane in cranes.values():
            _torsion.moment(Tm = _crane.load.Mx, 
                            Xe = _crane.Xe, 
                            L = coord[-1])
            # [_rotation, _web, _flange, _normal, _warping]
            
            if 'free' in self._supports:
                _tau.append(_torsion.cantilever(coord))
                
            elif 'fixed' in self._supports:
                _tau.append(_torsion.fixed_ends(coord))
            
            else:
                _tau.append(_torsion.pinned_ends(coord))
        #
        return self._get_stress(_tau, coord)
    #
    def torsional_stress_point_load(self, actions, coord):
        """
        Mt :  torsional moment
        La : distance to the moment from end 1
        coord  : beam coord stress points
        """
        #        
        _torsion = TorsionAISC(self._section, self._material, self._units)
        _tau = []
        _torsion.moment(Tm = actions[3], 
                        Xe = self.Xe, 
                        L = coord[-1])
        # [_rotation, _web, _flange, _normal, _warping]
        if 'free' in self._supports:
            _tau.append(_torsion.cantilever(coord))
        
        elif 'fixed' in self._supports:
            _tau.append(_torsion.fixed_ends(coord))
        
        else:
            _tau.append(_torsion.pinned_ends(coord))
        
        return self._get_stress(_tau, coord)
    #
    def _get_stress(self, _tau, coord):
        """
        """
        _rotation = {}
        _web = {}
        _flange = {}
        _normal = {}
        _warping = {}
        for _items in _tau:
            for x, _coord in enumerate(coord):
                try:
                    _rotation[_coord.value] += _items[0][x]
                    _web[_coord.value] += _items[1][x]
                    _flange[_coord.value] += _items[2][x]
                    _normal[_coord.value] += _items[3][x]
                    _warping[_coord.value] += _items[4][x]
                except KeyError:
                    _rotation[_coord.value] = _items[0][x]
                    _web[_coord.value] = _items[1][x]
                    _flange[_coord.value] = _items[2][x]
                    _normal[_coord.value] = _items[3][x]
                    _warping[_coord.value] = _items[4][x]
        #
        return tau(_rotation, _web, _flange, _normal, _warping)
    #
    @property
    def uniform_load(self):
        """
        udlx
        udly
        udlz
        """
        return self._q_load
    
    @uniform_load.setter
    def uniform_load(self, udl):
        """
        udlx
        udly
        udlz
        """
        self._q_load = UDL(*udl)
    #
    @property
    def actions(self):
        """
        Fx = Force in-plane
        Fy = Force out-plane
        Fz = Force vertical
        Mx = Torsion
        My = Moment (in-plane)
        Mz = Moment (out-plane)
        """
        return self._actions
    
    @actions.setter
    def actions(self, forces):
        """
        Fx = Force in-plane
        Fy = Force out-plane
        Fz = Force vertical
        Mx = Torsion
        My = Moment (in-plane)
        Mz = Moment (out-plane)
        """
        self._actions = forces    
    #
    def _get_crane_results(self, action_index, EA, EI):
        """
        """
        _model, q_member = set_beam_model(self._nodes, self.supports, EA, EI)
        #
        for key, _crane in self._cranes.items():
            _sign = (-1)**key
            for x, _node in enumerate(self._nodes):
                if _crane.Xe == _node:
                    _model.point_load(node_id = x + 1,
                                      Fx= (-_sign * (action_index - 1.0) 
                                           * _crane.load.Fx.convert('kilonewton').value), 
                                      Fz= -_crane.load[action_index].convert('kilonewton').value)
                    
                    _model.moment_load(node_id = x + 1, 
                                       Ty= (-_sign * (action_index - 1.0) 
                                            * _crane.load.My.convert('kilonewton * metre').value)) #aldh6850
                    # print(-_sign * (action_index - 1.0) * _crane.load.My.convert('kilonewton * metre').value) #aldh6850
        #
        #_model.show_structure()
        #
        return beam_assessment(_model, q_member, self._nodes, self._units)
    #
    def _get_actions_results(self, action_index, EA, EI):
        """
        """
        _model, q_member = set_beam_model(self._nodes, self.supports, EA, EI)
        #
        _sign = -1
        for x, _node in enumerate(self._nodes):
            if self.Xe == _node:
                _model.point_load(node_id = x + 1,
                                  Fx= (-_sign * (action_index - 1.0) 
                                       * self._actions[0].convert('kilonewton').value), 
                                  Fz= -self._actions[action_index].convert('kilonewton').value)
                
                _model.moment_load(node_id = x + 1, 
                                   Ty= (-_sign * (action_index - 1.0) 
                                        * self._actions[4].convert('kilonewton * metre').value)) #aldh6850   
        #
        #_model.show_structure()
        #
        return beam_assessment(_model, q_member, self._nodes, self._units)        
    #
    def _get_udl_results(self, action_index, EA, EI):
        """
        """
        _model, q_member = set_beam_model(self._nodes, self.supports, EA, EI)
        # udl loading
        _model.q_load(element_id=q_member, 
                      q=-self.uniform_load[action_index].convert('kilonewton/metre').value)
        #
        #_model.show_structure()
        return beam_assessment(_model, q_member, self._nodes, self._units)    
    #
    def solve(self):
        """
        """
        E = self.material.E
        A = self.section.area
        Iip = self.section.Iy
        Iop = self.section.Iz
        #
        if self._cranes:
            # actions form crane loading 
            _actions_op, _crane_disp_op = self._get_crane_results(action_index = 1,
                                                                  EA = E*A, EI = E*Iop)
            
            _actions_ip, _crane_disp_ip = self._get_crane_results(action_index = 2,
                                                                  EA = E*A, EI = E*Iip)
            # calculating torsional stress
            _sigma_T = self.torsional_stress_crane(self._cranes, self._nodes)
        else:
            # point load direct on beam
            _actions_op, _crane_disp_op = self._get_actions_results(action_index = 1,
                                                                    EA = E*A, EI = E*Iop)            
            
            _actions_ip, _crane_disp_ip = self._get_actions_results(action_index = 2,
                                                                    EA = E*A, EI = E*Iip)
            # calculating torsional stress
            _sigma_T = self.torsional_stress_point_load(self._actions, self._nodes)
        #
        # actions from udl load
        _udl_actions_op, _udl_disp_op = self._get_udl_results(action_index = 1,
                                                              EA = E*A, EI = E*Iop)
        #
        _udl_actions_ip, _udl_disp_ip = self._get_udl_results(action_index = 2,
                                                              EA = E*A, EI = E*Iip)
        #
        # combine actions
        _actions = {}
        _crane_position = [_crane.Xe.value for _crane in self._cranes.values()]
        for x in range(len(_actions_ip)):
            member_node = [self._nodes[x].value, self._nodes[x+1].value]
            _index = list(set(member_node).intersection(_crane_position))
            if _index:
                _index = _crane_position.index(_index[0])
                _Mx = self._cranes[_index+1].load.Mx
            else:
                _Mx = 0 * self._units.kN * self._units.m
            #
            _actions[x+1] = Forces(Fx = (_actions_ip[x].Fx 
                                         + _actions_op[x].Fx 
                                         + _udl_actions_ip[x].Fx
                                         + _udl_actions_op[x].Fx),
                                   Fy = _actions_op[x].Fz + _udl_actions_op[x].Fz,
                                   Fz = _actions_ip[x].Fz + _udl_actions_ip[x].Fz,
                                   Mx = _Mx,
                                   My = _actions_ip[x].My + _udl_actions_ip[x].My,
                                   Mz = _actions_op[x].My + _udl_actions_op[x].My)
        #
        # combining global bending + torsional stress
        beam_stress = {}
        for x, _action in _actions.items():
            member_node = [self._nodes[x-1].value, self._nodes[x].value]
            _stress_T = Stress(sigma_x= 0 * self._units.MPa, # axial
                               sigma_y= 0 * self._units.MPa, # bending in-plane
                               sigma_z= get_max([_sigma_T.normal[member_node[0]], 
                                                 _sigma_T.normal[member_node[1]]]), # bending out-plane
                               tau_x= 0 * self._units.MPa, # torsion
                               tau_y= get_max([_sigma_T.flange[member_node[0]]
                                               + copysign(1, _sigma_T.flange[member_node[0]].value)
                                               * abs(_sigma_T.warping[member_node[0]]),
                                               _sigma_T.flange[member_node[1]]
                                               + copysign(1, _sigma_T.flange[member_node[1]].value)
                                               * abs(_sigma_T.warping[member_node[1]])]), # shear horizontal
                               tau_z= get_max([_sigma_T.web[member_node[0]],
                                               _sigma_T.web[member_node[1]]])) # shear vertical
            #
            beam_stress[x] = self.section.stress(_action, _stress_T)
            #
            # Collect bending stress for local checks
            try:
                _index = _crane_position.index(self._nodes[x].value)
                _crane = self._cranes[_index+1]
                _crane._stress = beam_stress[x]
            except ValueError:
                continue
        #
        #
        # Collect bending stress for local checks
        #member_node = [_item.value for _item in self._nodes]
        #for key, _crane in self._cranes.items():
        #    _index = member_node.index(_crane.Xe.value)
        #    try:
        #        _crane._stress = beam_stress[_index+1]
        #        print('--1', _index+1)
        #    except KeyError:
        #        _crane._stress = beam_stress[_index]
        #        print('--2', _index)
        #
        # combine global displacement
        _displacement = {}
        for _node in self._nodes:
            _displacement[_node.value] = NodalDisplacement(ux = (_crane_disp_ip[_node.value].ux 
                                                                 + _udl_disp_ip[_node.value].ux
                                                                 + _crane_disp_op[_node.value].ux
                                                                 + _udl_disp_op[_node.value].ux),   # axial
                                                           uy = (_crane_disp_op[_node.value].uz 
                                                                 + _udl_disp_op[_node.value].uz),  # out-plane
                                                           uz = (_crane_disp_ip[_node.value].uz 
                                                                 + _udl_disp_ip[_node.value].uz),   # in-plane 
                                                           torsion = _sigma_T.rotation[_node.value].value * self._units.radians)  # torsion    
        #
        return _actions, beam_stress, _displacement
#
#
#@dataclass
class BeamElement(NamedTuple):
    """
    """
    compactness:Tuple
    actions:Tuple
    stress:Tuple
    code_check:Tuple
    node1:Tuple
    node2:Tuple
#
class Forces(NamedTuple):
    """
    Force & bending moments
    """
    Fx:float
    Fy:float
    Fz:float
    Mx:float
    My:float
    Mz:float
#
def set_beam_model(nodes, supports, EA, EI):
    """
    coord = []
    supports [end1, end2],  end[free, pinned, fixed]
    
      1   2   3   4    elements
    +===+===+===+===+
    1   2   3   4   5  coord
    """
    _model = SystemElements(EA=EA.convert('kilonewton').value,
                            EI=EI.convert('kilonewton*metre^2').value)
    #
    q_member = []
    _steps = len(nodes) - 1
    for x in range(_steps):
        _model.add_element(location=[[nodes[x].value, 0],
                                     [nodes[x+1].value, 0]])
        q_member.append(x+1)
    #
    #
    _support = {'pinned': _model.add_support_hinged,
                'fixed': _model.add_support_fixed}
    # Support end 1
    try:
        _support[supports[0]](node_id=1)
    except KeyError:
        pass
    # Support end 2
    try:
        _support[supports[1]](node_id=5)
    except KeyError:
        pass
    #
    return _model, q_member
#
def beam_assessment(model, q_member, nodes, units):
    """
    """
    #
    model.solve()
    node_results = model.get_node_displacements(node_id=0)
    member_results = model.get_element_results(element_id=0)
    #
    Mmax = [max(abs(item['Mmax']), abs(item['Mmin'])) 
            for item in member_results]
    Smax = [max(abs(item['shear_1']), abs(item['shear_2'])) 
            for item in member_results]
    Fxmax = [max(abs(item['N_1']), abs(item['N_2'])) 
             for item in member_results]        
    #
    #aldh6850 -------------------------------------
    #update to retain the signs of the forces and moments
    for x,item in enumerate(member_results):
        Mmax[x]=copysign(Mmax[x], item['Mmin'])
        if Mmax[x]==abs(item['Mmax']):
            Mmax[x]=copysign(Mmax[x], item['Mmax'])
        #else:
        #    Mmax[x]=copysign(Mmax[x], item['Mmin'])
        Smax[x]=copysign(Smax[x],item['shear_2'])
        if Smax[x]==abs(item['shear_1']):
            Smax[x]=copysign(Smax[x],item['shear_1'])
        #else:
        #    Smax[x]=copysign(Smax[x],item['shear_2'])
        Fxmax[x]=copysign(Fxmax[x],item['N_2'])
        if Fxmax[x]==abs(item['N_1']):
            Fxmax[x]=copysign(Fxmax[x],item['N_1'])
        #else:
        #    Fxmax[x]=copysign(Fxmax[x],item['N_2'])
    #aldh6850 -------------------------------------
    forces = []
    for x, member in enumerate(q_member):
        forces.append(Forces(Fx = Fxmax[x] * units.kN,
                             Fy = 0 * units.kN,
                             Fz = Smax[x] * units.kN,
                             Mx = 0 * units.kN * units.m,
                             My = Mmax[x] * units.kN * units.m,
                             Mz = 0 * units.kN * units.m))
    #
    displacement = {}
    for x, _node in enumerate(nodes):
        displacement[_node.value] = NodalDisplacement(ux = node_results[x][1] * units.m, # axial
                                                      uy = 0 * units.m, # out-plane
                                                      uz = node_results[x][2] * units.m, # in-plane
                                                      torsion = node_results[x][3] * units.radians) 
    #
    return forces, displacement
#
def get_max(items):
    """
    """
    _max = max(items)
    _min = min(items)
    if abs(_max) < abs(_min):
        return _min
    return _max
#
