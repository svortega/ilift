# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports
#from math import isclose, sqrt, copysign #aldh6850
from typing import NamedTuple, List, Dict, Tuple


# package imports
#


class tau(NamedTuple):
    """
    """
    rotation:Tuple
    web:Tuple
    flange:Tuple
    normal:Tuple
    warping:Tuple
#

#class BeamAnalysis:
#    
#    def __init__(self, units):
#        """
#        """
#        self._ur = 0.80
#        self._beam_support = 'simply_supported'
#        self._design = DesignProperties(units)
#        self._units = units
#        #
#        self.Xe = 0 * units.m