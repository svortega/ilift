# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports
from typing import NamedTuple, List


# package imports


class thetas(NamedTuple):
    """
    """
    theta:List
    theta_I:List
    theta_II:List
    theta_III:List
    span:List

