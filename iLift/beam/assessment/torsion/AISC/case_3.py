# 
# Copyright (c) 2019 iLift
#
#
# Python stdlib imports
#from typing import NamedTuple, List

# package imports
import numpy as np
from iLift.beam.assessment.torsion.process.process import thetas

#
#
#
class Case3:
    #
    def __init__(self, item):
        """
        """
        self._section = item._section
        self._material = item._material
        self.a = item.Xe
        self.span = item.L
    #
    # --------------
    # numpy
    # -------------
    def theta_function(self, T, z, alpha, a, L, GJ):
        """
        """
        #
        if z <= alpha * L:
            return (T*L/GJ
                    * ((1.0 - alpha) * z/L
                       + a/L * (np.sinh(alpha*L/a) / np.tanh(L/a)
                                - np.cosh(alpha*L/a))
                       * np.sinh(z/a)))
        else:
            return (T*L/GJ
                    * ((L - z) * alpha/L
                       + a/L * (np.sinh(alpha*L/a) / np.tanh(L/a) * np.sinh(z/a)
                                - np.sinh(alpha*L/a) * np.cosh(z/a))))
    #
    def get_thetas(self, Pu):
        """
        """
        alpha = self.a / self.span
        #print('alpha = {: 1.2f}'.format(alpha.value))
        #
        L =  self.span
        GJ = self._material.G * self._section.J
        a = (self._material.E * self._section.Cw / GJ)**0.50
        #print('a   = {:1.2f} mm'.format(a.convert('millimetre').value))
        #print('L/a = {:1.2f}'.format(self.span.value / a.value))
        #        
        #z_vals = np.linspace(0, self.span, self.span * 10)
        z_vals = np.linspace(0, L.value, int(L.convert('millimetre').value))
        # theta
        #theta_np_0 = [self.theta_function(276201, z, alpha.value, a.value, L.value, GJ.value)
        theta_np_0 = np.array([self.theta_function(Pu.value, z, alpha.value, 
                                                   a.value, L.value, GJ.value)
                               for z in z_vals])
        #theta_plot = [theta_* (self._material.G*self._section.J/Pu) * (1/1000 * self.span)
        #              for theta_ in theta_np_0]
        #mpl.plot(z_vals/(1000 * self.span), theta_plot)
        #mpl.plot(z_vals, theta_np_0)
        #mpl.plot(theta_np_0 * (GJ.value/Pu.value) * 1.0/L.value)
        #mpl.show()
        # theta'
        theta_np_1 = np.diff(theta_np_0)
        #mpl.plot(theta_np_1 * (GJ.convert('newton*millimetre^2').value/Pu.convert('newton * millimetre').value))
        #mpl.show()        
        # theta''
        theta_np_2 = np.diff(theta_np_1)
        #mpl.plot(theta_np_2 * (GJ.convert('newton*millimetre^2').value/Pu.convert('newton * millimetre').value) *
        #         a.convert('millimetre').value)
        #mpl.show()
        # theta'''
        #mpl.plot(theta_np_3 * (GJ.convert('newton*millimetre^2').value/Pu.convert('newton * millimetre').value) *
        #         a.convert('millimetre').value**2)
        #mpl.show()        
        #
        # @hami2230
        theta_np_3_a = np.diff(theta_np_2)
        index = theta_np_3_a.argmin()
        theta_np_3 = []
        #
        for n,z in enumerate(theta_np_3_a):
            if n < (index - 3):
                theta_np_3.append(z)
            elif n > (index + 3):
                theta_np_3.append(z)
            else:
                theta_np_3.append(0.5 * Pu.convert('newton * millimetre').value 
                                  / ((self._material.G.convert('megapascal').value) 
                                     * (self._section.J.convert('millimetre^4').value) 
                                     * (a.convert('millimetre').value**2)))
        return thetas(theta_np_0, theta_np_1, theta_np_2, theta_np_3, z_vals)
#