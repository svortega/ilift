# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports
#from typing import NamedTuple, List

# package imports
import numpy as np
from iLift.beam.assessment.torsion.process.process import thetas

#
#
#
class Case9:
    #
    def __init__(self, item):
        """
        """
        self._section = item._section
        self._material = item._material
        self.a = item.Xe
        self.span = item.L
    # --------------
    # numpy
    # -------------
    def theta_function(self, T, z, alpha, a, L, GJ):
        """
        """
        #
        #L =  self.span* 1000
        #J = self._beam.Isection.J
        #G = self._material.G
        # Torsional constant
        #a = self.a
    
        #def theta_np(z, alpha):
        #    """
        #    """
        if z <= alpha * L:
            return (T*a/GJ
                    * ((np.sinh(alpha*L/a) 
                        - np.tanh(L/a) * np.cosh(alpha*L/a) 
                        + np.tanh(L/a))
                       * (np.cosh(z/a) - 1.0) 
                       - np.sinh(z/a) + z/a))
        else:
            return (T*a/GJ
                    * ((np.tanh(L/a) * np.cosh(alpha*L/a) 
                        - np.tanh(L/a) - np.sinh(alpha*L/a))
                       - (np.cosh(alpha*L/a) - 1.0) 
                       * (np.tanh(L/a) * np.cosh(z/a))
                       + (np.cosh(alpha*L/a) - 1.0) * np.sinh(z/a)
                       + alpha*L/a))
    #
    #
    def get_thetas(self, Pu):
        """
        """
        #
        alpha = self.a / self.span
        #print('alpha = {: 1.2f}'.format(alpha.value))
        #
        L =  self.span
        GJ = self._material.G * self._section.J
        a = (self._material.E * self._section.Cw / GJ)**0.50
        #print('a   = {:1.2f} mm'.format(a.convert('millimetre').value))
        #print('L/a = {:1.2f}'.format(self.span.value / a.value))        
        #        
        # theta
        z_vals = np.linspace(0, L.value, int(L.convert('millimetre').value))
        #z_vals = np.linspace(0, L, L)
        #
        theta_np_0 = np.array([self.theta_function(Pu.value, z, alpha.value,
                                                   a.value, L.value, GJ.value) 
                               for z in z_vals])
        #theta_plot = [theta_* (G*J/T) * (1/a) for theta_ in theta_np_0]
        #mpl.plot(z_vals/L, theta_plot)
        
        #
        # theta'
        
        theta_np_1 = np.diff(theta_np_0)
        #L1 = len(theta_np_1)
        #z_vals = np.linspace(0, L1, L1)
        #mpl.plot(z_vals/L1, theta_np_1 * (G*J/T))
        
        #
        # theta''
        theta_np_2 = np.diff(theta_np_1)
        #L2 = len(theta_np_2)
        #z_vals = np.linspace(0, L2, L2)
        #mpl.plot(z_vals/L2, theta_np_2 * (G*J/T) * a)
        
        #
        # theta'''
        theta_np_3 = np.diff(theta_np_2)
        #L3 = len(theta_np_3)
        #z_vals = np.linspace(0, L3, L3)
        #mpl.plot(z_vals/L3, theta_np_3 * (G*J/T) * a**2)
        
        #mpl.ylabel("Speed")
        #mpl.show()
        return thetas(theta_np_0, theta_np_1, theta_np_2, theta_np_3, z_vals)