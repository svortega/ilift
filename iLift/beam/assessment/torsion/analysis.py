# 
# Copyright (c) 2019 iLift
#
#
# Python stdlib imports
from typing import NamedTuple, List, Tuple

# package imports
from iLift.beam.assessment.torsion.roark.table103_7ed import Table_103

#
#
class Torsional:
    """
    """
    def __init__(self, beam_section, material, units):
        """
        """
        self._section = beam_section
        self._material = material
        self.T = 0 * units.N * units.m
        self.Xe = 0 * units.m
        self._case = None
        self._units = units
    #
    @property
    def units(self):
        """
        """
        return self._units
    #
    #@property
    #def moment(self):
    #    """
    #    """
    #    return self.T 
    #@moment.setter
    def moment(self, Tm:float, Xe:float, L:float):
        """
        T :  torsional moment
        Xe : distance to the moment from end 1
        L  : beam span
        """
        self.T  = Tm
        self.Xe = Xe
        self.L = L    
#
#
class TorsionAISC(Torsional):
    
    def __init__(self, beam_section, material, units):
        """
        """
        Torsional.__init__(self, beam_section, material, units)
    #
    def fixed_ends(self, coord):
        """
        """
        from iLift.beam.assessment.torsion.AISC.case_6 import Case6
        _theta = Case6(self)
        _thetas = _theta.get_thetas(self.T)
        #
        #idx1 = np.searchsorted(_thetas[4], self.Xe)
        #idx = (np.abs(_thetas[4] - 1000 * self.Xe)).argmin()
        #
        self._case = 'fixed_ends'
        return self._get_stress(_thetas, self.L, coord)    
    #
    def pinned_ends(self, coord):
        """
        """
        from iLift.beam.assessment.torsion.AISC.case_3 import Case3
        _theta = Case3(self)
        _thetas = _theta.get_thetas(self.T)
        #
        #idx1 = np.searchsorted(_thetas[4], self.Xe)
        #idx = (np.abs(_thetas[4] - 1000 * self.Xe)).argmin()
        #
        self._case = 'pinned_ends'
        return self._get_stress(_thetas, self.L, coord)
    #
    def cantilever(self, coord):
        """
        """
        from iLift.beam.assessment.torsion.AISC.case_9 import Case9
        _theta = Case9(self)
        _thetas = _theta.get_thetas(self.T)
        self._case = 'cantilever'
        return self._get_stress(_thetas, self.L, coord)
    #
    def _get_stress(self, theta:Tuple, L:float, coord:List):
        """
        """
        #
        import numpy as np
        #
        _rotation = []
        _web = []
        _flange = []
        _normal = []
        _warping = []
        #
        _span = coord[-1].value
        for _node in coord:
            zl = _node.value / _span
            #print('a/l', zl)
            # rotation
            _length = len(theta[0])
            x_axis = np.linspace(0, 1, _length)
            idx = (np.abs(x_axis - zl)).argmin()
            _rotation.append(theta[0][idx] * self.T * L / (self._material.G * self._section.J))
            # theta 1 (shear stress)
            _length = len(theta[1])
            x_axis = np.linspace(0, 1, _length)
            idx = (np.abs(x_axis - zl)).argmin()
            _web.append(self._material.G.convert('megapascal').value *
                        self._section.tw.convert('millimetre').value * 
                        theta[1][idx] * self.units.MPa)
            #
            _flange.append(self._material.G.convert('megapascal').value * 
                           self._section.tf.convert('millimetre').value * 
                           theta[1][idx] * self.units.MPa)
            # theta 2 (normal stress due to warping)
            _length = len(theta[2])
            x_axis = np.linspace(0, 1, _length)
            idx = (np.abs(x_axis - zl)).argmin()
            _normal.append(self._material.E.convert('megapascal').value * 
                           self._section.Wns.convert('millimetre^2').value *
                           theta[2][idx] * self.units.MPa)
            # theta 3 (shear stress due to warping)
            _length = len(theta[3])
            x_axis = np.linspace(0, 1, _length)
            idx = (np.abs(x_axis - zl)).argmin()
            #if 'pinned_ends' in self._case:
            #    idx += 2
            _warping.append(self._material.E.convert('megapascal').value  * self.units.MPa * 
                            self._section.Sws.convert('millimetre^4').value *
                            theta[3][idx] / self._section.tf.convert('millimetre').value)
        #
        return [_rotation, _web, _flange, _normal, _warping]
#
#
class TorsionROARK(Torsional):
    
    def __init__(self, beam_section, material, units):
        """
        """
        Torsional.__init__(self, beam_section, material, units)
    #
    def fixed_ends(self, coord):
        """
        """
        #print('fixed')
        _theta = Table_103(self)
        _theta.beam_support = 'fixed'
        _thetas = _theta.get_thetas(self.T, coord)
        self._case = 'fixed_ends'
        return self._get_stress(_thetas, self.L, coord)        
    #
    def pinned_ends(self, coord):
        """
        """
        _theta = Table_103(self)
        _theta.beam_support = 'pinned'
        _thetas = _theta.get_thetas(self.T, coord)
        self._case = 'pinned_ends'
        return self._get_stress(_thetas, self.L, coord)
    #
    def cantilever(self, coord):
        """
        """
        #print('cantilever')
        _theta = Table_103(self)
        _theta.beam_support = 'cantilever'
        _thetas = _theta.get_thetas(self.T, coord)
        self._case = 'cantilever'
        return self._get_stress(_thetas, self.L, coord)        
    #
    def _get_stress(self, theta:Tuple, L:float, coord:List):
        """
        """
        # print('get stress')
        _rotation = []
        _web = []
        _flange = []
        _normal = []
        _warping = []
        #_t = min(self._section.tw, self._section.tf)
        _span = coord[-1].value
        #
        for x, _node in enumerate(coord):
            _rotation.append(theta[0][x])
            # shear stress
            _web.append(theta[1][x] * self._material.G * self._section.tw.value)
            _flange.append(theta[1][x] * self._material.G * self._section.tf.value)
            # normal stress due to warping
            _normal.append(theta[2][x] * self._section.d.value 
                           * self._section.bf.value * self._material.E / 4.0)
            # shear stress due to warping
            _warping.append(theta[3][x] * self._section.d.value 
                           * self._section.bf.value**2 * self._material.E / 16.0)
        #
        return [_rotation, _web, _flange, _normal, _warping]
    #