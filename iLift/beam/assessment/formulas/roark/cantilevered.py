# 
# Copyright (c) 2018 steelpy
#
# Python stdlib imports

# package imports
from iLift.beam.assessment.torsion.analysis import TorsionROARK


#
#
class Cantilevered_Point:
    #
    def __init__(self, beam, material, units):
        """
        """
        self._beam = beam
        self._material = material
        self.units = units
    #
    @property
    def material(self):
        """
        """
        return self._material
    @material.setter
    def material(self, material):
        """
        """
        self._material = material    
    #
    def get_reactions(self, W: float):
        """
        W : Vertical point load
        a : distance to the load from support 1
        L : span 
        ----
        return R1, R2
        """
        return W, W*0
    #
    def get_maximum_moment(self, W: float):
        """
        W : Vertical point load
        a : cantilever length
        L : span 
        ----
        return maximum moment at x 
        """
        return 0, -1 * W * self.a
    #
    def get_maximum_shear(self, W:float):
        """
        W : Vertical point load
        a : distance to the load from support 1
        L : span between support
        ----
        return maximum shear at x
        """
        return W
    #
    def get_maximum_deflection(self, W: float):
        """
        x : distance from support 1
        W : Vertical point load
        a : distance to the load from support 1
        Lc : span 
        E : Elastic modulus
        I : Second moment of inertia
        ----
        return deflection at x
        """
        return (self.a, W * self.a**3 *
                (3*self.L - self.a) 
                / (6 * self.material.E * self._beam.section.I))
    #
    def get_torsional_stress(self, Pu:float):
        """
        """
        if not self._material:
            raise ValueError('    *** error material not found')
        #
        _torsion = TorsionROARK(self._beam, self._material, self.units)
        _torsion.moment = Pu
        _tau= _torsion.cantilever
        #
        return _tau
    #
    @property
    def span(self):
        """
        L : Beam span or cantil
        """
        return self.L
    @span.setter
    def span(self, value:float) -> None:
        """
        L : Beam span or cantil
        """
        self.L = value
        self.a = value
#
class Cantilevered_UDL:
    #
    def __init__(self, beam, material, units):
        """
        """
        self._beam = beam
        self._material = material
        self.units = units
    #
    @property
    def material(self):
        """
        """
        return self._material
    @material.setter
    def material(self, material):
        """
        """
        self._material = material    
    #
    def get_reactions(self, w: float):
        """
        w : Force per unit length
        a : cantilever length
        L : span 
        ----
        return R1, R2
        """
        R = w * self.a
        return R, R * 0
    
    def get_maximum_moment(self, w: float):
        """
        w : Force per unit length
        a : cantilever length
        L : span 
        ----
        return maximum moment at x 
        """
        return 0, -1 * w * self.a**2 / 2.0
    #
    def get_maximum_shear(self, w:float):
        """
        w : Force per unit length
        a : cantilever length
        L : span 
        ----
        return maximum shear at x
        """
        return w * self.a
    #
    @property
    def span(self):
        """
        L : Beam span or cantil
        """
        return self.L
    @span.setter
    def span(self, value:float) -> None:
        """
        L : Beam span or cantil
        """
        self.L = value
        self.a = value
#
#
class Overhanging:
    """
    """
    def __init__(self, beam, material, units):
        """
        """
        self._beam = beam
        self._material = material
        self.units = units
    #
    @property
    def material(self):
        """
        """
        return self._material
    @material.setter
    def material(self, material):
        """
        """
        self._material = material
    #
    def get_reactions(self, W: float):
        """
        W : Vertical point load
        a : distance to the load from support 1
        L : span 
        ----
        return R1
        """
        return -W * self.a / self.L, W * (self.a + self.L) / self.L
    #
    def get_maximum_shear(self, W:float):
        """
        W : Vertical point load
        a : distance to the load from support 1
        L : span between support
        ----
        return maximum shear at x
        """
        return W
    #
    def get_maximum_moment(self, W: float):
        """
        W : Vertical point load
        a : distance to the load from support 1
        L : span 
        ----
        return maximum moment at x 
        """
        return 0, -1 * W * self.a
    #
    def get_torsional_stress(self, Pu:float):
        """
        """
        if not self._material:
            raise ValueError('    *** error material not found')
        #
        _torsion = TorsionROARK(self._beam, self._material, self.units)
        _torsion.moment = Pu
        _tau= _torsion.cantilever
        #
        return _tau
    #
    @property
    def span(self):
        """
        L : Beam span or cantil
        """
        return self.L
    @span.setter
    def span(self, value:float) -> None:
        """
        L : Beam span or cantil
        """
        self.L = value
        self.a = value
    
    