# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports

# package imports
from iLift.beam.assessment.torsion.analysis import TorsionROARK

#
class SimpleSupported_Point:
    #
    def __init__(self, beam, material, units):
        """
        """
        self._beam = beam
        self._material = material
        self.units = units
    #
    @property
    def material(self):
        """
        """
        return self._material
    @material.setter
    def material(self, material):
        """
        """
        self._material = material
    #
    def get_reactions(self, W:float):
        """
        W : Vertical point force
        a : distance to the load from support 1
        L : span between support
        ----
        return R1, R2
        """
        b = self.L - self.a
        return W*b/self.L, W*self.a/self.L
    #
    def get_deflections(self, x:float, W:float, E:float, I:float):
        """
        x : distance from support 1
        W : Vertical point load
        a : distance to the load from support 1
        L : span between support
        E : Elastic modulus
        I : Second moment of inertia
        ----
        return deflection at x
        """
        R1, R2 = self.get_reactions(W)
        EI = 6 * self._material.E * self._beam.section.I
        b = self.L - self.a

        if x < self.a:
            return R1 * (-x**3 + (self.L**2 - b**2) * x) / EI
        else:
            return R2 * (-(self.L - x)**3 + (self.L**2 - self.a**2) * (self.L - x)) / EI
    #
    def get_maximum_deflection(self, W:float, E:float, I:float):
        """
        x : distance from support 1
        W : Vertical point load
        a : distance to the load from support 1
        L : span between support
        E : Elastic modulus
        I : Second moment of inertia
        ----
        return deflection at x
        """
        b = self.L - self.a
        x = ((self.L**2 - b**2) / 3.0)**0.50
        if self.a > self.L/2.0 :
            return (x, W * b * 
                    (self.L**2 - b**2)**(3/2.0) /
                    (9 * (3)**0.50 * self.L * self._material.E * self._beam.section.I))
        else:
            1/0
    #
    def get_moment(self, x:float, W: float):
        """
        x : distance from support 1
        W : Vertical point load
        a : distance to the load from support 1
        L : span between support
        ----
        return moments at x
        """
        b = self.L - self.a
        if x < self.a:
            return W * b * x / self.L
        else:
            return W * self.a * (self.L - x) / self.L
    #
    def get_maximum_moment(self, W:float):
        """
        W : Vertical point load
        a : distance to the load from support 1
        L : span between support
        ----
        return maximum moment at x 
        """
        b = self.L - self.a
        return self.a, W * self.a * b / self.L
    #
    def get_shear(self, x:float, W:float):
        """
        x : distance from support 1
        W : Vertical point load
        a : distance to the load from support 1
        L : span between support
        ----
        return maximum shear at x
        """
        b = self.L - self.a
        if x > self.a > b:
            return  W * self.a / self.L
        else:
            return W * b / self.L
    #
    def get_maximum_shear(self, W:float):
        """
        W : Vertical point load
        a : distance to the load from support 1
        L : span between support
        ----
        return maximum shear at x
        """
        R1, R2 = self.get_reactions(W)
        b = self.L - self.a
        if self.a > b:
            return  R2
        else:
            return R1
    #
    def get_torsional_stress(self, Pu:float):
        """
        """
        if not self._material:
            raise ValueError('    *** error material not found')
        #
        _torsion = TorsionROARK(self._beam, self._material, self.units)
        _torsion.moment = Pu
        _tau= _torsion.pinned_ends
        #
        return _tau
    #
    @property
    def span(self):
        """
        L : Beam span or cantil
        """
        return self.L
    @span.setter
    def span(self, value:float) -> None:
        """
        L : Beam span or cantil
        """
        self.L = value
        self.a = value/2.0
#
class SimpleSupported_UDL:
    #
    def __init__(self, beam, material, units):
        """
        """
        self._beam = beam
        self._material = material
        self.units = units
    #
    @property
    def material(self):
        """
        """
        return self._material
    @material.setter
    def material(self, material):
        """
        """
        self._material = material
    
    def get_reactions(self, w:float):
        """
        w : Force per unit length
        L : span between support
        ----
        return R1, R2
        """
        return w*self.L/2.0, w*self.L/2.0
    
    def get_maximum_moment(self, w:float):
        """
        w : Force per unit length
        L : span between support
        ----
        x distance from end 1
        maximum moment at x 
        """
        return self.L/2, w * self.L**2 / 8.0
    #
    def get_maximum_shear(self, w:float):
        """
        W : Vertical point load
        L : span between support
        ----
        return maximum shear at x
        """
        R1, R2 = self.get_reactions(w)
        return max(R1,R2)
    #
    @property
    def span(self):
        """
        L : Beam span or cantil
        """
        return self.L
    @span.setter
    def span(self, value:float) -> None:
        """
        L : Beam span or cantil
        """
        self.L = value
        self.a = value/2.0    