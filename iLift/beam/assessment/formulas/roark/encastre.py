# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports

# package imports
from iLift.beam.assessment.torsion.analysis import TorsionROARK

#
class Encastre_Point:
    #
    def __init__(self, beam, material, units):
        """
        """
        self._beam = beam
        self._material = material
        self.units = units
    #
    @property
    def material(self):
        """
        """
        return self._material
    @material.setter
    def material(self, material):
        """
        """
        self._material = material
    #
    def get_reactions(self, W:float):
        """
        W : Vertical point force
        a : distance to the load from support 1
        L : span between support
        ----
        return R1, R2
        """
        b = self.L - self.a
        return W*(3*self.a + b) * b**2 / self.L**3, W*(self.a + 3*b) * self.a**2 / self.L**3
    #
    def get_deflections(self, x:float, W:float, E:float, I:float):
        """
        x : distance from support 1
        W : Vertical point load
        a : distance to the load from support 1
        L : span between support
        E : Elastic modulus
        I : Second moment of inertia
        ----
        return deflection at x
        """
        #R1, R2 = self.get_reactions(W)
        EI = 3 * self._material.E * self._beam.section.I
        b = self.L - self.a

        if x < self.a:
            return -W * b**2 * x**2 / (6 * self.L**3 * EI) * (3 * self.a * x + b*x - 3 * self.a * self.L)
        else:
            return -W * b**2 * x**2 / (6* self.L**3 * EI) * ((3*b + self.a)*(self.L-x) - 3*b*self.L)
    #
    def get_maximum_deflection(self, W:float, E:float, I:float):
        """
        x : distance from support 1
        W : Vertical point load
        a : distance to the load from support 1
        L : span between support
        E : Elastic modulus
        I : Second moment of inertia
        ----
        return deflection at x
        """
        b = self.L - self.a
        x = 2 * self.a * self.L / (self.L + 2* self.a)
        EI = 3 * self._material.E * self._beam.section.I
        
        if self.a > self.L/2.0 :
            return x, -2*W*(self.L - self.a)**2 * self.a**3 / (EI * (self.L + 2* self.a)**2)
        else:
            1/0
    #
    def get_moment(self, x:float, W:float):
        """
        x : distance from support 1
        W : Vertical point load
        a : distance to the load from support 1
        L : span between support
        ----
        return moments at x
        """
        b = self.L - self.a
        R1, R2 = self.get_reactions(W)
        
        if x < self.a:
            return - W * self.a * b**2 / self.L**2 + R1 * x 
        else:
            return - W * self.a * b**2 / self.L**2 + R1 * x - W * (x - self.a)
    #
    def get_maximum_moment(self, W:float):
        """
        W : Vertical point load
        a : distance to the load from support 1
        L : span between support
        ----
        return maximum moment at x 
        """
        b = self.L - self.a
        R1, R2 = self.get_reactions(W)
        return self.a, W * self.a * b**2 / self.L**2 - R1 * self.a
    #
    def get_shear(self, x:float, W:float):
        """
        x : distance from support 1
        W : Vertical point load
        a : distance to the load from support 1
        L : span between support
        ----
        return maximum shear at x
        """
        b = self.L - self.a
        
        if x > self.a > b:
            return  W / self.L**3 * (self.L - self.a)**2 * (self.L + 2 * self.a)
        else:
            return W * self.a**2 / self.L**3 * (3* self.L - 2 * self.a)
    #
    def get_maximum_shear(self, W:float):
        """
        W : Vertical point load
        a : distance to the load from support 1
        L : span between support
        ----
        return maximum shear at x
        """
        R1, R2 = self.get_reactions(W)
        b = self.L - self.a
        
        if self.a > b:
            return  R2
        else:
            return R1
    #
    def get_torsional_stress(self, Pu:float):
        """
        """
        if not self._material:
            raise ValueError('    *** error material not found')
        #
        _torsion = TorsionROARK(self._beam, self._material, self.units)
        _torsion.moment = Pu
        _tau= _torsion.pinned_ends
        #
        return _tau
    #
    @property
    def span(self):
        """
        L : Beam span or cantil
        """
        return self.L
    @span.setter
    def span(self, value:float) -> None:
        """
        L : Beam span or cantil
        """
        self.L = value
        self.a = value/2.0
#
class Encastre_UDL:
    #
    def __init__(self, beam, material, units):
        """
        """
        self._beam = beam
        self._material = material
        self.units = units
    #
    @property
    def material(self):
        """
        """
        return self._material
    @material.setter
    def material(self, material):
        """
        """
        self._material = material
    
    def get_reactions(self, w:float):
        """
        w : Force per unit length
        L : span between support
        ----
        return R1, R2
        """
        return w*self.L/2.0, w*self.L/2.0
    
    def get_maximum_moment(self, w:float):
        """
        w : Force per unit length
        L : span between support
        ----
        x distance from end 1
        maximum moment at x 
        """
        Mmid = w * self.L**2 / 24
        # self.L/2
        Mend = w * self.L**2 / 12
        return self.L, Mend
    #
    def get_maximum_shear(self, w:float):
        """
        W : Vertical point load
        L : span between support
        ----
        return maximum shear at x
        """
        R1, R2 = self.get_reactions(w)
        return max(R1,R2)
    #
    @property
    def span(self):
        """
        L : Beam span or cantil
        """
        return self.L
    @span.setter
    def span(self, value:float) -> None:
        """
        L : Beam span or cantil
        """
        self.L = value
        self.a = value/2.0    