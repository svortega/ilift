# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports
from typing import NamedTuple, List, Dict, Tuple, ClassVar
from math import isclose, copysign, sin

# package imports
from iLift.codes.design import DesignProperties
from iLift.beam.assessment.formulas.roark.simply_supported import (SimpleSupported_UDL, 
                                                                   SimpleSupported_Point)
from iLift.beam.assessment.formulas.roark.cantilevered import (Cantilevered_UDL,
                                                               Cantilevered_Point)
from iLift.beam.assessment.formulas.roark.encastre import (Encastre_UDL,
                                                           Encastre_Point)
import iLift.load.guess_beam_load as guess_load
from iLift.beam.section.process.stress import Stress
#from iLift.load.process.actions import Actions
from iLift.beam.assessment.torsion.analysis import TorsionROARK
from iLift.beam.assessment.process.process import tau
#
class BeamFormulas:
    
    def __init__(self, units):
        """
        """
        self._ur = 0.80
        self._beam_support = 'simply_supported'
        self._design = DesignProperties(units)
        self._units = units
        self._load_angle_lateral = 0 * units.deg
        self._load_angle_horizontal = 0 * units.deg
        #
        self._load_eccentricity_lateral = 0 * units.m
        self._load_eccentricity_vertical = 0 * units.m
        #
        self.Xe = 0 * units.m
        self._nodes:List = []
        self.load_factor:float = 1.0
    #
    @property
    def units(self):
        """
        """
        return self._units    
    #
    @property
    def nodes(self):
        """
        return nodes
        """
        return self._nodes
    
    @nodes.setter
    def nodes(self, nodes:List):
        """
        nodes : coordinates[horizontal, vertical]
        """
        self._nodes = nodes    
    #
    @property
    def UR(self):
        """
        UR: beam initial utilization ratio 
        """
        return self._ur
    
    @UR.setter
    def UR(self, value:float):
        """
        UR: beam initial utilization ratio 
        """
        self._ur = value
    #
    @property
    def load_lateral_angle(self):
        """
        """
        return self._load_angle_lateral
    
    @load_lateral_angle.setter
    def load_lateral_angle(self, value:ClassVar):
        """
        """
        self._load_angle_lateral = value
    #
    @property
    def load_horizontal_angle(self):
        """
        """
        return self._load_angle_horizontal
    
    @load_horizontal_angle.setter
    def load_horizontal_angle(self, value:ClassVar):
        """
        """
        self._load_angle_horizontal = value
    #
    #
    @property
    def load_lateral_eccentricity(self):
        """
        lateral load eccentricity
        """
        return self._load_eccentricity_lateral
    
    @load_lateral_eccentricity.setter
    def load_lateral_eccentricity(self, value:ClassVar):
        """
        """
        self._load_eccentricity_lateral = value
    #
    #
    @property
    def load_vertical_eccentricity(self):
        """
        lateral load eccentricity
        """
        return self._load_eccentricity_vertical
    
    @load_vertical_eccentricity.setter
    def load_vertical_eccentricity(self, value:ClassVar):
        """
        """
        self._load_eccentricity_vertical = value    
    #
    @property
    def beam_support(self):
        """
        """
        return self._beam_support
    
    @beam_support.setter
    def beam_support(self, support:List[str]):
        """
        """
        if 'free' in support:
            self._beam_support = 'cantilever'
        elif 'fixed' in support:
            self._beam_support = 'fixed_both_ends'
    
    #
    @property
    def section(self):
        """
        """
        return self._design.section
    
    @section.setter
    def section(self, value:ClassVar):
        """
        """
        self._design.section = value
    
    #
    @property
    def material(self):
        """
        """
        return self._design.material
    
    @material.setter
    def material(self, value:ClassVar):
        """
        """
        self._design.material = value
    
    #
    @property
    def beam_span(self):
        """
        """
        return self._design.member_length
    
    @beam_span.setter
    def beam_span(self, span:ClassVar):
        """
        """
        self._design.member_length = span
        self._design.lateral_braced_length = span
    #
    @property
    def design(self):
        """
        """
        return self._design
    #
    #
    def torsional_stress_point_load(self, actions, coord):
        """
        Mt :  torsional moment
        La : distance to the moment from end 1
        coord  : beam coord stress points
        """
        #
        _torsion = TorsionROARK(self.section, self.material, self.units)
        _tau = []
        _torsion.moment(Tm = actions.Mx, 
                        Xe = self.Xe, 
                        L = coord[-1])
        # [_rotation, _web, _flange, _normal, _warping]
        if 'free' in self.beam_support:
            _tau.append(_torsion.cantilever(coord))
        
        elif 'fixed' in self.beam_support:
            _tau.append(_torsion.fixed_ends(coord))
        
        else:
            _tau.append(_torsion.pinned_ends(coord))
        
        return self._get_stress(_tau, coord)    
    #
    def _get_stress(self, _tau, coord):
        """
        """
        _rotation = {}
        _web = {}
        _flange = {}
        _normal = {}
        _warping = {}
        for _items in _tau:
            for x, _coord in enumerate(coord):
                try:
                    _rotation[_coord.value] += _items[0][x]
                    _web[_coord.value] += _items[1][x]
                    _flange[_coord.value] += _items[2][x]
                    _normal[_coord.value] += _items[3][x]
                    _warping[_coord.value] += _items[4][x]
                except KeyError:
                    _rotation[_coord.value] = _items[0][x]
                    _web[_coord.value] = _items[1][x]
                    _flange[_coord.value] = _items[2][x]
                    _normal[_coord.value] = _items[3][x]
                    _warping[_coord.value] = _items[4][x]
        #
        return tau(_rotation, _web, _flange, _normal, _warping)    
    #
    @property
    def get_udl(self):
        """
        """
        print('------> Finding UDL for UR={:1.2f}, Span={:1.2f}m'
              .format(self.UR, self.beam_span.convert('metre').value))
        
        if 'cantilever' in self._beam_support:
            _beam = Cantilevered_UDL(self.section, self.material, self.units)
            w = guess_load.udl_cantilever(UR=self.UR, 
                                          L=self.beam_span, 
                                          Z=self.section.Zey, 
                                          Fy=self.material.Fy)
        
        elif 'fixed_both_ends' in self._beam_support:
            _beam = Encastre_UDL(self.section, self.material, self.units)
            w = guess_load.udl_fixed_both_ends(UR=self.UR, 
                                               L=self.beam_span, 
                                               Z=self.section.Zey, 
                                               Fy=self.material.Fy)
        
        elif 'simply_supported' in self._beam_support:
            _beam = SimpleSupported_UDL(self.section, self.material, self.units)
            w = guess_load.udl_simply_supported(UR=self.UR, 
                                                L=self.beam_span, 
                                                Z=self.section.Zey, 
                                                Fy=self.material.Fy)
        
        else:
            raise IOError(" Beam support {:} no yet implemented"
                          .format(self._beam_support))
        #
        _beam.span = self.beam_span
        #
        _udl = self._process_get_load(w, _beam)
        print('** Estimated UDL vertical load = {:1.2f} kN/m'
              .format(_udl[2].convert('kilonewton / metre').value))
        # return vertical load
        return _udl[2]
    #
    @property
    def get_point_load(self):
        """
        """
        print('------> Finding point load for UR={:1.2f}, x={:1.2f}m'
              .format(self.UR, self.Xe.convert('metre').value))
        
        if 'cantilever' in self._beam_support:
            _beam = Cantilevered_Point(self.section, self.material, self.units)
            p = guess_load.point_cantilever(UR=self.UR, 
                                            L=self.beam_span, 
                                            x=self.Xe,
                                            Z=self.section.Zey, 
                                            Fy=self.material.Fy)
            
        elif 'fixed_both_ends' in self._beam_support:
            _beam = Encastre_Point(self.section, self.material, self.units)
            p = guess_load.point_fixed_both_ends(UR=self.UR, 
                                                 L=self.beam_span, 
                                                 x=self.Xe,
                                                 Z=self.section.Zey, 
                                                 Fy=self.material.Fy)
        
        elif 'simply_supported' in self._beam_support:
            _beam = SimpleSupported_Point(self.section, self.material, self.units)
            p = guess_load.point_simply_supported(UR=self.UR, 
                                                  L=self.beam_span, 
                                                  x=self.Xe,
                                                  Z=self.section.Zey, 
                                                  Fy=self.material.Fy)
        
        else:
            raise IOError(" Beam support {:} no yet implemented"
                          .format(self._beam_support))
        #
        _beam.span = self.beam_span
        _beam.Xe = self.Xe
        _point = self._process_get_load(p, _beam, tol=0.01)
        #print('** Estimated Axial Load       = {: 1.2f} kN'
        #      .format(_point[0].convert('kilonewton').value))        
        #print('** Estimated Horizontal Load  = {: 1.2f} kN'
        #      .format(_point[1].convert('kilonewton').value))        
        print('** Estimated Point Vertical Load = {: 1.2f} kN'
              .format(_point[2].convert('kilonewton').value))
        #print('** Estimated Torsional Moment = {: 1.2f} kN-m'
        #      .format(_point[3].convert('kilonewton * metre').value))        
        # return vertical load
        return _point[2]
    #
    #
    def _process_get_load(self, guess_load:ClassVar, 
                          beam:ClassVar, tol=0.001):
        """
        """
        _load_Fz = guess_load
        _load_lateral_factor = sin(self.load_lateral_angle.value)
        _load_horizontal_factor = sin(self.load_horizontal_angle.value)
        #       
        #l = beam.span
        #a = l / 2.0
        #b = (l - a)
        #
        while True:
            x, Mby = beam.get_maximum_moment(_load_Fz)
            Vz = beam.get_maximum_shear(_load_Fz)
            Fz = beam.get_reactions(_load_Fz)
            # set load for either udl or point load
            Fy = Vz * _load_lateral_factor
            Fz = max(Vz, Fz[0], Fz[1]) * -1.0 # + upwards
            My = Mby
            Mz = Mby * _load_lateral_factor
            # get remainig load from point load
            try:
                Mx = (_load_Fz * self.load_vertical_eccentricity
                      + Fy * self.load_lateral_eccentricity)
                Fx = Vz * _load_horizontal_factor * -1.0 # in compression                
            except RuntimeError: # units incompatible error if udl
                Fx = 0 * self.units.N
                Mx = 0 * self.units.N * self.units.m
            # update actions
            self.design.actions.Fx = Fx 
            self.design.actions.Fy = Fy
            self.design.actions.Fz = Fz
            self.design.actions.Mx = Mx
            self.design.actions.My = My
            self.design.actions.Mz = Mz
            #
            if Mx.value == 0:
                self.design.stress = self.section.stress(self.design.actions)
            else:
                # calculating torsional stress
                #self.Xe  = a
                _sigma_T = self.torsional_stress_point_load(actions = self.design.actions, 
                                                            coord = self.nodes)
                #
                member_node = self.Xe.value
                _stress_T = Stress(sigma_x= 0 * self.units.MPa, # axial
                                   sigma_y= 0 * self.units.MPa, # bending in-plane
                                   sigma_z= _sigma_T.normal[member_node], # bending out-plane
                                   tau_x= 0 * self._units.MPa, # torsion
                                   tau_y= (_sigma_T.flange[member_node]
                                           + copysign(1, _sigma_T.flange[member_node].value)
                                           * abs(_sigma_T.warping[member_node])), # shear horizontal
                                   tau_z= _sigma_T.web[member_node]) # shear vertical                
                #
                #self.design.stress = self.section.stress(self.design.actions)
                self.design.stress = self.section.stress(self.design.actions, _stress_T) 
            #
            try:
                _results = self.design.AISC_WSD()
                _current_UR = _results.UR_results.UR
                if isclose(_current_UR, self.UR, abs_tol=tol):
                    break
                _delta = round(self.UR / _current_UR, 4)
                _load_Fz = _load_Fz * _delta
            except Warning as exception: # beam exceed slenderness parameter
                print(exception)
                _load_Fz = 0 * guess_load
                Fy = _load_Fz
                Fx = _load_Fz 
                Mx = 0 * self.units.N * self.units.m
                My = 0 * self.units.N * self.units.m
                Mz = 0 * self.units.N * self.units.m
                break 
        #
        _load_Fz /= self.load_factor
        return [Fx, Fy, _load_Fz, Mx, My, Mz]