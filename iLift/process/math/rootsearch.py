# Copyright (c) 2019 iLift

# Python stdlib imports
import math
import re

# package imports


#
#-------------------------------------------------
#                Supporting Section
#-------------------------------------------------
#
# module error
# 
def err(string):
    """ err(string).
    Prints string and terminates program.
    """
    print (string)
    raw_input("Press return to exit")
    exit()
#
#
# module rootsearch
#
def rootsearch(f, a, b, dx):
    """ x1,x2 = rootsearch(f,a,b,dx).
    Searches the interval (a,b) in increments dx for
    the bounds (x1,x2) of the smallest root of f(x).
    Returns x1 = x2 = None if no roots were detected.
    """
    x1 = a
    f1 = f(a)
    x2 = a + dx
    f2 = f(x2)
    #
    while f1*f2 > 0.0:
        if x1 >= b: 
            return None, None
        x1 = x2
        f1 = f2
        x2 = x1 + dx
        f2 = f(x2)
    else:
        return x1,x2
    #
    #
#
#
# module bisect
#
def bisect(f, x1, x2, switch=0, epsilon=1.0e-9):
    """ root = bisect(f,x1,x2,switch=0,tol=1.0e-9).
    Finds a root of f(x) = 0 by bisection.
    The root must be bracketed in (x1,x2).
    Setting switch = 1 returns root = None if
    f(x) increases as a result of a bisection.
    """
    #
    f1 = f(x1)
    if f1 == 0.0: return x1
    #
    f2 = f(x2)
    if f2 == 0.0: return x2
    if f1*f2 > 0.0: err("Root is not bracketed")
    #
    n = int(math.ceil(math.log(abs(x2 - x1)/epsilon)/math.log(2.0)))
    #
    for i in range(n):
        #
        x3 = 0.5*(x1 + x2)
        f3 = f(x3)
        #
        if (switch == 1) and (abs(f3) >abs(f1)) and (abs(f3) > abs(f2)):
            return None
        if f3 == 0.0: return x3
        #
        if f2*f3 < 0.0:
            x1 = x3; f1 = f3
        #
        else:
            x2 =x3; f2 = f3
        #
    return (x1 + x2)/2.0
#
#
#
def GoalSeeker(f, bracket, search="FULL", step=0.01):
    #
    search = search.upper()
    #
    a,b,dx = (0.0, bracket, step)
    #
    #
    if search != "FULL":
        x1,x2 = rootsearch(f, a, b, dx)
        root = bisect(f,x1,x2,1)
    else:
        Roots = []
        while 1:
            # search for the bounds
            x1,x2 = rootsearch(f, a, b, dx)
            # check if root exist
            if x1 != None:
                a = x2
                root = bisect(f,x1,x2,1)
                # if root found, report it
                if root != None and root > 0.0:
                    #print (root)
                    # if root is real positive
                    #if root > 0.0:
                    Roots.append(root)
            else:
                break
        #
        root = min(Roots)
    #print ("Print min root",root)
    return root
    #
#
#
def solve_2variables(eq, var=('x', 'y')):
    """ Solve a system of simultaneous equation in
    two variables of the form

    2*x + 5*y=c1; 3*x - 5*y=c2

    Example: solve('12*x - 3*y = 21; 9*x  - 18*y=0')

    Should work for negative constants as well.

    Example: solve('3*x - 5*y=-11; 12*x + 3*y=48')

    Returns a two tuple of (x, y) values.

    NOTE: Won't denegarate to the special case
    of solving for only one variable.
    
    """

    var_re = re.compile(r'(\+|\-)\s*(\d*)\s*\*?\s*(x|y)')
    const_re = re.compile(r'(\+|\-)\s*(\-?\d+)$')

    constants, eqns, coeffs, default  = [],[], {'x': [], 'y': []}, {'': '1'}

    for e in eq.split(';'):
        eq1 = e.replace("="," - ").strip()
        if not eq1.startswith('-'):
            eq1 = '+' + eq1
        eqns.append(eq1)

    var_eq1, var_eq2 = map(var_re.findall, eqns)

    constants = [-1*int(x[0][1]) for x in map(const_re.findall, eqns)]
    [coeffs[x[2]].append(int((x[0]+ default.get(x[1], x[1])).strip())) for x in (var_eq1 + var_eq2)]
    
    ycoeff = coeffs['y']
    xcoeff = coeffs['x']

    # Adjust equations to take out y and solve for x
    if ycoeff[0]*ycoeff[1] > 0:
        ycoeff[1] *= -1
        xcoeff[0] *= ycoeff[1]
        constants[0] *= -1*ycoeff[1]        
    else:
        xcoeff[0] *= -1*ycoeff[1]
        constants[0] *= ycoeff[1]
        
    xcoeff[1] *= ycoeff[0]
    constants[1] *= -1*ycoeff[0]

    # Obtain x
    xval = sum(constants)*1.0/sum(xcoeff)

    # Now solve for y using value of x
    z = eval(eqns[0],{'x': xval, 'y': 1j})
    yval = -z.real*1.0/z.imag

    return (xval, yval)
#
def np_solve_2variables():
    import numpy as np
    a = np.array([[np.cos(np.pi/6), np.cos(np.pi/6)], [np.sin(np.pi/6), -1*np.sin(np.pi/6)]])
    b = np.array([[55],[15]])
    res = np.linalg.solve(a, b)
    res
#
def myGauss(m):
    #eliminate columns
    for col in range(len(m[0])):
        for row in range(col+1, len(m)):
            r = [(rowValue * (-(m[row][col] / m[col][col]))) for rowValue in m[col]]
            m[row] = [sum(pair) for pair in zip(m[row], r)]
    #now backsolve by substitution
    ans = []
    m.reverse() #makes it easier to backsolve
    for sol in range(len(m)):
        if sol == 0:
            ans.append(m[sol][-1] / m[sol][-2])
        else:
            inner = 0
            #substitute in all known coefficients
            for x in range(sol):
                inner += (ans[x]*m[sol][-2-x])
            #the equation is now reduced to ax + b = c form
            #solve with (c - b) / a
            ans.append((m[sol][-1]-inner)/m[sol][-sol-2])
    ans.reverse()
    return ans
#
#
#sol = solve_2variables('12*x - 3*y = 21; 9*x  - 18*y=0')
#sol = solve_2variables('x*0.8660254037844387 + y*0.8660254037844387 = 55; x*0.49999999999999994  - y*0.49999999999999994 = 15')
#
#
#sol = myGauss([[math.cos(math.pi/6), math.cos(math.pi/6), 55], 
#                [math.sin(math.pi/6), -1*math.sin(math.pi/6), 15]])
#
#print(sol)
#np_solve_2variables()
#print('ok')