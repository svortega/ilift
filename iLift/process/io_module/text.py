# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports
import re

#
# package imports

def match_line(word_in, key):
    """
    search key word at the begining of the string
    """
    word_out = False
    
    for _key, _item in key.items():
        rgx = re.compile(_item, re.IGNORECASE)
        keys = rgx.match(word_in)
        
        if keys: 
            word_out = _key        
    
    return word_out
#
#
def remove_comment(_lineIn):
    """
    """
    pattern = r"#|\*{2}|'|\"|\_|\-|!|%|/{2}"
    reobj = re.compile(pattern)
    for match in reobj.finditer(_lineIn):
        _index = match.start()
        
    _lineOut = _lineIn[0:_index]
    _lineOut = _lineOut.strip()

    return _lineOut
#
def search_line(lineIn, key, keyWord="N/A", count=1):
    """
    search key word anywere in the the string
    """
    lineOut = lineIn
    _match = False
    for _key, _item in key.items():
        rgx = re.compile(_item)
        keys = rgx.search(lineIn)
        
        if keys: 
            keyWord = _key
            lineOut = re.sub(keys.group(), " ", lineIn, count)
            _match = True
    
    lineOut = lineOut.strip()
    return keyWord, lineOut, _match
#
def word_pop(lineIn, count = 1):
    #
    _word  = str(lineIn).split()
    _temp   = re.compile(_word[0])
    lineOut = re.sub(_temp, " ", lineIn, count)
    lineOut = str(lineOut).strip()
    wordOut = str(_word[0]).strip()
    #
    return lineOut, wordOut
#
#
#