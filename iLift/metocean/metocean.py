# 
# Copyright (c) 2019 iLift
# 

# Python stdlib imports


# package imports


class Wind:
    
    __slots__ = ('_units', 'velocity', '_air_density',
                 '_direction')
    
    def __init__(self, units):
        """
        """
        self._units = units
        self.velocity = 15 * units.m / units.s
        self._air_density:float = 1.225 * units.kg/ units.m**3
        self._direction = 90 * units.deg
    #
    @property
    def speed(self):
        """
        """
        return self.velocity
    @speed.setter
    def speed(self, value:float):
        """
        """
        self.velocity = value
    #
    #
    @property
    def direction(self):
        """
        Wind direction 
        """
        return self._direction
    @direction.setter
    def direction(self, value:float):
        """
        Wind direction: anticlockwise angle in the yx plane
        
        ^ y (90 degrees)
        |
        |
        +---------> x (0 degrees)
        """
        self._direction = value    
    #
    @property
    def pressure(self):
        """
        """
        return self.velocity**2 * self._air_density # Pa
    #
#
class Metocean:
    """ """
    __slots__ = ('_units', '_wind')
    
    def __init__(self, units):
        """
        """
        self._units = units
        self._wind = Wind(units)
    #
    @property
    def wind(self):
        """
        """
        return self._wind
    #
    #
    #
