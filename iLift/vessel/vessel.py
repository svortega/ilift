# 
# Copyright (c) 2019 iLift
#
# Python stdlib imports
from typing import ClassVar, Dict # NamedTuple, List

# package imports


#
class Accelerations:
    """ """
    __slots__ = ('_vertical', '_lateral', 
                 '_horizontal','_item_report')
    
    def __init__(self, vertical:ClassVar, lateral:ClassVar, horizontal:ClassVar):
        """
        """
        self._vertical:ClassVar = vertical
        self._lateral:ClassVar = lateral
        self._horizontal:ClassVar = horizontal
        self._item_report:Dict = {'vertical': vertical,
                                  'lateral': lateral,
                                  'horizontal': horizontal}
    #
    @property
    def horizontal(self):
        """
        """
        return self._horizontal
    @horizontal.setter
    def horizontal(self, value:ClassVar):
        """
        """
        self._horizontal = value
        self._item_report['horizontal'] = value
    #
    @property
    def longitudinal(self):
        """
        """
        return self._horizontal
    @longitudinal.setter
    def longitudinal(self, value):
        """
        """
        self._horizontal = value
        self._item_report['horizontal'] = value
    #
    #
    @property
    def vertical(self):
        """
        """
        return self._vertical
    @vertical.setter
    def vertical(self, value:ClassVar):
        """
        """
        self._vertical = value
        self._item_report['vertical'] = value
    #
    #
    @property
    def lateral(self):
        """
        """
        return self._lateral
    @lateral.setter
    def lateral(self, value:ClassVar):
        """
        """
        self._lateral = value
        self._item_report['lateral'] = value
    #
    @property
    def transverse(self):
        """
        """
        return self._lateral
    @transverse.setter
    def transverse(self, value):
        """
        """
        self._lateral = value
        self._item_report['lateral'] = value
    #
    #
    @property
    def file_out(self):
        """
        printing lifting item details
        """
        return self._item_report     
#
#
class Vessel:
    
    __slots__ = ('_acceleration')
    
    def __init__(self, units):
        """
        """
        self._acceleration = Accelerations(vertical = 0.0 * units.gravity, 
                                           lateral = 0.0 * units.gravity,
                                           horizontal = 0.0 * units.gravity)
    #
    @property
    def acceleration(self):
        """
        """
        return self._acceleration
    #
#