# 
# Copyright (c) 2019 iLift
# 

# Python stdlib imports


# package imports
from iLift.process.io_module.text import match_line


#
#
def find_material_item(word_in):
    
    _key = {"Fy": r"\b(yield(strength)?|fy|limite(d[e]?|\_)?elasti(co|que|ite|cidad)|(flyte|streck)gren(s|z)e)\b",
            "poisson": r"\b(poisson|nu|v)\b",
            "Emodulus": r"\b((young|e(lastic)?)(\_)?(modulus)?)\b",
            "density": r"\b(densi(ty|dad|te)|rho|tetthet|dichte)\b",
            "alpha": r"\b((coefficient)?(of|\_)?thermal(\_)?(expansion)?|alpha)\b",
            "name": r"\b(name[s]?|id|nom(bre)?[s]?|navn)\b",
            "number": r"\b(num(ber[s]?|mer|ero[s]?))\b",
            "factor": r"\b(factor)\b",
            "Gmodulus": r"\bG\b",
            "Fu" : r"\bFu\b"}
    
    _match = match_line(word_in, _key)
    
    if not _match:
        return None
        #print('  **  erorr material item {:} not recognized'.format(word_in))
        #print('      process terminated')
        #sys.exit()

    return _match
#
def assign_material_item(_material, mat_items):
    """
    Assign material's input data to a dictionary by name/number
    """
    
    for key, value in mat_items.items():
        
        _item = find_material_item(key)
        #_mat[_item] = value
        if _item == 'Fy':
            _material.Fy = float(value)
        
        elif _item == 'Emodulus':
            _material.E = float(value)
        
        elif _item == 'poisson':
            _material.poisson = float(value)
        
        elif _item == 'Gmodulus':
            _material.G = float(value)
        
        elif _item == 'Fu':
            _material.Fu = float(value)
        
        elif _item == 'density':
            _material.rho = float(value)
        
        elif _item == 'alpha':
            _material.alpha = float(value)

        #elif _item == 'rho':
        #    _material.rho = float(value)
        
        else:
            print('error material item : {:} not recognized'
                  .format(key))
    #
    # check if minimum required
    #try:
    #    _material.Fy
    #except AttributeError:
    #    print('error Fy value not found, program aborted')
    #    sys.exit()
    # Poisson check
    #try:
    #    _material.poisson
    #except AttributeError:
    #    _material.poisson = 0.30
    # Minimum Tensile Strenth
    #try:
    #    _material.Fu
    #except AttributeError:
    #    _material.Fu = _material.Fy / 0.75
    #print('ok')
    #return _material
#
#--------------------------------------------------------------------
#                            Material
#--------------------------------------------------------------------
#
#
class Material:
    __slots__ = ('number', 'name', '_units',
                 '_E', '_poisson', '_density', '_G',
                 '_Fy', '_Fu', '_damping', '_alpha', 
                 'units_in', 'type',  'grade')
    
    def __init__(self, units):
        """
        alpha : Thermal expansion ratio (in 1/K at 20C )
        """
        #  ----- Material -----
        self._units = units
        self._poisson = 0.30
        self._density = 7850E-3 * units.g / units.mm**3
        self._E = 205000 * units.MPa
        self._G = 77200.0 * units.MPa
        self._alpha = 1.2E-5  / units.K # (20 degrees C)
    #
    @property
    def units(self):
        """
        Input:\n
        length : [mandatory]\n
        mass   : [mandatory]\n
        force  : [mandatory]\n
        time   : sec [default]\n
        temperature : \n
        gravity     : [default : 9.81ms^2]\n

        ------
        units [length, mass, time, temperature, force, pressure/stress]/n
        """
        return self._units
    @units.getter
    def units(self, value):
        """
        Input:\n
        length : [mandatory]\n
        mass   : [mandatory]\n
        force  : [mandatory]\n
        time   : sec [default]\n
        temperature : \n
        gravity     : [default : 9.81ms^2]\n

        ------
        units [length, mass, time, temperature, force, pressure/stress]/n
        """
        self._units = value
    #
    def __getattr__(self, key):
        """
        """
        _item = find_material_item(key)
        
        if _item == 'Fy':
            try:
                return self._Fy
            except Exception:
                raise RuntimeError("'error Fy value not found'")
        elif _item == 'Emodulus':
            return self._E
        elif _item == 'poisson':
            return self._poisson
        elif _item == 'Gmodulus':
            return self._G
        elif _item == 'Fu':
            try:
                return self._Fu
            except Exception:
                return self.Fy / 0.75
        elif _item == 'density':
            return self._density
        elif _item == 'alpha':
            return self._alpha
        else:
            raise Exception(" material item {:} not recognized".format(key))
        #    print('fix here')        
        #print('-->')
    #
    def __setattr__(self, key, value):
        """
        """
        _item = find_material_item(key)
        
        if _item == 'Fy':
            self._Fy = value
        elif _item == 'Emodulus':
            self._E = value
        elif _item == 'poisson':
            self._poisson = float(value)
        elif _item == 'Gmodulus':
            self._G = value
        elif _item == 'Fu':
            self._Fu = value
        elif _item == 'density':
            self._density = value
        elif _item == 'alpha':
            self._alpha = value
        else:
            super().__setattr__(key, value)        
        #print('-->')
    #
    #
    #
    def user_input(self, **kwargs):
        """
        Input:/n
        Fy :
        Emodulus : Elastic modulus
        poisson  :
        density  : material density
        alpha    : thermal
        """
        #
        for key, value in kwargs.items():
            self.__setattr__(key, value)
            #key, value
        #assign_material_item(self, kwargs)
        #
        # Check if missing material data
        #
        # units [length, mass, time, temperature, force, pressure/stress]
        #_units_input = ["metre", "kilogram", "second", "celsius", "newton", "newton/metre^2"]
        #_units_output = self.units_in
        #factors, gravity = units.get_factors_and_gravity(_units_input, 
        #                                                 _units_output)
        #print('ok')        
    #
    def _print_material(self):
        """
        """
        output =[]
        output.append("_______________________________________________________________________________________\n")
        output.append(" "+"\n")
        output.append("                                  MATERIAL PROPERTIES\n")
        output.append(" "+"\n")
        output.append("Name           Fy [N/mm2]  Fu [N/mm2]  E  [N/mm2]  Poisson     Rho[kg/m3]    G  [N/mm2]\n")  
        #output.append("Number\n")
        output.append(".......................................................................................\n")
        output.append("\n")
        output.append("{:14s} {:1.4E}  {:1.4E}  {:1.4E}  {:1.4E}  {:1.4E}\n"
                      .format("", self.Fy.convert('megapascal').value, self.Fu.convert('megapascal').value, 
                              self.E.convert('megapascal').value, self.poisson, self.rho.convert('kilogram/metre^3').value,
                              self.G.convert('megapascal').value))
        return output