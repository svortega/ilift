# 
# Copyright (c) 2019 iLift


# Python stdlib imports
import datetime
#
# package imports
from iLift.codes.EN1993.April2007.EN1993_6_2007 import *


class EN1993_6:
    """
    Eurocode 3 - Design of steel structures
    Part6: Crane supporting structures
    """
    def __init__(self, cls_design):
        """
        """
        # ----- General Data -----
        #
        self._units = cls_design._units
        self.beam_name = cls_design.item_name
        #
        #  ----- Material -----
        self._material = cls_design._material
        self._section = cls_design._section
        #  ----- element Forces -----
        self.actions = cls_design.actions        
        # ----- internal stress -----
        self.stress = cls_design.stress
        #
    #
    def get_EN1993_6_results(self):
        """
        """
        #
        #print report 
        #self.file_out = print_header() 
        #self.file_out.extend(self._material._print_material())
        #self.file_out.extend(self._section._print_section_properties())
        #
        # Partial factors from EN 1991-3 Annex A
        # Transient situation
        self.gamma = table_A_1()        
        #
        # Wheel load
        self.F_zEd = self.actions.Fz
        self.F_zEd *= self.gamma.Gsup
        #
        #
        print('')
        print('-------------------------------------------')
        print(' Resistance of bottom flange to wheel loads')
        #
        # 6.7 Resistance of bottom flanges to wheel loads
        #
        # sigma_fEd is the stress at the midline of the flange due
        # to the overall internal moment in the beam
        #
        _sigma_fEd = max([abs(_item) for _item in self.trolley._stress.sigma_y]) # @hami2230 - 20.02.19 - changed from von mises
        #
        #_sigma_fEd = max([abs(_von_mises[3]), 
        #                  abs(_von_mises[4]), 
        #                  abs(_von_mises[5])])
        #
        self.sigma_fEd = _sigma_fEd * self.gamma.Gsup # variable crane actions
        print('sigma_fEd = {: 1.2f} MPa'.format(self.sigma_fEd.convert('megapascal').value))
        #
        # F_fRd is the design resistance of the bottom of the flange of a beam
        # to a wheel load Fz_Ed
        self.F_fRd = chapter_6_7(self, self._section, self._material, self.trolley)
        print('F_fRd = {: 1.2f} kN'.format(self.F_fRd.convert('kilonewton').value))
        #
        if self.F_fRd <= 0:                                                    # @hami2230 - 18.02.19 - updated
            print('F_fRD < 0 --> Fail')
        elif self.F_zEd > self.F_fRd:
            print('F_zEd [{: 1.2f} kN] > F_fRd [{: 1.2f} kN] --> Fail'
                  .format(self.F_zEd.convert('kilonewton').value, 
                          self.F_fRd.convert('kilonewton').value))
        else:
            print('F_zEd [{: 1.2f} kN] < F_fRd [{: 1.2f} kN] --> ok'
                  .format(self.F_zEd.convert('kilonewton').value, 
                          self.F_fRd.convert('kilonewton').value))
        #
        if self.F_fRd <= 0:
            print('Utilization remote = N/A')
        else:
            print('Utilization remote = {:1.2f}'.format(self.F_zEd.value/self.F_fRd.value))
        #
        # 7.5 Reversible behaviour 
        # (3) The nominal stresses for runway beams with a monorail hoist block or an 
        #     underslung crane should include the local stresses sigma_ox_Ed_ser and 
        #     sigma_oy_Ed_ser in the bottom flange, see 5.8, in addition to the globa
        #     stress sigmax_Ed_ser and tau_Ed_ser
        print('')
        print('-------------------------------------------')        
        print(' Local Bending Stresses in Bottom Flange (SLS Check)')
        #
        # 5.8 Local bending stresses in the bottom flange due to wheel loads
        sigma_ox, sigma_oy = chapter_5_8(self, self._section, self.trolley)
        #
        print('')
        print(' Superimposed Stresses Using Service Level Vertical Loads')
        print('')
        # 
        for x in range(3):
            # apply load factor                                                # @hami2230 - 21/02/19 - updated                  
            self.trolley._stress.sigma_x[6 + x] *= self.gamma.Gsup
            self.trolley._stress.sigma_y[6 + x] *= self.gamma.Gsup
            self.trolley._stress.sigma_z[6 + x] *= self.gamma.Gsup
            self.trolley._stress.tau_x[6 + x] *= self.gamma.Gsup
            self.trolley._stress.tau_y[6 + x] *= self.gamma.Gsup
            self.trolley._stress.tau_z[6 + x] *= self.gamma.Gsup
            # combine global longitudinal stresses                             # @hami2230 - 21/02/10 - updated
            self.trolley._stress.sigma_y[6 + x] += self.trolley._stress.sigma_x[6 + x]
            if self.trolley._stress.sigma_y[6 + x] > 0 and self.trolley._stress.sigma_z[6 + x] > 0:
                self.trolley._stress.sigma_y[6 + x] += self.trolley._stress.sigma_z[6 + x]
            elif self.trolley._stress.sigma_y[6 + x] < 0 and self.trolley._stress.sigma_z[6 + x] < 0:
                self.trolley._stress.sigma_y[6 + x] += self.trolley._stress.sigma_z[6 + x]
            else:
                self.trolley._stress.sigma_y[6 + x] -= self.trolley._stress.sigma_z[6 + x]
            # add global and local stresses                                    # @hami2230 - 21/02/10 - updated
            self.trolley._stress.sigma_y[6 + x] += sigma_ox[x]
            # self.trolley._stress.sigma_x[6 + x] += sigma_oy[x]               
            #
            #sigmax_Ed_ser = abs(sigma_ox[x]) + abs(self.stress.sigma_x[6+x]) # bottom plate
            #sigmay_Ed_ser = abs(sigma_oy[x]) + abs(self.stress.sigma_y[6+x]) 
            #tau_ser = abs(self.stress.tau_z[3+x]) 
            print('Reversible Behaviour position {:}'.format(x))
            chapter_7_5(self, self._material,                                  # @hami2230 - 21/02/19 - updated
                        self.trolley._stress.sigma_y[6 + x], # bottom plate face
                        sigma_oy[x],                         # bottom plate face
                        self.trolley._stress.tau_z[6+x],
                        self.trolley._stress.tau_y[6+x])     # mid plate
        #
        #print('--> end')
    #
    #
    @property
    def I_section(self):
        """
        """
        return self.i_section
    
    @I_section.setter
    def I_section(self, I_section):
        """
        leff is the effective length of flange resisting the wheel load
        """
        # 2) The lever arm m from the wheel load to the root of the flange
        #    should be determined as follows:
        if 'welded' in I_section.build.lower() :
            self.trolley.m = (0.50 * (I_section.bfb - I_section.tw) 
                              - (0.80 * I_section.r) - self.trolley.n)
        else:
            self.trolley.m = (0.50 * (I_section.bfb - I_section.tw) 
                              - (0.80 * 2**0.5 * I_section.r) - self.trolley.n)
        #
        print('m = {:1.3f} mm'.format(self.trolley.m.convert('millimetre').value))
        #
        # 3) The effective length of flange leff resisting one wheel load 
        #    should be determined from table 6.2
        case = self.trolley.case
        self.l_eff = table_6_2(case, self.trolley)
        print('The effective length of flange resisting the wheel load')
        print('l_eff = {:1.1f} mm'.format(self.l_eff.convert('millimetre').value))         
        #
        # 4) Generally the coefficients Cx and Cy for determining the longitudinal and 
        #    transverse bending stresses at the three locations 0, 1 and 2, may be determined
        #    from table 5.2 depending on whether the beam has parallel flanges or taper 
        #    flanges, and the value of the ratio mu given by:
        self.mu = 2 * self.trolley.n.value / (I_section.bfb.value - I_section.tw.value)
        print('Ration mu (5.7) = {:1.3f}'.format(self.mu))
        
        self.Cx, self.Cy = table_5_2(I_section, self.mu)
        #
        # 5) Alternatively, in the case of wheel loads applied near the outside edges of 
        #    the flange, the values of the coefficients Cx and Cy given in table 5.3 may used.
        #self.Cx2, self.Cy2 = table_5_3(I_section, self.mu)
        #
        # plate en the yz plane of the flange
        self.i_section = I_section
        self._section.d = I_section.tfb
        self._section.w = I_section.bfb / 2.0  # self.l_eff
        self._section._get_properties()
        #
    #
    @property
    def trolley(self):
        """
        """
        return self.hoist_trolley
    #
    @trolley.setter
    def trolley(self, trolley):
        """
        """
        self.hoist_trolley = trolley
    #
    @property
    def sigma_fEd(self):
        """
        sigma_fEd : stress at the midline of the flange due to the overall internal moment in the beam
        """
        return self.sigmafEd
    #
    @sigma_fEd.setter
    def sigma_fEd(self, value):
        """
        sigma_fEd : stress at the midline of the flange due to the overall internal moment in the beam
        """
        self.sigmafEd = value
    #
    @property
    def F_zEd(self):
        """
        F_zEd : vertical crane wheel load
        """
        return self.FzEd
    
    @F_zEd.setter
    def F_zEd(self, value):
        """
        F_zEd : vertical crane wheel load
        """
        self.FzEd = value
    #
    def print_results(self):
        """
        """
        return self.file_out
    #
#
def print_header():
    """
    """
    today = datetime.date.today()
    output = [] # open(self.FileOut,'w')
    #
    #output.append(" "+"\n")
    output.append("***************************************************************************************\n")
    output.append("*                                  CODE CHECK TOOL                                    *\n")
    output.append("*                                  EN 1993-6 (2007)                                   *\n")
    output.append("*   Eurocode 3: Design of steel structures - Part 6: Crane supporting structures      *\n")
    output.append("*                                   BETA Version                             12/12/18 *\n")            
    output.append("***************************************************************************************\n")
    output.append("DATE: {:8s} {:>59}".format(str(today), ""))
    # 
    #output=open(self.FileOut,'a+')
    return output
#