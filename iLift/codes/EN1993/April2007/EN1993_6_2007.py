# 
# Copyright (c) 2019 iLift


# Python stdlib imports
import math
from collections import namedtuple

#
# package imports

#
#
#
C_x = namedtuple('Cx', ['x0', 'x1', 'x2'])
C_y = namedtuple('Cy', ['y0', 'y1', 'y2'])
#
def table_5_2(section, mu):
    """
    Table 5.2 : Coefficients Cxi and Cyi for calculating stresses at points
                i = 0, 1 and 2
    
    Sign convention: Cxi and Cyi are positive for tensile stresses at the bottom
                     face of the flange
    
    NOTE: The coefficients for taper flange beams are for a slope of 14% or 8 degrees.
          They are conservative ror beams with a larger flange slope.
          For beams with a smaller flange slope, it is conservative to adopt the coefficients
          for parallel flange beams. Alternatively linear interpolation may be used.
    """
    #
    if 'symmetrical' in section.type.lower():
        # Coefficients for longitudinal bending stress
        Cx0 = 0.050 - 0.580*mu + 0.148 * math.exp(3.015*mu)
        Cx1 = 2.230 - 1.490*mu + 1.390 * math.exp(-18.33*mu)
        Cx2 = 0.730 - 1.580*mu + 2.910 * math.exp(-6.000*mu)
        # Coefficients for transverse bending stress
        Cy0 = -2.110 + 1.977*mu + 0.0076 * math.exp(6.530*mu)
        Cy1 = 10.108 - 7.408*mu - 10.108 * math.exp(-1.364*mu)
        Cy2 = 0.00
    else:
        # Coefficients for longitudinal bending stress
        Cx0 = -0.981 - 1.479*mu + 1.120 * math.exp(1.322*mu)
        Cx1 = 1.810 - 1.150*mu + 1.060 * math.exp(-7.700*mu)
        Cx2 = 1.990 - 2.810*mu + 0.840 * math.exp(-4.690*mu)
        # Coefficients for transverse bending stress
        Cy0 = -1.096 + 1.095*mu + 0.192 * math.exp(-6.000*mu)
        Cy1 = 3.965 - 4.835*mu - 3.965 * math.exp(-2.675*mu)
        Cy2 = 0.00         
    #
    return C_x(Cx0, Cx1, Cx2), C_y(Cy0, Cy1, Cy2)
#
def table_5_3(section, mu):
    """
    Table 5.3 : Coefficients for calculating stresses near the outside edges of flanges
    
    Sign convention: Cxi and Cyi are positive for tensile stresses at the bottom
                     face of the flange
    
    NOTE: The coefficients for taper flange beams are for a slope of 14% or 8 degrees.
          They are conservative ror beams with a larger flange slope.
          For beams with a smaller flange slope, it is conservative to adopt the coefficients
          for parallel flange beams. Alternatively linear interpolation may be used.
    """
    #
    if 'symmetrical' in section.type.lower():
        if mu <= 0.10:
            # Coefficients for longitudinal bending stress
            Cx0 = 0.20 
            Cx1 = 2.30 
            Cx2 = 2.20 
            # Coefficients for transverse bending stress
            Cy0 = -1.90 
            Cy1 = 0.60 
            Cy2 = 0.00 
        else:
            # Coefficients for longitudinal bending stress
            Cx0 = 0.20 
            Cx1 = 2.10 
            Cx2 = 1.70 
            # Coefficients for transverse bending stress
            Cy0 = -1.80 
            Cy1 = 0.60 
            Cy2 = 0.00 
    else:
        # Coefficients for longitudinal bending stress
        Cx0 = 0.20 
        Cx1 = 2.00 
        Cx2 = 2.00 
        # Coefficients for transverse bending stress
        Cy0 = -0.90 
        Cy1 = 0.60 
        Cy2 = 0.00 
    #
    return C_x(Cx0, Cx1, Cx2), C_y(Cy0, Cy1, Cy2)
#
def table_6_1():
    """
    Table 6.1 Partial factors for resistance
    """
    GammaM = namedtuple('GammaM', ['M0', 'M1', 'M2', 'M3', 'M4', 'M5', 'M6_ser', 'M7', 'M3_ser'])
    gamma_M = GammaM(M0=1.0, M1=1.0, M2=1.25, M3=1.25, 
                     M4=1.0, M5=1.0, M6_ser=1.0, M7=1.0, M3_ser=1.10)
    return gamma_M
#
def table_6_2(case, trolley):
    """
    Table 6.2 : Effective length leff
    """
    
    if "a" in case:
        # Wheel adjacent to a non-reinforced simple joint
        l_eff = 2 * (trolley.m + trolley.n)
    elif "b" in case:
        # Wheel remote from the end of a member
        if trolley.Xw < 4 * 2**0.5 * (trolley.m + trolley.n):
            l_eff = 2 * 2**0.5 * (trolley.m + trolley.n) + 0.50 * trolley.Xw
        else:
            l_eff = 4 * 2**0.5 * (trolley.m + trolley.n)
    elif "c" in case:
        # Wheel adjacent to an end stop at a distance Xe from the end of the member
        l_eff = (2 * (trolley.m + trolley.n) 
                 * (trolley.Xe / trolley.m + math.sqrt(1.0 + (trolley.Xe / trolley.m)**2)))        
        if trolley.Xw < 2 * 2**0.5 * (trolley.m + trolley.n) + trolley.Xe:
            l_eff = min(l_eff, 2**0.5 * (trolley.m + trolley.n) + (trolley.Xw + trolley.Xe)/2.0)
        else:
            l_eff = min(l_eff, 2**0.5 * (trolley.m + trolley.n) + trolley.Xe)
    elif "d" in case:
        # Wheel adjacent to an end that is fully supported either from below or by 
        # a welded closer plate at a distance Xe from the end of the member
        if (trolley.Xw < (2 * 2**0.5 * (trolley.m + trolley.n) +
                          trolley.Xe + 2 * (trolley.m + trolley.n)**2 / trolley.Xe)):
            l_eff = (2**0.5 * (trolley.m + trolley.n) + (trolley.Xe + trolley.Xe) / 2.0 +
                     (trolley.m + trolley.n)**2 / trolley.Xe)
        else:
            l_eff = (2 * 2**0.5 * (trolley.m + trolley.n) + trolley.Xe  +
                     2 * (trolley.m + trolley.n)**2 / trolley.Xe)
    
    return l_eff
    
#
def table_A_1():
    """
    from EN 1991-3 Annex A
    Transient situation
    """
    Gamma = namedtuple('Gamma', ['Gsup', 'Ginf', 'Qsup', 'Q', 'A'])
    return Gamma(Gsup=1.35, Ginf=1.00, Qsup=1.35, Q=1.50, A=1.0)
#
#
def chapter_5_8(self, section, trolley):
    """
    5.8 Local bending stresses in the bottom flange due to wheel loads
    
    1) The following method may be used to determine the local bending stresses
       in the bottom flange of an I-section beam, due to wheel loads applied to 
       the bottom flange.
    
    2) The bending stresses due to wheel loads applied at locations more than b
       from the end of the beam, where b is the flange width, can be determined 
       at the three locations:
       - location 0 : the web-to-flange transition
       - location 1 : centreline of the wheel load
       - location 2 : outside edge of the flange
    """
    #
    #   Note Cx & Cy were calculated in EN1993_6.py --> I_section
    #
    # 4) Generally the coefficients Cx and Cy for determining the longitudinal and 
    #    transverse bending stresses at the three locations 0, 1 and 2, may be determined
    #    from table 5.2 depending on whether the beam has parallel flanges or taper 
    #    flanges, and the value of the ratio mu given by:
    #mu = 2 * trolley.n / (section.bf - section.tw)
    #print('Ration mu (5.7) = {:1.3f}'.format(mu))
    #
    #Cx, Cy = table_5_2(section, mu)
    #
    # 5) Alternatively, in the case of wheel loads applied near the outside edges of 
    #    the flange, the values of the coefficients Cx and Cy given in table 5.3 may used.
    #Cx2, Cy2 = table_5_3(section, mu)
    #
    # 3) Provided that the distance Xw along the runway beam between adjacent
    #    wheel loads is not less than 1.5b, where b is the flange width of the beam,
    #    the local longitudinal bending stress sigma_ox,Ed and transverse bending 
    #    stress sigma_oy,Ed in the bottom flange due to the application of a wheel
    #    load more than b from end of the beam should be obtained from:  
    # 5.5
    # d is the thickness of the rectangular section
    sigma_ox = [self.Cx[i] * self.F_zEd  / section.d**2 for i in range(3)]
    # 5.6
    sigma_oy = [self.Cy[i] * self.F_zEd  / section.d**2 for i in range(3)]
    #
    #
    # 6) In the absence of better information, the local bending stress sigma_oy.end.Ed 
    #    in an unstiffened bottom flange due to the application of wheel loads at a 
    #    perpendicular end of the beam should be determined from:
    #sigma_oy_end_Ed = (5.6 - 3.225 * self.mu - 2.8 * self.mu**3) * self.F_zEd  / section.d**2 
    #
    return C_x(*sigma_ox), C_y(*sigma_oy) #, sigma_oy_end_Ed
#
def chapter_6_7(self, section, material, trolley):
    """
    6.7 Resistance of bottom flanges to wheel loads
    """
    #
    #   Note lever arm m and l_eff were calculated in EN1993_6.py --> I_section
    #    
    # 2) The lever arm m from the wheel load to the root of the flange
    #    should be determined as follows:
    #if 'welded' in section.build.lower() :
    #    trolley.m = 0.50 * (section.bf - section.tw) - (0.80 * section.r) - trolley.n
    #else:
    #    trolley.m = 0.50 * (section.bf - section.tw) - (0.80 * 2**0.5 * section.r) - trolley.n
    
    # 3) The effective length of flange leff resisting one wheel load 
    #    should be determined from table 6.2
    #case = trolley.case
    #self.l_eff = table_6_2(case, trolley)
    
    # 1) The design resistance FfRd of the bottom flange of a beam to a wheel load
    #    FzEd from an underslung crane or hoist block trolley wheel, should be 
    #    determined from:
    
    #sigma_fEd = actions.My * (section.height - section.tf) * 0.50 / section.Iy
    #print('sigma_fEd = {: 1.2f} MPa'.format(self.sigma_fEd.convert('megapascal').value))
    
    gamma = table_6_1()
    fysigma = material.Fy / gamma.M0
    print('sigma_y = {:1.2f} MPa'.format(fysigma.convert('megapascal').value))
    # d is the thickness of the rectangular section
    F_fRd = ((self.l_eff * section.d**2 * fysigma / (4 * trolley.m)) 
             * (1.0 - (self.sigma_fEd.value / fysigma.value)**2))
    #print('F_fRd = {:1.2f} kN'.format(F_fRd.convert('kilonewton').value))
    #
    return F_fRd
#
def chapter_7_5(self, material,  sigma_Edser_x, sigma_Edser_y, tau_Edser_z, tau_Edser_y): #@hami2230 - updated to include shear in y and z directions
    """
    7.5 Reversible behaviour
    """
    #
    # get factors
    gamma = table_6_1()
    sigma_y = material.Fy / gamma.M3_ser
    #
    #
    # 1) To ensure reversible behaviour, the stresses sigma_Ed,ser and tau_Ed,ser resulting 
    #    from the relevant characteristic load combination or test load combination,
    #    calculated making due a]]owance where relevant for the effects of shear lag in
    #    wide flanges and for the secondary effects induced by deformations (for instance 
    #    secondary moments in trusses) should be limited as follows:
    #
    # 7.2a Bending
    print('sigma_x 7.2a = {:1.2f} MPa'.format(sigma_Edser_x.convert('megapascal').value))
    print('UR bending x = {:1.3f}'.format(sigma_Edser_x.value / sigma_y.value))
    #
    print('sigma_y 7.2a = {:1.2f} MPa'.format(sigma_Edser_y.convert('megapascal').value))
    print('UR bending y = {:1.3f}'.format(sigma_Edser_y.value / sigma_y.value))
    #
    # 7.2b Shear
    print('tau 7.2b = {:1.2f} MPa'.format(tau_Edser_z.convert('megapascal').value))
    print('UR Shear = {:1.3f}'.format(tau_Edser_z.value / (3**0.50 * sigma_y.value)))
    print('tau 7.2b = {:1.2f} MPa'.format(tau_Edser_y.convert('megapascal').value))
    print('UR Shear = {:1.3f}'.format(tau_Edser_y.value / (3**0.50 * sigma_y.value)))
    #
    #
    # 7.2c
    sigma_ser_72c = (sigma_Edser_x**2 + 3*(tau_Edser_z**2+tau_Edser_y**2))**0.50
    print('sigma 7.2c = {:1.2f} MPa'.format(sigma_ser_72c.convert('megapascal').value))
    print('UR comb = {:1.3f}'.format(sigma_ser_72c.value / sigma_y.value))
    # 7.2d
    sigma_ser_72d = (sigma_Edser_x**2 + sigma_Edser_y**2 
                     - (sigma_Edser_x * sigma_Edser_y) + 3*(tau_Edser_z**2+tau_Edser_y**2))**0.50
    print('sigma 7.2d = {:1.2f} MPa'.format(sigma_ser_72d.convert('megapascal').value))
    print('UR comb = {:1.3f}'.format(sigma_ser_72d.value / sigma_y.value))
    #
    # 7.2e Not applicable
    #
    print('Combined Bending Stress checks')
    sigma_ser = max(sigma_ser_72c, sigma_ser_72d)
    #
    if abs(sigma_ser) > sigma_y :
        print('sigma_xf [{:1.2f}] > fy/gamma [{:1.2f}] --> fail'
              .format(sigma_ser.convert('megapascal').value, 
                      sigma_y.convert('megapascal').value))
    else:
        print('sigma_xf [{:1.2f}] < fy/gamma [{:1.2f}] --> ok'
              .format(sigma_ser.convert('megapascal').value, 
                      sigma_y.convert('megapascal').value))
    #
    print('UR Von-Mises = {:1.3f}'.format(sigma_ser.value/sigma_y.value))
    print('')
    #
#
#