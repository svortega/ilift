# 
# Copyright (c) 2019 iLift
#

# Python stdlib imports
from typing import NamedTuple, Tuple # List,

# package imports
from iLift.process.io_module.text import match_line


#
#
def find_stress_item(word_in):
    """
    """
    _key = {"sigma_x": r"\b((sigma|stress)(\_)?(x))\b",
            "sigma_y": r"\b((sigma|stress)(\_)?(y))\b",
            "sigma_z": r"\b((sigma|stress)(\_)?(z))\b",
            #
            "tau_x": r"\b((tau)(\_)?(x))\b",
            "tau_y": r"\b((tau)(\_)?(y))\b",
            "tau_z": r"\b((tau)(\_)?(z))\b"}
    
    _match = match_line(word_in, _key)
    if not _match:
        raise IOError('  ** item {:} not recognized'.format(word_in))
    return _match
#
def assign_stress_item(self, mat_items):
    """
    Assign stress 
    
    """
    #if not self.stress:
    #    self.stress = Stress(0, 0, 0, 0, 0, 0)
    
    for key, value in mat_items.items():
        _item = find_stress_item(key)
        #
        if _item == 'sigma_x':
            self.stress.sigma_x += value
        elif _item == 'sigma_y':
            self.stress.sigma_y += value
        elif _item == 'sigma_z':
            self.stress.sigma_z += value
        elif _item == 'tau_x':
            self.stress.tau_x += value
        elif _item == 'tau_y':
            self.stress.tau_y += value
        elif _item == 'tau_z':
            self.stress.tau_z += value
        else:
            raise IOError('error stress item : {:} not recognized'
                          .format(_item))
    #
    #self.stress = Stress(_sigma_x, _sigma_y, _sigma_z, _tau_x, _tau_y, _tau_z)
    #print('ok')
#
#
#
class CodeResults(NamedTuple):
    """
    """
    axial:Tuple
    shear:Tuple
    bending:Tuple
    combined:Tuple
    total:Tuple
    report:str
#
#Chapter_results = namedtuple('chapter', ['URy', 'URz', 'UR_flag'])
#Summary_results = namedtuple('total', ['UR', 'status', 'UR_flag'])
#
class ChapterResults(NamedTuple):
    """
    """
    URy : float
    URz : float
    UR_flag : str
    #
    allowable_y:float
    allowable_z:float
#
class AxialResults(NamedTuple):
    """
    """
    UR : float
    #URz : float
    UR_flag : str
    #
    allowable:float
    warning:str
#
class SummaryResults(NamedTuple):
    """
    """
    UR : float
    status : str
    UR_flag : str