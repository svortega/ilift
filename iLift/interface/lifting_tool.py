# 
# Copyright (c) 2019 iLift
#
#
# Python stdlib imports
from typing import ClassVar
#

# package imports
from iLift.beam.beam import Beam
from iLift.metocean.metocean import Metocean
from iLift.lift_item.item import LiftingItem, Rigging
from iLift.load.load import Loading, Factors
from iLift.load.lift_safe_load import LiftingSafeLoad
from iLift.cranes.beam_clamps import GirderClamp, HoistTrolley
from iLift.process.units.units import Units
from iLift.vessel.vessel import Vessel
#
#
#
class Lifting_Tool:
    """ 
    """
    __slots__ = ('_cranes', '_units', 'report',
                 '_item', '_rigging', '_vessel',
                 '_factors', '_metocean', '_load', 
                 '_beam')
    
    def __init__(self):
        """
        """
        #
        self._cranes = {}
        #        
        self._units = Units()
        #
        self._item = LiftingItem(self._units)
        self._rigging = Rigging(self._units)
        #
        self._factors = Factors(self._units)
        self._metocean = Metocean(self._units)
        self._vessel = Vessel(self._units)
        #
        self._load = Loading(self._item, self._rigging, self._cranes,
                             self._factors, self._metocean, self._vessel,
                             self._units)
        #
        #
        self._beam = Beam(self._cranes, self._load, 
                          self._units)        
        #print('-->')
    @property
    def units(self) -> ClassVar:
        """
        """
        return self._units
    
    @property
    def wind(self) -> ClassVar:
        """
        """
        return self._metocean.wind
    
    @property
    def factors(self) -> ClassVar:
        """
        """
        return self._factors
    
    @property
    def vessel(self):
        """
        """
        return self._vessel    
    
    @property
    def item(self) -> ClassVar:
        """
        Item to be lift
        """
        return self._item
    
    @property
    def rigging(self) -> ClassVar:
        """
        """
        return self._rigging    
    
    @property
    def load(self) -> ClassVar:
        """
        """
        return self._load
    
    @property
    def beam(self) -> ClassVar:
        """
        """
        return self._beam
    
    #
    #
    @property
    def clamp(self) -> ClassVar:
        """
        """
        _number = len(self._cranes) + 1
        # limit clamps to two units (i.e. hztal crosshouling using two trolleys only)
        if _number > 2:
            _number = 2 
            raise Warning('Only two crane items are allowed')
        #
        self._cranes[_number] = GirderClamp(self)
        return self._cranes[_number]
    #
    @property
    def trolley(self) -> ClassVar:
        """
        """
        _number = len(self._cranes) + 1
        # limit trolley to one unit (i.e. hztal crosshouling with two trolleys is rare)
        if _number > 2:
            _number = 2 
            raise Warning('Only two crane items are allowed')        
        elif _number == 2 and self._cranes[1]._type == 'trolley':
            _number = 1
            raise Warning('Only one trolley item is allowed')
        #
        self._cranes[_number] = HoistTrolley(self)
        return self._cranes[_number]
    #
    @property
    def lift_angle(self) -> ClassVar:
        """
        """
        return self._load.lift_angle
    #
    @property
    def cross_hauling_angle(self) -> ClassVar:
        """
        """
        return self._load.lift_angle
    #
    #
    def safe_lifting_load(self, allowable_UR:float, 
                          actual_UR:float):
        """
        """
        _safe_load = LiftingSafeLoad(self)
        return _safe_load.get_safe_lift_load(allowable_UR = allowable_UR, 
                                             actual_UR = actual_UR)
    # 
    #
    #def global_check(self):
    #    """
    #    """
    #    self.beam.code_check()
    #
    #def local_check(self):
    #    """
    #    """
    #    self.beam.local_code_check()
    #
    def print_report(self):
        """
        """
        #
        from iLift.report.word import Report
        #
        self.beam.code_check()
        self.beam.local_code_check()
        #
        print('-->')
        # FIXME: reporting needs updating 
        return 
        # self._beam._simply_supported
        self.report = Report(self._units)
        self.report.methodology(self._factors.file_out(),
                                self._load.file_out())
        # Loading section
        self.report.loading(self._load, self._item, self._rigging)
        #
        if 'clamp' in self._crane_type:
            self.report.print_clamp()
        else:
            self.report.print_trolley()
        #
        # conclusions
        self.report.conclusions(self.beam.design, 
                                self._flange.design,
                                self._web.bottom.design,
                                self._web.top.design)
        #
        # References
        self.report.references()
        self.report._report.add_page_break()
        # Appendix
        # beam global check
        self.report.add_headings(heading_text='Appendix A.- AISC Analysis of Steel Members',
                                 level_number=1)
        self.report.add_headings(heading_text='A1. Beam Global Check',
                                 level_number=2)
        self.report.appendixAISC(self.beam.design.file_out)
        #
        # local check
        self.report._report.add_page_break()
        self.report.add_headings(heading_text='A2. Flanges for Transverse Bending Check',
                                 level_number=2)
        self.report.appendixAISC(self._flange.design.file_out)
        # 
        self.report._report.add_page_break()
        self.report.add_headings(heading_text='A3. Web Bottom Flange Check',
                                 level_number=2)
        self.report.appendixAISC(self._web.bottom.design.file_out)
        # 
        self.report._report.add_page_break()
        self.report.add_headings(heading_text='A4. Web Top Flange Check',
                                 level_number=2)
        self.report.appendixAISC(self._web.top.design.file_out)         
        #
        #
        self.report.appendixTorsion()
        self.report.appendixCrossHaul()
        #
        self.report._report.save('iLift_demo.docx')
        #print('--end')
    #
#
# 