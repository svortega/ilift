# 
# Copyright (c) 2019 iLift
#
#
# Python stdlib imports
from typing import ClassVar

# package imports
from iLift.cranes.process.clamp import MasterClamp, LocalActions




#
#
class HoistTrolley(MasterClamp):
    """
    """
    __slots__ = ('_type', 'hoist_velocity', 'hoist_class',
                 'Xw', 'n', 'm', 'jaw_size', '_case', 
                 'wheels_number')
    
    def __init__(self, cls_lift:ClassVar):
        """
        """
        #
        MasterClamp.__init__(self, cls_lift) 
        #
        self._type:str = 'trolley'
        self.hoist_velocity:ClassVar = 0 * self._units.m / self._units.s
        self.hoist_class:str = 'HC1'
        # geometry
        self.Xw:ClassVar = 0 * self._units.m
        self.n:ClassVar = 0 * self._units.m
        self.jaw_size = 0 * self._units.m
        #
        self._case:str = 'b'
        self.wheels_number:int = 4
    #
    #
    @property
    def velocity(self) -> ClassVar:
        """
        """
        return self.hoist_velocity
    @velocity.setter
    def velocity(self, value:ClassVar) -> None:
        """
        """
        self.hoist_velocity = value
    #
    @property
    def wheel_centres(self) -> ClassVar:
        """
        Xw : Is the wheel spacing
        """
        return self.Xw
    @wheel_centres.setter
    def wheel_centres(self, value:ClassVar) -> None:
        """
        Xw : Is the wheel spacing
        """
        self.Xw = value
    #
    @property
    def wheel_to_flange_edge(self) -> ClassVar:
        """
        """
        return self.n
    @wheel_to_flange_edge.setter
    def wheel_to_flange_edge(self, value:ClassVar) -> None:
        """
        """
        self.n = value
    #
    #
    @property
    def case(self) -> str:
        """
        """
        return self._case
    @case.setter
    def case(self, value:str) -> int:
        self._case = value
    #
    @property
    def number_wheels(self) -> int:
        """
        Xw : Is the wheel spacing
        """
        return self.wheels_number
    
    @number_wheels.setter
    def number_wheels(self, value:int) -> None:
        """
        Xw : Is the wheel spacing
        """
        self.wheels_number = value    
    # 
    
    
    