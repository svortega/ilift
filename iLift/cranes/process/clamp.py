# 
# Copyright (c) 2019 iLift
#
#
# Python stdlib imports
from typing import NamedTuple, List, ClassVar, Tuple

#
# package imports
#

class LocalActions(NamedTuple):
    """
    Force & bending moments
    """
    Fx:float
    Fy:float
    Fz:float
    Mx:float
    My:float
    Mz:float
#
#

class MasterClamp:
    """ """
    
    __slots__ = ('_lift', '_units', 'Xe',
                 'eccentricity', '_weight', '_angle',
                 '_stress', '_load', 'load_split')
    
    def __init__(self, cls_lift:ClassVar):
        """
        """
        self._lift:ClassVar = cls_lift
        self._units:ClassVar = cls_lift.units
        #
        self.Xe:ClassVar = 0 * self._units.m
        self.eccentricity:ClassVar = 0.10 * self._units.m
        self._weight:ClassVar = 0 * self._units.kg
        self._angle:ClassVar = 15 * self._units.deg
        #
        self._stress:List = []
        self._load:List = []
        #
        self.load_split:float = 0.60
    #
    #@property
    #def sling_angle(self):
    #    """
    #    """
    #    return self._angle
    #@sling_angle.setter
    #def sling_angle(self, value:float):
    #    """
    #    """
    #    self._angle = value
    #
    @property
    def eccentricity_to_flangle(self) -> ClassVar:
        """
        """
        return self.eccentricity
    @eccentricity_to_flangle.setter
    def eccentricity_to_flangle(self, value:ClassVar):
        """
        """
        self.eccentricity = value
    #    
    #@property
    #def local_checks(self):
    #    """
    #    """
    #    return ClampLocalCheck(self)
    #
    @property
    def weight(self) -> ClassVar:
        """
        """
        return self._weight
    @weight.setter
    def weight(self, value:ClassVar):
        """
        """
        self._weight = value
        self._lift.rigging.weight = value
    #
    @property
    def distance_to_beam_end_1(self) -> ClassVar:
        """
        Xe : distance from end 1 of the member to the centreline of the clamp
        """
        return self.Xe
    @distance_to_beam_end_1.setter
    def distance_to_beam_end_1(self, value:ClassVar) -> None:
        """
        Xe : distance from end 1 of the member to the centreline of the clamp
        """
        self.Xe = value
    #
    @property
    def load(self) -> ClassVar:
        """
        """
        return self._load
    
    @load.setter
    def load(self, load:ClassVar) -> Tuple:
        """
        """
        self._load = LocalActions(*load)
        #self._load = Actions(*load)
    #