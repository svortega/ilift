# 
# Copyright (c) 2019 iLift
#
#
# Python stdlib imports
from typing import ClassVar

# package imports
from iLift.cranes.process.clamp import MasterClamp, LocalActions

#
#
#
class GirderClamp(MasterClamp):
    """ """
    __slots__ = ('_type', 'jaw_size', 'jaw_depth')
    
    def __init__(self, cls_lift:ClassVar):
        """
        """
        MasterClamp.__init__(self, cls_lift)
        self._type: str = 'clamp'
        self.jaw_size = None # 0 * self.units.m
        self.jaw_depth = None # 0 * self.units.m
        #
        #print('-->')
    #

    