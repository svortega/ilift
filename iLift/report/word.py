# 
# Copyright (c) 2018 steelpy

# Python stdlib imports
import os
#import math

#
# package imports
#from docx import Document
#from docx.shared import Mm, Pt, RGBColor
#from docx.oxml import parse_xml
#from docx.oxml.ns import nsdecls
##from docx.shared import Pt, RGBColor
#from docx.enum.text import WD_LINE_SPACING, WD_ALIGN_PARAGRAPH
#from docx.enum.style import WD_STYLE_TYPE
##from docx.enum.text import WD_ALIGN_PARAGRAPH
#from docx.enum.table import WD_TABLE_ALIGNMENT
#
import iLift.report.text.introduction as intro
import iLift.report.text.methodology as method

class Report:
    """ """
    def __init__(self, units):
        """
        """
        #
        #
        self._units = units
        self._headings:int = 0
        # open document
        self._report = Document()
        self._report.styles['Normal'].font.name = 'Arial'
        self._report.styles['Normal'].font.size = Pt(10)
        #paragraph_txt = self._report.add_paragraph().add_run()
        #style = self._report.styles['Normal']
        #font = style.font
        #font.name = 'Arial'
        #font.size = Pt(11)
        #
        #
        # get file
        # find path
        #fileDir = os.path.dirname(os.path.realpath('__file__'))        
        #filepath = os.path.join(fileDir, 'iLift/doc/pictures/bplogo.png')
        #filepath = os.path.abspath(os.path.realpath(filepath))
        #
        filepath = self.dir_path(file_name='iLift_logo.png')
        #
        # add picture in the heading
        heading_0 = self._report.add_heading(level=0)
        pic = heading_0.add_run()
        pic.add_picture(filepath, width=Mm(25))
        pic.add_text('Certification of Existing Steelwork Tool')
        #
        _style = heading_0.style
        font = _style.font
        font.name = 'Arial'
        font.size = Pt(22)
        font.color.rgb = RGBColor(0, 0, 0) # black
        #
        #
        # --------------------------------------------------------------------
        #        
        # Leyent main page
        styles = self._report.styles
        new_heading_style = styles.add_style('New Intro', WD_STYLE_TYPE.PARAGRAPH)
        new_heading_style.base_style = styles['Normal']
        font = new_heading_style.font
        font.name = 'Arial'
        font.size = Pt(24)
        font.color.rgb = RGBColor(0, 0, 0) 
        #
        # header 1 style
        new_heading_style = styles.add_style('New Heading 1', WD_STYLE_TYPE.PARAGRAPH)
        new_heading_style.base_style = styles['Heading 1']
        font = new_heading_style.font
        font.name = 'Arial'
        font.size = Pt(14)
        font.color.rgb = RGBColor(0, 153, 0)  # bp green
        #
        # header 2 style
        new_heading_style = styles.add_style('New Heading 2', WD_STYLE_TYPE.PARAGRAPH)
        new_heading_style.base_style = styles['Heading 2']
        font = new_heading_style.font
        font.name = 'Arial'
        font.size = Pt(12)
        font.color.rgb = RGBColor(0, 153, 0)  # bp green
        #
        # header 3 style
        new_heading_style = styles.add_style('New Heading 3', WD_STYLE_TYPE.PARAGRAPH)
        new_heading_style.base_style = styles['Heading 3']
        font = new_heading_style.font
        font.name = 'Arial'
        font.size = Pt(11)
        font.color.rgb = RGBColor(0, 153, 0)  # bp green
        #
        # header 4 style
        new_heading_style = styles.add_style('New Heading 4', WD_STYLE_TYPE.PARAGRAPH)
        new_heading_style.base_style = styles['Heading 4']
        font = new_heading_style.font
        font.name = 'Arial'
        font.size = Pt(10)
        font.color.rgb = RGBColor(0, 153, 0)  # bp green        
        #
        # calcs appendix style
        obj_styles = self._report.styles
        obj_charstyle = obj_styles.add_style('CalcStyle', WD_STYLE_TYPE.CHARACTER)
        obj_font = obj_charstyle.font
        obj_font.size = Pt(8)
        obj_font.name = 'Consolas'
        #
        # Leyent main page
        styles = self._report.styles
        new_heading_style = styles.add_style('TableHeader', WD_STYLE_TYPE.PARAGRAPH)
        new_heading_style.base_style = styles['Normal']
        font = new_heading_style.font
        font.name = 'Arial'
        font.size = Pt(10)
        font.bold = True
        font.color.rgb = RGBColor(0, 0, 0)         
        #
        # --------------------------------------------------------------------
        #
        self._report.add_paragraph('Lift from uncertified beam', style='New Intro')
        self._report.add_page_break()        
        self._report.add_paragraph('Executive Summary', style='New Heading 1')       
        #
        intro.introduction(self)
        #
        #self._report.add_page_break()
        #self._report.add_paragraph('3. Loads and load combinations', style='New Heading 1')
        #self._report.add_paragraph('4. Conclusion and recommendations', style='New Heading 1')
    #
    def dir_path(self, file_name):
        """
        """
        file = 'iLift/report/pictures/' + str(file_name)
        fileDir = os.path.dirname(os.path.realpath('__file__'))        
        filepath = os.path.join(fileDir, file)
        filepath = os.path.abspath(os.path.realpath(filepath))
        return filepath
    #
    def add_headings(self, heading_text:str, level_number:int=1):
        """ """
        style_text = 'New Heading ' + str(level_number)
        self._report.add_paragraph(heading_text, style=style_text)
    #
    def methodology(self, factors, loading):
        """
        """
        method.methodology(self, factors, loading['lifting_angle'])
    #
    def beam_load(self, actions):
        """
        """
        pass
    #
    def lift_item(self, item_data):
        """
        """
        
        self._report.add_paragraph('3.1 Lifting item', style='New Heading 2')
        paragraph = self._report.add_paragraph()
        paragraph.add_run('Lifted object is assumed to be a cubic box. The wind shape coefficients for a '
                          'cubic shape can be defined as recommended in DNV- RP-C205 [4].')
        
        if item_data['size']:
            #
            table = self._report.add_table(rows=1, cols=4)
            table.style='Table Grid'
            table.alignment = WD_TABLE_ALIGNMENT.CENTER
            hdr_cells = table.rows[0].cells
            # create table headings
            self.table_header(hdr_cells, cell_number=0, col_name='Heigth [m]')
            self.table_header(hdr_cells, cell_number=1, col_name='Width [m]')
            self.table_header(hdr_cells, cell_number=2, col_name='Shape Factor')           
            self.table_header(hdr_cells, cell_number=3, col_name='Area [m^2]')
            #
            #for key, item in item_data['size'].items():
            item = item_data['size']
            row_cells = table.add_row().cells
            #
            _cell = row_cells[0].add_paragraph(item['height'])
            _cell.alignment = WD_ALIGN_PARAGRAPH.CENTER
            _cell = row_cells[1].add_paragraph(item['width'])
            _cell.alignment = WD_ALIGN_PARAGRAPH.CENTER
            _cell = row_cells[2].add_paragraph(item['shape_factor'])
            _cell.alignment = WD_ALIGN_PARAGRAPH.CENTER
            _cell = row_cells[3].add_paragraph(item['area'])
            _cell.alignment = WD_ALIGN_PARAGRAPH.CENTER            
            #
            #row_cells[0].text = item['height']
            #row_cells[1].text = item['width']
            #row_cells[2].text = item['shape_factor']
            #row_cells[3].text = item['area']
            #
            #for item, size in item_data:
            #    row_cells = table.add_row().cells
            #    row_cells[0].text = item
            #    row_cells[1].text = str(size)
        #
        self._report.add_paragraph()
        paragraph = self._report.add_paragraph()
        #paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
        #paragraph.add_run("\n")
        paragraph.add_run('Lifting item weight = {:} kg'.format(item_data['weight']))        
    #
    def rigging_item(self, item_data):
        """
        """
        self._report.add_paragraph('3.2 Rigging items', style='New Heading 2')
        paragraph = self._report.add_paragraph()
        paragraph.add_run('Rigging weights to be supported by the beam for the proposed lift are '
                          'provided in the following table:')        
        #
        table = self._report.add_table(rows=1, cols=2)
        table.style='Table Grid'
        table.alignment = WD_TABLE_ALIGNMENT.CENTER
        hdr_cells = table.rows[0].cells
        #
        self.table_header(hdr_cells, cell_number=0, col_name='Item')
        self.table_header(hdr_cells, cell_number=1, col_name='Weight [kg]')
        #
        _sum = []
        for key, item in item_data.items():
            row_cells = table.add_row().cells
            row_cells[0].text = str(key)
            _sum.append(item.convert('kilogram').value)
            row_cells[1].text = "{:1.3f}".format(_sum[-1])
        #
        row_cells = table.add_row().cells
        row_cells[0].text = 'Total'
        row_cells[1].text = "{:1.3f}".format(sum(_sum))

    #
    def loading(self, load, item, rigging):
        """
        """
        self._report.add_page_break()
        self.add_headings(heading_text='3. Loads and load combinations',
                                 level_number=1)
        #
        self._report.add_paragraph('3.1 General', style='New Heading 2')
        paragraph = self._report.add_paragraph()
        paragraph.add_run('Check of the existing uncertified steelwork accounts for the following loads:')
        self._report.add_paragraph('Load to be lifted.', style='List Bullet')
        self._report.add_paragraph('Existing load on the beam.', style='List Bullet')
        self._report.add_paragraph('Self-weight of the beam.', style='List Bullet')
        self._report.add_paragraph('Weight of lifting appliance.', style='List Bullet')
        self._report.add_paragraph('Inertial loads.', style='List Bullet')
        self._report.add_paragraph('Wind loads.', style='List Bullet')       
        #
        self.lift_item(item.file_out())
        self.rigging_item(rigging.file_out())        
        #
        self._report.add_paragraph('3.4 Loads', style='New Heading 2')
        # wind
        item_data = load.file_out()
        _wind = item_data['wind']
        self._report.add_paragraph('3.4.1 Wind load', style='New Heading 3')
        #self._report.add_paragraph()
        paragraph = self._report.add_paragraph()
        paragraph.add_run('Wind load = area ({:1.3f} m^2) x wind pressure ({: 1.3f} kN/m^2) = {:1.3f} kN'
                          .format(_wind[0].convert('metre^2').value, 
                                  _wind[1].convert('kilonewton/metre^2').value,
                                  _wind[2].convert('kilonewton').value))
        # udl
        _udl = item_data['uniform_distributed']
        self._report.add_paragraph('3.4.2 Existing loads', style='New Heading 3')
        paragraph = self._report.add_paragraph()
        paragraph.add_run('Beside the lift load, there are also existing loads applied on the beam, including:')
        self._report.add_paragraph('Weights supported by the beam, such as beam selfweight, '
                                   'plating/grating weight, equipment and pipe weights.', style='List Bullet')
        self._report.add_paragraph('Inertial loads due to platform motions and wind loads applied on the equipment, '
                                   'pipe, and beams.', style='List Bullet')
        self._report.add_paragraph('Member forces due to global structural deformations.', style='List Bullet')
        #
        table = self._report.add_table(rows=1, cols=2)
        table.style='Table Grid'
        table.alignment = WD_TABLE_ALIGNMENT.CENTER
        hdr_cells = table.rows[0].cells
        #
        self.table_header(hdr_cells, cell_number=0, col_name='UDL Item')
        self.table_header(hdr_cells, cell_number=1, col_name='Value [kN/m]')
        #
        _sum = []
        for key, item in enumerate(_udl):
            row_cells = table.add_row().cells
            row_cells[0].text = str(key)
            _sum.append(item.convert('kilonewton/metre').value)
            row_cells[1].text = "{:1.3f}".format(_sum[-1])
        #
        row_cells = table.add_row().cells
        row_cells[0].text = 'Total'
        row_cells[1].text = "{:1.3f}".format(sum(_sum))        
        #
        #
        self._report.add_paragraph('3.4.3 Vertical load', style='New Heading 3')
        paragraph = self._report.add_paragraph()
        paragraph.add_run('Lift load includes the weights of lifted object, beam clamps, slings, '
                          'and equipment used during lift operation. '
                          'The hoist loads factor is conservatively used for the lift loads and includes '
                          'the vertical inertia and uncertainties in allowing for other influences.')
        #
        #
        self._report.add_paragraph('3.4.4 Lateral load', style='New Heading 3')
        paragraph = self._report.add_paragraph()
        paragraph.add_run('Maximum horizontal lift load accounts for:')
        self._report.add_paragraph('Possible lateral loads from out-of-vertical lift angle.', style='List Bullet')
        self._report.add_paragraph('Possible inertia loads from vessel motions during the lift operation.', style='List Bullet')
        self._report.add_paragraph('Possible wind loads during the lift operation.', style='List Bullet')
        #
        #
        self._report.add_paragraph('3.5 Load Combinations', style='New Heading 2')
        paragraph = self._report.add_paragraph()
        paragraph.add_run('Beam is controlled by the loads applying along the transverse direction. '
                          'Hence, the load combination of horizontal lift load, wind load, and inertial load applied '
                          'along the beam axial direction with vertical loads is not addressed.')
    #
    def print_clamp(self):
        """
        """
        self._report.add_page_break()
        self._report.add_paragraph('4. Clamp check', style='New Heading 1')
        paragraph = self._report.add_paragraph()
        paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
        paragraph.add_run('Typical I beam with a clamp hanging a lifted object is shown in the following figure:')
        # --------
        # picture
        filepath = self.dir_path(file_name='beam_clamp.PNG')
        self._report.add_picture(filepath, width=Mm(100))
        last_paragraph = self._report.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER    
        self._report.add_paragraph('Figure 4.1.- Beam Clamp')
        last_paragraph = self._report.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
        #
        self._report.add_paragraph('4.1 Beam local check', style='New Heading 2')
        paragraph = self._report.add_paragraph()
        paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
        paragraph.add_run('Once the beam is confirmed suitable for global analysis, local checks must also be '
                          'undertaken to ensure the flange is not overstressed due to the beam clamp resting on '
                          'the edge of the flange. Further to this, the combined stress of both longitudinal '
                          'bending and transverse bending must be considered.')
        #
        paragraph = self._report.add_paragraph()
        paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
        paragraph.add_run('Bending stress in the flange is calculated based on the total load to be lifted being '
                          'equally distributed over the two jaws of the clamp. '
                          'Each side of the flange is treated as a cantilever.')
        #
        paragraph = self._report.add_paragraph()
        paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
        paragraph.add_run('The load on the end of each cantilever produces a moment at the center line of the web '
                          'which is equal and opposite. '
                          'This ensures that the moment does not need to be resisted by the web of the section. '
                          'The critical section through the flange is not at the point of maximum moment. '
                          'The area considered critical is at the toe of the root radius prior to the increase in '
                          'thickness as the flange passed the web.')        
        # --------
        # picture
        filepath = self.dir_path(file_name='beam_clamp_local2.PNG')
        self._report.add_picture(filepath, width=Mm(100))
        last_paragraph = self._report.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER    
        self._report.add_paragraph('Figure 4.2.- Beam Local Checks')
        last_paragraph = self._report.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
        #
        #
        self._report.add_paragraph('4.1 Flange in local bending check', style='New Heading 2')
        paragraph = self._report.add_paragraph()
        paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
        paragraph.add_run('Area which is assessed for resistance of the load is based on a 60 degrees spread from the jaw '
                          'of the clamp to the root radius and includes the width of the clamp. '
                          'A point load is assumed from the contact of the clamp onto the edge of the flange. '
                          'This is presumed to be the worst case scenario. '
                          'This is then used to calculate the bending stress in the flange. '
                          'The stresses from the major axis bending stress and flange bending stress are then '
                          'combined to ensure the maximum allowable yield stress is not exceeded.')
        # --------
        # picture
        filepath = self.dir_path(file_name='flange_local_check2.png')
        self._report.add_picture(filepath, width=Mm(100))
        last_paragraph = self._report.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER    
        self._report.add_paragraph('Figure 4.3.- Bottom flange local checks')
        last_paragraph = self._report.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER        
        #
        #
        #
        self._report.add_paragraph('4.2 Web in local bending check', style='New Heading 2')
        paragraph = self._report.add_paragraph()
        paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
        paragraph.add_run('This calculation ensures that the flange is adequate for the bending that is induced by the '
                          'vertical component of the load and also by the moment that is generated from the horizontal'
                          'component. Further to this, the horizontal load must also be transferred back through the lower '
                          'flange into the main section. This can be looked at in two ways, either by assuming the lower '
                          'flange spans between the two end connections or by transferring the load to the top flange '
                          'where it is held by the deck above.')
        # --------
        # picture
        filepath = self.dir_path(file_name='web_local_check.png')
        self._report.add_picture(filepath, width=Mm(100))
        last_paragraph = self._report.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER    
        self._report.add_paragraph('Figure 4.3.- Web local local checks')
        last_paragraph = self._report.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER        
        #        
    #
    def print_trolley(self):
        """
        """
        self._report.add_page_break()
        self._report.add_paragraph('4. Trolley check', style='New Heading 1')
        paragraph = self._report.add_paragraph()
        paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
        paragraph.add_run('BS EN 1993-6:2007 covers the design of steel crane supporting structures, '
                          'which includes overhead runway beams. '
                          'This report focuses on runway beams supporting a monorail hoist block travelling '
                          'on the bottom of the flange. '
                          'The bottom flange is subject to a complex state of stress, experiencing direct '
                          'stresses from the global bending, but also local stresses around the wheel positions, '
                          'which vary with the proximity of the hoist to the end of the beam. '
                          'Figure below shows a typical situation, with a four wheeled hoist. '
                          'The local resistance of the flange is based on nominal yield lines, shown in the plan.')
        #
        paragraph = self._report.add_paragraph()
        paragraph.add_run('Typical I beam with a Hoist-Trolley unit is shown in the following figure:')        
        # --------
        # picture
        filepath = self.dir_path(file_name='Hoist_Trolley_unit.PNG')
        self._report.add_picture(filepath, width=Mm(100))
        last_paragraph = self._report.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER    
        self._report.add_paragraph('Figure 4.1 - Hoist-Trolley')
        last_paragraph = self._report.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
        #
        #
        self._report.add_paragraph('4.1 Local Bending Stresses in the bottom flange due to wheel Loads', style='New Heading 2')
        paragraph = self._report.add_paragraph()
        paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
        paragraph.add_run('The following method is used to determine the local bending stresses in the bottom flange of an '
                          'I-section beam, due to wheel loads applied to the bottom fill')
        
        paragraph = self._report.add_paragraph()
        paragraph.add_run('The bending stresses due to wheel loads applied at locations more than b from the end of the beam, '
                          'where b is the flange width, can be determined at the three locations indicated in Figure 5.6.')
        self._report.add_paragraph('Location 0 : the web-to-flange transition.', style='List Bullet')
        self._report.add_paragraph('Location 1 : Centreline of the wheel load.', style='List Bullet')
        self._report.add_paragraph('Location 2 : Outside edge of the flange.', style='List Bullet')
        
    #  
    def conclusions(self, beam, flange,
                    web_bottom, web_top):
        """
        """
        self._report.add_page_break()
        self._report.add_paragraph('5. Conclusions and recommendations', style='New Heading 1')
        self._report.add_paragraph('5.1 Summary', style='New Heading 2')
        self._report.add_paragraph('5.1.1 Global check', style='New Heading 3')
        paragraph = self._report.add_paragraph()
        paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
        paragraph.add_run('Summary of utilisation rates')
        #
        #
        table = self._report.add_table(rows=1, cols=5)
        table.style='Table Grid'
        table.alignment = WD_TABLE_ALIGNMENT.CENTER
        #
        hdr_cells = table.rows[0].cells
        self.table_header(hdr_cells, cell_number=0, col_name='Axis')
        self.table_header(hdr_cells, cell_number=1, col_name='UR Axial')
        self.table_header(hdr_cells, cell_number=2, col_name='UR Shear')
        self.table_header(hdr_cells, cell_number=3, col_name='UR Moment')
        self.table_header(hdr_cells, cell_number=4, col_name='UR Combined')
        #
        row_cells = table.add_row().cells
        row_cells[0].text = 'Mayor axis'
        row_cells[1].text = "{:1.3f}".format(beam.Axial_results.URy)
        row_cells[2].text = "{:1.3f}".format(beam.ChaperG_results.URy)
        row_cells[3].text = "{:1.3f}".format(beam.ChaperF_results.URy)
        row_cells[4].text = "{:1.3f}".format(beam.UR)
        #
        row_cells = table.add_row().cells
        row_cells[0].text = 'Minor axis'
        row_cells[1].text = "{:1.3f}".format(beam.Axial_results.URz)
        row_cells[2].text = "{:1.3f}".format(beam.ChaperG_results.URz)
        row_cells[3].text = "{:1.3f}".format(beam.ChaperF_results.URz)
        row_cells[4].text = "{:1.3f}".format(beam.UR)
        #
        #
        self._report.add_paragraph('5.1.2 Flange check in local bending', style='New Heading 3')
        paragraph = self._report.add_paragraph()
        paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
        paragraph.add_run('Summary of utilisation rates')
        #
        table = self._report.add_table(rows=1, cols=5)
        table.style='Table Grid'
        table.alignment = WD_TABLE_ALIGNMENT.CENTER
        #
        hdr_cells = table.rows[0].cells
        self.table_header(hdr_cells, cell_number=0, col_name='Web')
        self.table_header(hdr_cells, cell_number=1, col_name='UR Axial')
        self.table_header(hdr_cells, cell_number=2, col_name='UR Shear')
        self.table_header(hdr_cells, cell_number=3, col_name='UR Moment')
        self.table_header(hdr_cells, cell_number=4, col_name='UR Combined')
        #
        row_cells = table.add_row().cells
        row_cells[0].text = 'Main axis'
        row_cells[1].text = "{:1.3f}".format(flange.Axial_results.URy)
        row_cells[2].text = "{:1.3f}".format(flange.ChaperG_results.URy)
        row_cells[3].text = "{:1.3f}".format(flange.ChaperF_results.URy)
        row_cells[4].text = "{:1.3f}".format(flange.UR)
        #       
        #
        #
        self._report.add_paragraph('5.1.3 Web bottom and top flange check', style='New Heading 3')
        paragraph = self._report.add_paragraph()
        paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
        paragraph.add_run('Summary of utilisation rates')
        #
        table = self._report.add_table(rows=1, cols=5)
        table.style='Table Grid'
        table.alignment = WD_TABLE_ALIGNMENT.CENTER
        #
        hdr_cells = table.rows[0].cells
        self.table_header(hdr_cells, cell_number=0, col_name='Web')
        self.table_header(hdr_cells, cell_number=1, col_name='UR Axial')
        self.table_header(hdr_cells, cell_number=2, col_name='UR Shear')
        self.table_header(hdr_cells, cell_number=3, col_name='UR Moment')
        self.table_header(hdr_cells, cell_number=4, col_name='UR Combined')
        #
        row_cells = table.add_row().cells
        row_cells[0].text = 'Bottom flange'
        row_cells[1].text = "{:1.3f}".format(web_bottom.Axial_results.URz)
        row_cells[2].text = "{:1.3f}".format(web_bottom.ChaperG_results.URz)
        row_cells[3].text = "{:1.3f}".format(web_bottom.ChaperF_results.URz)
        row_cells[4].text = "{:1.3f}".format(web_bottom.UR)        
        #
        row_cells = table.add_row().cells
        row_cells[0].text = 'Top flange'
        row_cells[1].text = "{:1.3f}".format(web_top.Axial_results.URz)
        row_cells[2].text = "{:1.3f}".format(web_top.ChaperG_results.URz)
        row_cells[3].text = "{:1.3f}".format(web_top.ChaperF_results.URz)
        row_cells[4].text = "{:1.3f}".format(web_top.UR)        
        #        
        #
        self._report.add_paragraph('5.4 Conclusion', style='New Heading 2')
        paragraph = self._report.add_paragraph()
        paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
        if ('pass' in beam.URStatus.lower() and 'pass' in flange.URStatus.lower() and 
            'pass' in web_bottom.URStatus.lower() and 'pass' in web_top.URStatus.lower()):
            paragraph.add_run('Based on the assessments carried out, all the members assesssed have '
                              'been found to remain code compliant with the stated loads applied.')
        else:
            paragraph.add_run('Based on the assessments carried out, the members assesssed have '
                              'failed with the stated loads applied.')            
        #
        #
        self._report.add_paragraph('5.5 Recommendations', style='New Heading 2')
        paragraph = self._report.add_paragraph()
        paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
        paragraph.add_run('The following actions should be taken by the company to confirm '
                          'the validity of conclusion made in this assessment')
        self._report.add_paragraph('Confirm all assumptions made regarding the structural '
                                   'assessment are reasonable.', style='List Bullet')
        self._report.add_paragraph('Confirm there are no other significant loads applied to '
                                   'the designated structural members.', style='List Bullet')
        self._report.add_paragraph('Confirm by inspection the structural members and their connections '
                                   'have not suffered from  mechanical damage or significant corrosion.',
                                   style='List Bullet')
    #
    def references(self):
        """
        """
        self._report.add_page_break()
        self._report.add_paragraph('References', style='New Heading 1')
        
        self._report.add_paragraph('ANSI/AISC 360-16, "Specification for Structural Steel Buildings", '
                                   'AISC, July 2016.', style='List Number')
        self._report.add_paragraph('AISC, "Torsional Analysis of Structured Steel Members", '
                                   '1997.', style='List Number')
        self._report.add_paragraph('BS 2853:2011, "Specification for the testing of steel overhead runway beam for hoist blocks", '
                                   'BSI, 2011.', style='List Number')        
        self._report.add_paragraph('Det Norske Veritas, "Design Against Accidental Loads", ' 
                                   'DNV-RP-C204 Chapter 4, October 2010', style='List Number')
        self._report.add_paragraph('Eurocode 3, "Design of steel structures - Part 6: Crane supporting structures", ' 
                                   'July 2009', style='List Number')

    #    
    def appendixAISC(self, text, space_before=0, space_after=0, line_spacing=0):
        """ 
        print appendix code check results
        """
        #
        paragraph = self._report.add_paragraph()
        paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
        #
        if isinstance(text, (list, tuple)):
            for line in text:
                paragraph.add_run(line, style = 'CalcStyle')
        #elif isinstance(value, Number):
        #    item.append(value)
        #else:
        #    raise ValueError('{:} item {:} not a number'
        #                     .format(item_type, value))
    #
    def appendixTorsion(self):
        """
        """
        self._report.add_page_break()
        self.add_headings(heading_text='Appendix B.- AISC Torsional Analysis of Steel Members',
                          level_number=1)        
        method.Appendix_torsion(self)
    #
    def appendixCrossHaul(self):
        """
        """
        self._report.add_page_break()
        self.add_headings(heading_text='Appendix C.- Cross Haul Check Procedure',
                          level_number=1)        
        method.Appendix_CrossHaul(self)
    #
    def table_header(self, cells, cell_number:int, col_name:str):
        """
        """
        shading_elm = parse_xml(r'<w:shd {} w:fill="5AD146"/>'.format(nsdecls('w')))
        _cell = cells[cell_number].add_paragraph(col_name, style='TableHeader')
        _cell.alignment = WD_ALIGN_PARAGRAPH.CENTER
        cells[cell_number]._tc.get_or_add_tcPr().append(shading_elm)
#