# 
# Copyright (c) 2018 steelpy

# Python stdlib imports
#import os

#
# package imports
#from docx.shared import Mm
#from docx.enum.text import WD_LINE_SPACING, WD_ALIGN_PARAGRAPH
#from docx.enum.table import WD_TABLE_ALIGNMENT


def methodology(self, factors, lift_angle):
    """
    """
    self._report.add_page_break()
    self._report.add_paragraph('2. Methodology', style='New Heading 1')
    self._report.add_paragraph('2.1. Beam assessment', style='New Heading 2')
    self._report.add_paragraph('2.1.1. Global stress', style='New Heading 3')
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('The bending stresses in the extreme fibers of the beam element '
                      'should be checked to account for both:')
    self._report.add_paragraph('The longitudinal global stress (f1) from the span of the beam.', style='List Bullet')
    self._report.add_paragraph('The transverse local stress (f2) from the local deflection of'
                               'the flange supporting the beam clamp/trolley.', style='List Bullet')
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('Further to this, if a cross haul operation is undertaken, '
                      'an out of plane load will be produced; therefore the minor axis bending stress'
                      '(f3) must also be considered.')
    paragraph.add_run('The bending stress in the beam (f1) is calculated in the traditional fashion for major axis bending:')
    #
    #
    self._report.add_paragraph('2.1.2. Local stress', style='New Heading 3')
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('The bending stress in the flange (f2) is calculated based on the total load to be lifted'
                      'being equally distributed over the two jaws of the clamp. Each side of the flange'
                      'is then treated as a cantilever. ' 
                      'The load on the end of each cantilever produces a moment at the centre line of the web '
                      'which is equal and opposite. This ensures that the moment does not need to be resisted by'
                      'the web of the section. The critical section through the flange is not at the point of maximum moment.'
                      'The area considered critical is at the toe of the root radius prior to the increase in thickness'
                      'as the flange passed the web.')
    #paragraph = self._report.add_paragraph()
    #paragraph.add_run('The area which is assessed for resistance of the load is based on a 60� spread from the jaw of the clamp'
    #                  'to the root radius and will also include the width of the clamp.')
    #
    #
    self._report.add_paragraph('2.1.3. Beam Deflections', style='New Heading 3')
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('If the design stresses are within acceptable limits for any given national code then the beam must'
                      'finally be checked to ensure excessive deformation will not occur under the lifting operation. '
                      'Serviceability limits can be used as guidance (i.e. span / 360), but each beam under consideration '
                      'will be dependant on what it already supports and engineering judgment must be used to decide on '
                      'a maximum allowable deflection on a case by case basis.')
    #
    dataAssumtions(self, factors, lift_angle)
    #
#
def dataAssumtions(self, factors, lift_angle):
    """
    """
    self._report.add_paragraph('2.2 Data and assumptions', style='New Heading 2')
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('The following assumptions have been made for this assessment:')
    #
    # lifting angle
    self._report.add_paragraph('2.2.1 Lifting angle', style='New Heading 3')
    # self._report.add_paragraph()
    paragraph = self._report.add_paragraph()
    paragraph.add_run('The maximum allowable angle between the lift sling and vertical direction is {:} degrees '
                      .format(lift_angle.convert('degree').value))
    paragraph.add_run('and this maximum allowable angle includes the motions introduced by wind '
                      'and inertial loads on the lifted object.')
    #
    # item_data
    self._report.add_paragraph('2.2.2 Factors', style='New Heading 3')
    paragraph = self._report.add_paragraph()
    paragraph.add_run('Lift load accounts for the possible dynamic amplification during the lift operation '
                      'and can include a lift factor that represents the possible increase in load due to vessel motions. '
                      'The DAF ranges from 1.00 to 1.30 and differentiates between manual hoisting or electric hoisting.')    
    #
    table = self._report.add_table(rows=1, cols=3)
    table.style='Table Grid'
    table.alignment = WD_TABLE_ALIGNMENT.CENTER
    hdr_cells = table.rows[0].cells
    #
    self.table_header(hdr_cells, cell_number=0, col_name='Factor')
    self.table_header(hdr_cells, cell_number=1, col_name='Vertical')
    self.table_header(hdr_cells, cell_number=2, col_name='Horizontal')
    #
    daf_factor = factors['daf']
    row_cells = table.add_row().cells
    row_cells[0].text = 'DAF'
    row_cells[1].text = "{:1.3f}".format(daf_factor['vertical'])
    row_cells[2].text = "{:1.3f}".format(daf_factor['lateral'])
    #
    vessel_factor = factors['vessel']
    if vessel_factor:
        row_cells = table.add_row().cells
        row_cells[0].text = 'Vessel'
        row_cells[1].text = "{:1.3f}".format(vessel_factor['vertical'])
        row_cells[2].text = "{:1.3f}".format(vessel_factor['lateral'])

#
#
def Appendix_torsion(self):
    """
    """
    #
    self._report.add_paragraph('B.1. Introduction', style='New Heading 2')
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('Under normal design conditions, the steel designer assumes the loads are perpendicular to the centroidal'
                      'axes and pass through the centroid. Inherently, these assumptions negate torsional effects, because the '
                      'sections are doubly symmetric, and thus the centroid and  shear centre coincide. '
                      'The shear centre is the longitudinal axis about which a section would rotate if only torsional forces can'
                      'act through this axis, only bending actions of deformation will occur. '
                      'If the loads are eccentric with respect to shear centre, the vertical loads may be positioned through the '
                      'shear centre with balancing torsional forces.')
    paragraph = self._report.add_paragraph()
    paragraph.add_run('The response of each action is then evaluated separately and superimposed to give the final solution.')
    paragraph = self._report.add_paragraph()
    paragraph.add_run('This approach first requires determination of the shear centre, and then the resulting torsional'
                      'stress and deformations.')
    #
    self._report.add_paragraph('B.2. Torsion fundamentals', style='New Heading 2')
    self._report.add_paragraph('B.2.1 Shear Centre', style='New Heading 3')
    paragraph = self._report.add_paragraph()
    paragraph.add_run('The shear center is the point through which the applied loads must pass to produce bending without twisting.'
                      'If a shape has a line of symmetry, the shear center will always lie on that line; for cross-sections with '
                      'two lines of symmetry, the shear center is at the intersection of those lines (as is the centroid).')
    #
    self._report.add_paragraph('B.2.3 Resistance of a cross-section to a torsional moment', style='New Heading 3')
    paragraph = self._report.add_paragraph()
    paragraph.add_run('At any point along the length of a member subjected to a torsional moment, the cross-section will rotate '
                      'through an angle theta as shown in Figure 2.1. '
                      'For non-circular cross-sections this rotation is accompanied by warping; that is, transverse sections '
                      'do not remain plane. '
                      'If this warping is completely unrestrained, the torsional moment resisted by the cross-section is:')
    # --------
    # picture
    filepath = self.dir_path(file_name='Torsion_fig2_2.PNG')
    self._report.add_picture(filepath, width=Mm(100))
    last_paragraph = self._report.paragraphs[-1] 
    last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER    
    self._report.add_paragraph('Figure 2.1')
    last_paragraph = self._report.paragraphs[-1] 
    last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
    #
    self._report.add_paragraph('B.3. General torsional theory', style='New Heading 2')
    self._report.add_paragraph('B.3.1. Torsional properties', style='New Heading 3')
    paragraph = self._report.add_paragraph()
    paragraph.add_run('Torsional properties J, a, C_w, W_ns and S_ws are necessary for the solution of the above equations. '
                      'The terms J, a and C_w are properties of the entire cross-section. '
                      'The terms W_ns and S_ws vary at different points on the cross-section.')
    #
    self._report.add_paragraph('B.3.2. Torsional functions', style='New Heading 3')
    paragraph = self._report.add_paragraph()
    paragraph.add_run('In addition to the torsional properties given in Section 2.3.2.1, the torsional rotation theta and '
                      'its derivatives are necessary for the solution of equations.')
    #
    #
    self._report.add_paragraph('B.4. Analysis for torsion', style='New Heading 2')    
    self._report.add_paragraph('B.4.1 Torsional stress on I-shape sections', style='New Heading 3')
    paragraph = self._report.add_paragraph()
    paragraph.add_run('Shapes of open cross-section tend to warp under torsional loading. If this warping is unrestrained,'
                      'only pure torsional stresses are present. However, when warping is restrained, additional direct '
                      'shear stresses as well as longitudinal stresses due to warping must also be considered. '
                      'Pure torsional shear stresses, shear stresses due to warping, and normal stresses due to warping are'
                      'each related to the derivatives of the rotational function theta. Thus, when the derivatives of theta '
                      'are determined along the girder length, the corresponding stress conditions can be evaluated. '
                      'The calculation of these stresses is described in the following sections.')
    #
    self._report.add_paragraph('B.4.2 Pure torsional shear stress', style='New Heading 3')
    paragraph = self._report.add_paragraph()
    paragraph.add_run('These shear stresses are always present on the cross-section of a member subjected to a torsional '
                      'moment and provide the resisting moment T_t as described in Section 2.2. '
                      'These are in-plane shear stresses that vary linearly across the thickness of an element of the '
                      'cross-section and act in a direction parallel to the edge of the element. '
                      'They are maximum and equal, but of opposite direction, at the two edges. '
                      'The maximum stress is determined by the equation:')
    #
    paragraph.add_run('The pure torsional shear stresses will be largest in the thickest elements of the cross-section. '
                      'These stress states are illustrated in Figures 2.3.1b, 2.3.2b, and 2.3.3b for I-shapes.')
    # --------
    # picture
    filepath = self.dir_path(file_name='Torsion_fig4_1.png')
    self._report.add_picture(filepath, width=Mm(100))
    last_paragraph = self._report.paragraphs[-1] 
    last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER    
    self._report.add_paragraph('Figure 2.3')
    last_paragraph = self._report.paragraphs[-1] 
    last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER    
    #
    self._report.add_paragraph('B.4.3 Shear stress due to warping', style='New Heading 3')
    paragraph = self._report.add_paragraph()
    paragraph.add_run('When a member is allowed to warp freely, these shear stresses will not develop. '
                      'When warping is restrained, these are in-plane shear stresses that are constant across the '
                      'thickness of an element of the cross-section, but vary in magnitude along the length of the element. '
                      'They act in a direction parallel to the edge of the element. '
                      'The magnitude of these stresses is determined by the equation:')
    #
    self._report.add_paragraph('B.4.4 Normal stress due to warping', style='New Heading 3')
    paragraph = self._report.add_paragraph()
    paragraph.add_run('When a member is allowed to warp freely, these normal stresses will not develop. '
                      'When warping is restrained, these are direct stresses (tensile and compressive) resulting from bending '
                      'of the element due to torsion. '
                      'They act perpendicular to the surface of the cross-section and are constant across the '
                      'thickness of an element of the cross-section but vary in magnitude along the length of the element. '
                      'The magnitude of these stresses is determined by the equation:')
    #
    self._report.add_paragraph('B.4.5 Approximate shear and normal stress due to warping on I-shape sections',
                               style='New Heading 3')
    paragraph = self._report.add_paragraph()
    paragraph.add_run('The shear and normal stresses due to warping may be approximated for short-span I-shapes by resolving '
                      'the torsional moment T into an equivalent force couple acting at the flanges as illustrated in Figure 4.4. '
                      'Each flange is then analyzed as a beam subjected to this force. '
                      'The shear stress at the center of the flange is approximated as:')
    #
    #
    self._report.add_paragraph('B.5. Combining torsional stress with other stresses', style='New Heading 2')    
    paragraph = self._report.add_paragraph()
    paragraph.add_run('To determine the total stress condition, the stresses due to torsion are combined algebraically with all'
                      'other stresses using the principles of superposition. The total normal stress f_n is:')
    #
    paragraph = self._report.add_paragraph()
    paragraph.add_run('and the total shear stress fv, is:')
    #
    paragraph = self._report.add_paragraph()
    paragraph.add_run('The following terms sigma_w and tau_w may be taken as zero in the following cases:')
    self._report.add_paragraph('Members for which warping is unrestrained.', style='List Bullet')
    self._report.add_paragraph('single-angle members.', style='List Bullet')
    self._report.add_paragraph('structural tee members.', style='List Bullet')
    #
    paragraph = self._report.add_paragraph()
    paragraph.add_run('In the foregoing, it is imperative that the direction of the stresses be carefully observed. '
                      'The positive direction of the torsional stresses as used in the sign convention presented herein '
                      'is indicated in Figure 4.1.'
                      'In the sketches accompanying each figure, the stresses are shown acting on a cross-section of the '
                      'member located at distance z from the left support and viewed in the direction indicated in Figure 4.1.')
    #
    paragraph = self._report.add_paragraph()
    paragraph.add_run('For I-shapes, and are both at their maximum values at the edges of the flanges as shown in Figures 4.1 and 4.8. '
                      'Likewise, there are always two flange tips where these stresses add regardless of the directions of the '
                      'applied torsional moment and bending moment. '
                      'Also for I-shapes, the maximum values of and in the flanges will always add at some point regardless of the '
                      'directions of the applied torsional moment and vertical shear to give the maximum shear stress in the flange. '
                      'For the web, the maximum value of tau_b adds to the value of tau_t in the web, regardless of the direction of '
                      'loading, to give the maximum shear stress in the web. Thus, for I-shapes, Equations may be simplified as follows:')
    #
    paragraph = self._report.add_paragraph()
    paragraph.add_run('Determining the maximum values of the combined stresses for all types of shapes is somewhat cumbersome because '
                      'the stresses and are not all at their maximum values at the same transverse cross-section along the length of the member. '
                      'Therefore, in many cases, the stresses should be checked at several locations along the member.')
    # --------
    # picture
    filepath = self.dir_path(file_name='Torsion_fig4_8.png')
    self._report.add_picture(filepath, width=Mm(120))
    last_paragraph = self._report.paragraphs[-1] 
    last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER    
    self._report.add_paragraph('Figure 2.4')
    last_paragraph = self._report.paragraphs[-1] 
    last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
#
def Appendix_CrossHaul(self):
    """
    """
    #
    #self._report.add_paragraph('C.1. Cross haul check', style='New Heading 2')
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('If a cross haul operation is undertaken then further calculations to those stated for the direct '
                      'lift must be undertaken to ensure the beam is still satisfactory.'  
                      'As a rule of thumb the cross haul capacity will be approximately half of that for a direct lift'
                      'due to the additional loads induced by the lateral action on the flange. '
                      'This issue is discussed in more detail here:')
    paragraph = self._report.add_paragraph()
    paragraph.add_run('The bending stress in the beam (f1) is calculated in the traditional fashion for major axis bending:')
    #
    # --------
    # picture
    filepath = self.dir_path(file_name='CrossHaul.PNG')
    self._report.add_picture(filepath, width=Mm(50))
    last_paragraph = self._report.paragraphs[-1] 
    last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER    
    self._report.add_paragraph('Figure C.1 - Cross Haul')
    last_paragraph = self._report.paragraphs[-1] 
    last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER    
    #
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('A cross haul operation provides much more complex behavior within the bottom flange than a direct lift'
                      'for the calculation of f2. The eccentricity between the centre line of the beam clamp pin and the '
                      'centre line of the flange will produce an additional moment on the flange, which depending on the angle'
                      'of pull may significantly increase the bending moment in the flange. Further to this, because of the '
                      'imbalance between each side of the flange the web is also required to resist the moment.')
    #
    # 
    # --------
    # picture
    filepath = self.dir_path(file_name='CrossHaul_check.PNG')
    self._report.add_picture(filepath, width=Mm(100))
    last_paragraph = self._report.paragraphs[-1] 
    last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER    
    self._report.add_paragraph('Figure C.2 - Effect of cross haul on lower flange')
    last_paragraph = self._report.paragraphs[-1] 
    last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER    
    #
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('When a direct lift is undertaken, the bending stress is assessed at the toe of the root radius as this '
                      'is the point on the flange where the thickness increases as it approached the web. This is also true '
                      'of the cross haul, except the web of the beam must also be considered. '
                      'The activated flange and web lengths to resist the moment are calculated in the same way as previously '
                      'discussed. Further to this, the lateral load (F_HS) and its affect on the bottom flange must be '
                      'assessed. '
                      'This is normally done by considering whether the bottom flange can transfer the lateral load in '
                      'bending back to the support points. This will only work however, if the bottom flange is restrained at '
                      'the supports, otherwise the lateral load must be taken to the mid point of the beam and will '
                      'therefore increase the bending moment in the web at the connection to the upper flange.')
    #
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('The stresses from the major axis bending stress and flange bending stress are combined to ensure the '
                      'maximum allowable yield stress is not exceeded. Further to this the stress in the web must also be '
                      'check to ensure it has sufficient capacity against the moments due to rotation.  '
                      'As with a vertical lift the maximum allowable deflection under will be dependant on what the existing'
                      'beam already supports and engineering judgment must be used to decide on maximum allowable deflection'
                      'on a case-by-case basis')
    #