# 
# Copyright (c) 2018 steelpy

# Python stdlib imports

#
# package imports
#from docx.enum.text import WD_LINE_SPACING
##from docx.enum.style import WD_STYLE_TYPE
#from docx.enum.table import WD_TABLE_ALIGNMENT


def introduction(self):
    """
    """
    self._report.add_page_break()
    self._report.add_paragraph('1. Introduction', style='New Heading 1')
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('In some circumstances it may not be possible to lift and move equipment using a runway beam. '
                      'In these situations, the existing steelwork around the worksite may be required to aid in the removal. '
                      'It is the company responsibility to ensure that any steelwork used in lifting operation'
                      'has been checked and is fit for purpose.  '
                      'In order to do this, a lift plan should confirm the following:')
    
    self._report.add_paragraph('Size and weight of the package to be moved.', style='List Bullet')
    self._report.add_paragraph('The beams that are to be used in vertical lift and cross haul operations', 
                               style='List Bullet')
    self._report.add_paragraph('The route the load will take to reach final laydown location.', style='List Bullet')
    self._report.add_paragraph('The equipment that will be required to undertake the operation' 
                               '(i.e. beam clamps, chain block etc.).',
                               style='List Bullet')
    #
    #
    self._report.add_paragraph('1.1 Initial assessment', style='New Heading 2')
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE        
    paragraph.add_run('To assess whether a beam is suitable for the proposed lift load the following should be checked:')
    
    self._report.add_paragraph('The size, section weight and steel grade of the existing beam.', style='List Bullet')
    self._report.add_paragraph('The condition of the existing beam. i.e. significant corrosion.', style='List Bullet')
    self._report.add_paragraph('The condition of the connections to any supporting steelwork.', style='List Bullet')
    self._report.add_paragraph('The overall span of the beam and any other significant fixity (i.e. flange restraint).',
                               style='List Bullet')
    self._report.add_paragraph('Current loading without the addition of lifting load.', style='List Bullet')
    #
    #
    self._report.add_paragraph('1.2 Design loads', style='New Heading 2')
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('When undertaking checks of existing steelwork account of the following loads should be taken:')
    
    self._report.add_paragraph('The Load to be lifted.', style='List Bullet')
    self._report.add_paragraph('Any existing load on the beam.', style='List Bullet')
    self._report.add_paragraph('The self weight of the beam.', style='List Bullet')
    self._report.add_paragraph('The weight of lifting appliance.', style='List Bullet')
    self._report.add_paragraph('The amount of effort that will be exerted on the hand chain to assess a DAF.',
                               style='List Bullet')
    
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('Most lifting operations using uncertified steelwork will involve manual pull chains.'
                      'As a result the dynamic amplification factor will be in line with runway beam design,'
                      'i.e. 10% of total lift weight.'
                      'Otherwise, if motorised lifting equipment is used then a 25% DAF may be applicable.')
    paragraph = self._report.add_paragraph()
    
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('For floating systems, and additional horizontal force is considered, '
                      'usually 15% of the total vertical load.')
    #
    #
    self._report.add_paragraph('1.3 Existing loads', style='New Heading 2')
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('In addition to the lifting loads, are existing loads applied to the beam.'
                      'The existing loading can be estimated:')
    
    self._report.add_paragraph('From a global topside analysis.', style='List Bullet')
    self._report.add_paragraph('Assuming the existing loading represents 80% of the member utilization.',
                               style='List Bullet')
    #
    #
    self._report.add_paragraph('1.4 Lifted object', style='New Heading 2')
    paragraph = self._report.add_paragraph()
    paragraph.line_spacing_rule = WD_LINE_SPACING.SINGLE
    paragraph.add_run('The lift load includes:')
    
    self._report.add_paragraph('The weights of lifted object.', style='List Bullet')
    self._report.add_paragraph('Clamp, hoist trolley, etc.', style='List Bullet')
    self._report.add_paragraph('Slings/spreader.', style='List Bullet')
    self._report.add_paragraph('Equipment used during lift operation.', style='List Bullet')
#
