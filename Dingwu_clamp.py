#
import iLift as iLift
#
lifting = iLift.Lifting_Tool()
units = lifting.units
#
# Cross Haul
lifting.cross_hauling_angle.transverse = 4.0 * units.deg
lifting.cross_hauling_angle.longitudinal = 20.0 * units.deg
#
# DAF factors
factors = lifting.factors
factors.DAF.vertical = 1.0
factors.DAF.transverse = 1.0
factors.DAF.longitudinal = 1.0
#
# add beam section
beam = lifting.beam
beam.UR = 0.0  # current beam UR
# beam boundary condition setup
beam.span_between_supports = 13 * units.foot + 6 * units.inch
#
# add material
material = beam.material
material.Fy = 36 * units.ksi
material.Fu = 410 * units.MPa
material.E = 200_000 * units.MPa
material.G = 79_300 * units.MPa
material.density = 0 * units.kg / units.m**3
# set section
Ibeam = beam.Isection
Ibeam.d = 12.22 * units.inch
Ibeam.tw = 0.23 * units.inch
Ibeam.bf = 6.49 * units.inch
Ibeam.tf = 0.38 * units.inch
Ibeam.r = 0.635 * units.inch
#
#
#
# add clamp-crane
clamp = lifting.clamp
clamp.jaw_size = 5 * units.inch
#clamp.weight = 55 * units.lbs
clamp.eccentricity_to_flangle = 4 * units.inch
clamp.load_split = 0.50
#
# add item to be lifted
exchanger = lifting.item
exchanger.weight = 4.8 * 1000  * units.lbs
#
# Weight of lifting equipment (assumed)
rigging = lifting.rigging
#rigging.weight = 0.20 * 1000 * units.lbs
#
# print load summary
load = lifting.load
print('Load')
print('vertical = {: 1.3f} kips'
      .format(load.lift_load.vertical.convert('kilolbf').value))
print('Across  = {: 1.3f} kips'
      .format(load.lift_load.transverse.convert('kilolbf').value))
print('Along  = {: 1.3f} kips'
      .format(load.lift_load.longitudinal.convert('kilolbf').value))
#
beam.code_check()
beam.local_code_check()
#
# Results
#
#
# Adopted lengths
_flange_bottom = beam.flange[1].properties.bottom
_web_bottom = beam.web[1].properties.bottom
_web_top = beam.web[1].properties.top
#
#
#
print('')
_flanges = {}
_flanges_stress = {}
for key, _item in beam.flange.items():
    _flanges[key] = _item.code_check
    _flanges_stress[key] = _item.stress

for key, _stress in _flanges_stress.items():
    print('flange bottom :', key)
    # calculate von mises
    _von_mises = _stress.bottom.von_mises()
    # loop 9 stress points in the element
    for x, _mises in enumerate(_von_mises):
        # Beam lift stresses - data update
        print('')
        print("sigma x = {:1.3f} ksi".format(_stress.bottom.sigma_x[x].convert('kilopsi').value))
        print("sigma y = {:1.3f} ksi".format(_stress.bottom.sigma_y[x].convert('kilopsi').value))
        print("sigma z = {:1.3f} ksi".format(_stress.bottom.sigma_z[x].convert('kilopsi').value))
        print("tau x {:1.3f} ksi".format(_stress.bottom.tau_x[x].convert('kilopsi').value))
        print("tau y {:1.3f} ksi".format(_stress.bottom.tau_y[x].convert('kilopsi').value))
        print("tau z {:1.3f} ksi".format(_stress.bottom.tau_z[x].convert('kilopsi').value))
        print("von Mises {:1.3f} ksi".format(_mises.convert('kilopsi').value))
#
#
print('Finish')