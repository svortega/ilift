#
import iLift as iLift
#
lifting = iLift.Lifting_Tool()
units = lifting.units
#
# Cross Haul
lifting.cross_hauling_angle.transverse = 15 * units.deg
lifting.cross_hauling_angle.longitudinal = 0 * units.deg
#
# DAF factors
factors = lifting.factors
factors.DAF.vertical = 1.10
factors.DAF.horizontal = 1.10
# 
#
#
# add beam section
beam = lifting.beam
beam.UR = 0.80  # current beam UR
beam.span_between_supports = 1 * units.m
# add material
material = beam.material
material.Fy = 250 * units.MPa
material.Fu = 410 * units.MPa
material.E = 200_000 * units.MPa
material.G = 79_300 * units.MPa
material.density = 8050 * units.kg / units.m**3
# set section
Ibeam = beam.Isection
Ibeam.d = 933.2 * units.mm
Ibeam.tw = 24 * units.mm
Ibeam.bf = 423 * units.mm
Ibeam.tf = 42.7 * units.mm
Ibeam.r = 115.5 * units.mm
#
# add clamp-crane
clamp = lifting.clamp
clamp.jaw_size = 70 * units.mm
clamp.weight = 10 * units.kg
clamp.eccentricity_to_flangle = 100 * units.mm
clamp.load_split = 0.60
#
#
# Find safe lifting load 
lifting.safe_lifting_load(allowable_UR = 1.0,
                          actual_UR = 0.80)
#
print('Finish')