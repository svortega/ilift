#
import iLift as iLift
#
lifting = iLift.Lifting_Tool()
units = lifting.units
#
# Cross Haul 
lifting.cross_hauling_angle.transverse = 15 * units.deg
lifting.cross_hauling_angle.longitudinal = 15 * units.deg
#
# DAF
factors = lifting.factors
factors.DAF.vertical = 1.10
#
# Weight of lifting equipment (assumed)
#rigging = lifting.rigging
#rigging.weight = 25.50 * units.kg
#
# set wind speed
wind = lifting.wind
wind.velocity = 15 * units.m / units.s
wind.direction = 90 * units.deg
#
#
# add beam section
beam = lifting.beam
#beam.UR = 0.80  # current beam UR
beam.span_between_supports = 4 * units.m
#
# add material
material = beam.material
material.Fy = 250 * units.MPa
material.Fu = 410 * units.MPa
material.E = 200_000 * units.MPa
material.G = 79_300 * units.MPa
material.density = 7850 * units.kg / units.m**3
#
# set section
Ibeam = beam.Isection
Ibeam.d = 1008.10 * units.mm
Ibeam.tw = 21.10 * units.mm
Ibeam.bf = 302 * units.mm
Ibeam.tf = 40 * units.mm
Ibeam.r = 30 * units.mm
#
# add clamp-crane
clamp = lifting.clamp
clamp.jaw_size = 100 * units.mm
#clamp.weight = 10 * units.kg
clamp.eccentricity_to_flangle = 100 * units.mm
clamp.load_split = 0.50
#clamp.distance_to_beam_end_1 = 0.50 * units.m
#
#
door = lifting.item
#door.weight = 2.53 * units.kN / units.gravity
door.height = 1.0 * units.m
door.width = 1.0 * units.m
door.shape_factor = 1.00
#
# Find safe lifting load 
lifting.safe_lifting_load(allowable_UR = 1.0,
                          actual_UR = 0.80)
#
print('Finish')
