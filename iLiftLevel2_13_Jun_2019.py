import sys
print(sys.path)

sys.path.append("NumPy_path")

#
import iLift as iLift
#
lifting = iLift.Lifting_Tool()
units = lifting.units
version = iLift.__version__

print("iLift Version : {:}".format(version))

import pyodbc
import datetime

#
# create database connection
#server = 'zscugo0n18cimrsgsrv00.database.windows.net'
#database = 'OFSiLiftPreProdDB'
#username = 'SqlAdmin-ZSCUGO0N18CIMRSGsrv00'
#password = 'OFScim_123'

server = 'zscugo1p20cimrsgsrv00.database.windows.net'
database = 'OFSiLiftProdDB'
username = 'SqlAdmin-ZSCUGO1P20CIMRSGsrv00'
password = 'OFScim_1q12w23e3'

driver= '{ODBC Driver 13 for SQL Server}'

cnxn = pyodbc.connect('DRIVER='+driver+';SERVER='+server+';PORT=1433;DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = cnxn.cursor()

intLiftID = 198      # 37 38 34                         # unique lift ID

#
# read liftproperties
cursor.execute(	" SELECT "
                " BeamSectionSize  "	        # [0] beam section size
                ", BeamFixity  "	        # [1] beam fixity
                ", BeamExistingIR  "	        # [2] beam existing IR

                ", FY_Nmm2 "		        # [3] beam yield strength N/mm^2
                ", E_Nmm2 "		        # [4] beam young's modulus N/mm^2
                ", Fu_Nmm2 "		        # [5] beam ultimate strength N/mm^2
                ", Density_kgm3 "	        # [6] beam density kg/m^3
                ", PoissonsRatio "	        # [7] poissons ratio

                ", BeamLength_m "		# [8] beam length m
                ", BeamUnbracedLength_m "	# [9] beam unbraced length m
                ", BeamOverhangLength_m "	# [10] beam overhang length m
                ", BeamOverhangUnbracedLength_m "   # [11] beam overhang unbraced length m
                ", LoadLocation_m "             # [12] beam overhang unbraced length m

                ", CraneWeight_kg "		# [13] clamp mass kg
                ", HoistWeight_kg "	        # [14] hoist mass kg
                ", EquipmentWeight_kg "	        # [15] equipment mass kg

                ", AirDensity_kgm3 "	        # [16] air density kg/m^3
                ", LateralDAF "		        # [17] DAF lateral  
                ", VerticalDAF "	        # [18] DAF vertical
                ", HorizontalDAF "	        # [19] DAF horizontal
                ", LateralAngle "	        # [20] lift angle lateral 
                ", HorizontalAngle "	        # [21] lift angle horizontal

                ", WindSpeed_ms "	        # [22] wind speed m/s
                ", WindDirection "	        # [23] wind direction
                ", EquipmentArea_m2 "		# [24] lift area m^2
                ", EquipmentShapeFactorCa "	# [25] equipment shape coefficient
                ", BeamShapeFactorCs "	        # [26] beam shape coefficient

                ", LateralAcceleration "        # [27] intertia lateral
                ", HorizontalAcceleration "     # [28] inertia horizontal
                ", VerticalAcceleration "       # [29] intertia vertical

                ", EquipmentHeight_m "          # [30] equipment height
                ", EquipmentWidth_m "           # [31] equipment width
                ", EquipmentLength_m "          # [32] equipment length

                ", ClampJawSize_mm "		# [33] clamp jaw size
                ", ClampOffset_mm "	        # [34] clamp eccentricity
                ", ClampLoadSplit "	        # [35] clamp load split
            
                ", LiveLoadUDL_kNm2 "	        # [36] beam UDL live
                ", DeadLoadUDL_kNm2 "	        # [37] beam UDL dead
                ", BeamSpacing_m "	        # [38] beam spacing

                ", BeamType "	                # [39] beam type
                ", BeamShearAISC "	        # [40] beam shear AISC

                ", BeamCMy "	                # [41] beam moment modifier y-axis
                ", BeamCMz "	                # [42] beam moment modifier z-axis

                ", BeamKy "	                # [43] beam effective length factor y-axis
                ", BeamKz "	                # [44] beam effective length factor z-axis

                " FROM tblProjectBeamLiftInput"
                " WHERE LiftID = ?", intLiftID)

#
# fetch all of the rows from the query
liftdata = cursor.fetchall ()

print('')
print("Lift input read from database")

#
# set lift properties
for row in liftdata :
    beam_SectionSize = row[0]	                # [0] beam section size
    beam_Fixity = row[1]	                # [1] beam fixity
    beam_ExistingIR = row[2]	                # [2] beam existing IR

    section_Fy = row[3]                         # [3] beam yield strength N/mm^2
    section_E = row[4]                          # [4] beam young's modulus N/mm^2
    section_Fu = row[5]                         # [5] beam ultimate strength N/mm^2
    section_Density = row[6]                    # [6] beam density kg/m^3 
    section_v = row[7]                          # [7] poissons ratio 

    section_L = row[8]                          # [8] beam length m 
    section_Lb = row[9]	                        # [9] beam unbraced length m
    section_Lo = row[10]                        # [10] beam overhang length m 
    section_Lbo = row[11]                       # [11] beam overhang unbraced length m
    load_location = row[12]                     # [12] load location m                              #### Salvador, I have added

    lift_CW = row[13]                           # [13] clamp mass kg 
    lift_HW = row[14]                           # [14] hoist mass kg 
    lift_EW = row[15]                           # [15] equipment mass kg

    lift_AirDensity = row[16]                   # [16] air density kg/m^3
    lift_DAFLateral = row[17]                   # [17] DAF lateral 
    lift_DAFVertical = row[18]                  # [18] DAF vertical
    lift_DAFHorizontal = row[19]                # [19] DAF horizontal 
    lift_AngleLateral = row[20]                 # [20] lift angle lateral 
    lift_AngleHorizontal = row[21]              # [21] lift angle horizontal 

    lift_WS = row[22]                           # [22] wind speed m/s
    lift_WindDirection = row[23]                # [23] wind direction
    lift_Alift = row[24]                        # [24] lift area m^2 
    lift_Calift = row[25]                       # [25] equipment shape coefficient, for wind
    lift_Cs = row[26]                           # [26] beam shape coefficient, for wind

    lift_AccelerationLateral = row[27]          # [27] intertia lateral
    lift_AccelerationHorizontal = row[28]       # [28] inertia horizontal
    lift_AccelerationVertical = row[29]         # [29] inertia vertical

    equipment_height = row[30]                  # [30] equipment height
    equipment_width = row[31]                   # [31] equipment width
    equipment_length = row[32]                  # [32] equipment length

    lift_ClampJawSize = row[33]                 # [33] clamp jaw size mm 
    lift_ClampEcentricity = row[34]             # [34] clamp eccentricity mm
    lift_ClampLoadSplit = row[35]               # [35] clamp load split %

    lift_BeamDeadUDL = row[36]                  # [36] beam dead load UDL kN/m^2
    lift_BeamLiveUDL = row[37]                  # [37] beam live load UDL kN/m^2
    lift_BeamSpacing = row[38]                  # [38] beam spacing m

    beam_build_type = row[39]                   # [39] beam type
    shear_stress_type = row[40]                 # [40] beam shear AISC

    beam_Cmy = row[41]                          # [41] beam moment modifier y-axis                              #### Salvador, I have added
    beam_Cmz = row[42]                          # [42] beam moment modifier z-axis                              #### Salvador, I have added

    beam_Ky = row[43]                           # [43] beam effective length factor y-axis                      #### Salvador, I have added
    beam_Kz = row[44]                           # [44] beam effective length factor z-axis                      #### Salvador, I have added

print('')
print("Lift parameters set")
print('')
print("Section: {:}, Fy: {:}, E: {:}, Fu: {:}, Density: {:}, v {:}"
      .format(beam_SectionSize, section_Fy, section_E, section_Fu, section_Density, section_v))
print('')
print("Clamp: {:}, Hoist: {:}, Equipment: {:}".format(lift_CW, lift_HW, lift_EW))
print('')
print("DAF Lateral: {:}, DAF Vertical: {:}".format(lift_DAFLateral, lift_DAFVertical))
print('')
print("Lateral accel: {:}, Horiz accel: {:}, Vert accel: {:}"
      .format(lift_AccelerationLateral, lift_AccelerationHorizontal, lift_AccelerationVertical))
print('')

#
# read databse section properties
cursor.execute(	" SELECT "
                "  B "	                # [0] flange width mm 
                ", Tf "	                # [1] flange thickness mm
                ", Dw "	                # [2] web depth mm
                ", Tw "	                # [3] web thickness mm
                ", r "	                # [4] root radius mm
                " FROM tblProjectBeamLiftInputBeam"
                " WHERE LiftID = ?", intLiftID)

print("Beam data read from database")

#
# fetch all of the rows from the query
beamdata = cursor.fetchall ()

#
# set beam properties
for row in beamdata :
    beam_bf = row[0]	                 # [0] flange width mm 
    beam_tf = row[1]	                 # [1] flange thickness mm
    beam_d = row[2]	                 # [2] web depth mm
    beam_tw = row[3]	                 # [3] web thickness mm
    beam_r = row[4]	                 # [4] root radius mm

print('')
print(beam_SectionSize, beam_bf, beam_tf, beam_d, beam_tw, beam_r)

#
# cross Haul
lifting.cross_hauling_angle.lateral = lift_AngleLateral * units.deg             # Angle lateral - from database
lifting.cross_hauling_angle.horizontal = lift_AngleHorizontal * units.deg       # Angle horizontal - from database

#
# DAF factors
factors = lifting.factors
factors.DAF.vertical = lift_DAFVertical                                         # DAF vertical - from database
factors.DAF.lateral = lift_DAFLateral                                           # DAF lateral - from database
factors.DAF.horizontal = lift_DAFHorizontal                                     # DAF horizontal - from database

# set wind speed
wind = lifting.wind
wind.velocity = lift_WS * units.m / units.s                                     # Wind speed in m/s- from database
wind.direction = lift_WindDirection * units.deg                                 # Wind direction in deg - from database

# Vessel 
vessel = lifting.vessel
vessel.acceleration.vertical = lift_AccelerationVertical * units.gravity             # Vertical acceleartion in g - from database
vessel.acceleration.horizontal = lift_AccelerationHorizontal * units.gravity         # Horizontal acceleartion in g - from database
vessel.acceleration.lateral = lift_AccelerationLateral * units.gravity               # Lateral acceleartion in g - from database

#
# add beam section
beam = lifting.beam

# Current beam UR
if beam_ExistingIR is None:
    beam.UR = 0
else:
    beam.UR = beam_ExistingIR
                                       
beam.span_between_supports = section_L * units.m                    # Beam span length - from database

#
# add material
material = beam.material
material.Fy = section_Fy * units.MPa                                # Yield strength - from database
material.Fu = section_Fu * units.MPa                                # Ultimate strength from - from database
material.E = section_E * units.MPa                                  # Young's modulus from database
material.G = 79_300 * units.MPa                                     # Hard-coded
material.density = section_Density * units.kg / units.m**3          # Density - from database
#
# set section
Ibeam = beam.Isection
Ibeam.d = beam_d * units.mm                                         # Web depth in mm - from database
Ibeam.tw = beam_tw * units.mm                                       # Web thickness in mm - from database
Ibeam.bf = beam_bf * units.mm                                       # Flange width in mm - from database
Ibeam.tf = beam_tf * units.mm                                       # Flange thickness in mm - from database
Ibeam.r = beam_r * units.mm                                         # Root radius in mm - from database
#
# !!! Level 3 !!!
Ibeam.build = beam_build_type                                       # welded (default)
Ibeam.shear_stress_type = shear_stress_type                         # average (default)

#
# add weight of clamp 
clamp = lifting.clamp
clamp.jaw_size = lift_ClampJawSize * units.mm                       # Clamp jaw size in mm - from database
clamp.weight = lift_CW * units.kg                                   # Clamp weight in kg - from database
clamp.eccentricity_to_flangle = lift_ClampEcentricity * units.mm    # Clamp eccentricity in mm - from database
clamp.load_split = lift_ClampLoadSplit / 100                        # Clamp eccentricity in % convert to decimal - from database
clamp.distance_to_beam_end_1 = load_location * units.m              # Load location 

#
# add weight of lifting equipment
rigging = lifting.rigging
rigging.weight = lift_HW * units.kg                                 # Hoist weight in kg - from database

#
# add weight of item to be lifted
door = lifting.item
door.weight = lift_EW * units.kg                                    # Equipment weight in kg - from database

#
# beam UDL (Note the UDL will be in addition to the UDL from UR)
beam_udl = beam.udl
if lift_BeamSpacing is None:
    lift_BeamUDL = 0 * units.kN / units.m
else:
    if lift_BeamLiveUDL is None:
        lift_BeamLiveUDL = 0
        #lift_BeamUDL = 0 * units.kN / units.m
    if lift_BeamDeadUDL is None:
        lift_BeamDeadUDL = 0
        #lift_BeamUDL = 0 * units.kN / units.m
    #else:
    lift_BeamUDL = ((lift_BeamDeadUDL  * units.kN / units.m**2 
                     + lift_BeamLiveUDL * units.kN / units.m**2) 
                    * lift_BeamSpacing * units.m)

beam_udl.vertical = lift_BeamUDL

#
#
# !!! Level 3 !!! 
# expose beam's design parameters
beam_design = beam.design
beam_design.laterally_unbraced_length = section_L * units.m             # Lb
beam_design.moment_modifier = beam_Cmz                                  # Cb
beam_design.Ky = beam_Ky                                                # Effective length factor for flexural buckling about y-axis
beam_design.Kz = beam_Kz                                                # Effective length factor for flexural buckling about z-axis 
#
#
##########################################
# perform level 2 check
##########################################

beam.code_check()
beam.local_code_check()

##########################################
##########################################

#
print('')
print('iLift Loads')

#
# User
sql_analysis_date = datetime.datetime.now()

#
wind_load = lifting.load.wind_load.file_out                                 # Wind loads

#
# beam udl load
beam_load = lifting.load.file_out['beam']
sql_wind_load_beam = beam_load['wind_pressure'].convert('kilonewton/metre').value 
sql_sw_load_beam = beam_load['selfweight'].convert('kilonewton/metre').value

#
# lift load
load_lateral = lifting.load.file_out['lift_load']['lateral']                # Lateral loads
load_horizontal = lifting.load.file_out['lift_load']['horizontal']          # Horizontal loads
load_vertical = lifting.load.file_out['lift_load']['vertical']              # Vertical loads

# Crane loads
_crane_load = beam.cranes[1].load                                           # Beam local loads
_flange_bottom_load = beam.flange[1].actions.bottom                         # Flange local loads
_web_bottom_load = beam.web[1].actions.bottom                               # Bottom web local loads
_web_top_load = beam.web[1].actions.top                                     # Top web local loads

#
# Adopted and effective lengths
_flange_bottom = beam.flange[1].properties.bottom
_web_bottom = beam.web[1].properties.bottom
_web_top = beam.web[1].properties.top

#
# Beam lift loads - data update
cursor.execute("UPDATE tblProjectBeamLiftOutputLoad SET "
               " LFlangeAdopted_mm = ? "
               ", LBottomWebAdopted_mm = ? "
               ", LTopWebAdopted_mm = ? " 

               ", LFlangeEff_mm = ? "
               ", LBottomWebEff_mm = ? "
               ", LTopWebEff_mm = ? " 

               ", WindLoadObjectLateral_kN = ? "
               ", WindLoadObjectHorizontal_kN = ? "
               ", WindLoadObjectVertical_kN = ? "
               ", WindLoadBeamUDL_kNm = ? "
               ", BeamSelfWeightLoadUDL_kNm = ? "

               ", LiftLateralLoadAngle_kN = ? "
               ", LiftLateralLoadInertia_kN = ? "
               ", LiftLateralLoadWind_kN = ? "
               ", BeamLateralLoadTotal_kN = ? "

               ", LiftHorizontalLoadAngle_kN = ? "
               ", LiftHorizontalLoadInertia_kN = ? "
               ", LiftHorizontalLoadWind_kN = ? "
               ", BeamHorizontalLoadTotal_kN = ? "

               ", LiftVerticaLoadAngle_kN = ? "
               ", LiftVerticalInertiaLoad_kN = ? "
               ", LiftVerticalLoadWind_kN = ? "
               ", BeamVerticalLoadTotal_kN = ? "

               ", CranePointLoad_kN = ? "

               " WHERE LiftID = ?",
               _flange_bottom.Leff.convert('millimetre').value,
               _web_bottom.Leff.convert('millimetre').value,
               _web_top.Leff.convert('millimetre').value, 

               _flange_bottom.la.convert('millimetre').value,
               _web_bottom.la.convert('millimetre').value,
               _web_top.la.convert('millimetre').value,

               wind_load['lateral'].convert('kilonewton').value,
               wind_load['horizontal'].convert('kilonewton').value,
               wind_load['vertical'].convert('kilonewton').value,
               sql_wind_load_beam, sql_sw_load_beam,

               load_lateral.from_angle.convert('kilonewton').value,
               load_lateral.inertia.convert('kilonewton').value,
               load_lateral.from_wind.convert('kilonewton').value,
               load_lateral.total.convert('kilonewton').value,

               load_horizontal.from_angle.convert('kilonewton').value,
               load_horizontal.inertia.convert('kilonewton').value,
               load_horizontal.from_wind.convert('kilonewton').value,
               load_horizontal.total.convert('kilonewton').value,

               load_vertical.from_angle.convert('kilonewton').value,
               load_vertical.inertia.convert('kilonewton').value,
               load_vertical.from_wind.convert('kilonewton').value,
               load_vertical.total.convert('kilonewton').value,

               _flange_bottom_load.Fz.convert('kilonewton').value,

                intLiftID)

cnxn.commit()

print('')
print("Loads posted to cloud")
print('')
##
total_UR = {}
##
#
# the code check data per FE element forming the beam
_beam_elements = beam.elements
_beam_forces = {}
_beam_stress = {}
_beam_results = {}
_beam_disp_node1 = {}
_beam_disp_node2 = {}
_beam_compactness = {}

for key, _element in _beam_elements.items():
    _beam_results[key] = _element.code_check
    _beam_forces[key] = _element.actions
    _beam_stress[key] = _element.stress
    _beam_disp_node1[key] = _element.node1
    _beam_disp_node2[key] = _element.node2
    _beam_compactness[key] = _element.compactness

#
# compactness
#
for key, _results in _beam_compactness.items():
    print('beam element compactness:', key)
    # global compacness
    _results.compression
    _results.flexure
    # flange compression
    _results.flange.compression.compactness
    _results.flange.compression.bottom
    _results.flange.compression.top
    # flange flexure
    _results.flange.flexure.compactness
    _results.flange.flexure.bottom
    _results.flange.flexure.top
    # web
    _results.web.compression.compactness
    _results.web.flexure.compactness

#
# beam internal forces
for key, _results in _beam_forces.items():
    print('beam element :', key)
    #
    cursor.execute("UPDATE tblProjectBeamLiftOutputIRsBeam SET "
                    " AxialLoad_kN = ? "
                    ", ShearMajorLoad_kN = ? "
                    ", ShearMinorLoad_kN = ? "
                    ", MajorAxisBending_kNm = ? "
                    ", MinorAxisBending_kNm = ? "
                    ", TorsionLoad_kNm = ? "
                    " WHERE LiftID = ? AND BeamElement = ?",
                   
                    _results.Fx.convert('kilonewton').value,            #### beam element axial load
                    _results.Fz.convert('kilonewton').value,            #### beam major axis shaer load 
                    _results.Fy.convert('kilonewton').value,            #### beam minor axis bending moment                  
                    _results.My.convert('kilonewton * metre').value,    #### beam major axis bending moment 
                    _results.Mz.convert('kilonewton * metre').value,    #### beam minor axis bending moment 
                    _results.Mx.convert('kilonewton * metre').value,    #### beam torsion load

                   intLiftID, key)
    #
    cnxn.commit()
#
print('')
print("Beam Loads posted to cloud")
print('')

#
# beam IRs, stresses, allowables
for key, _results in _beam_results.items():
    print('beam element :', key)

##########################################
# Updated by HW 13-June-2019
##########################################
    total_UR['beam_' + str(key)] = max(_results.combined.UR, 
                           _results.shear.URz, _results.shear.URy)
##########################################
##########################################
    #
    print('WARNING :', _results.axial.warning)
    #
    cursor.execute("UPDATE tblProjectBeamLiftOutputIRsBeam SET "
                   " AllowAxial_Nmm2 = ? "
                   ", AllowBendingMajor_Nmm2 = ? "
                   ", AllowBendingMinor_Nmm2 = ? "
                   ", AllowShearMajor_Nmm2 = ? "
                   ", AllowShearMinor_Nmm2 = ? "

                   ", AxialIR = ? "
                   ", AxialIRFlag = ? "
                   ", BendingMajorIR = ? "
                   ", BendingMinorIR = ? "
                   ", BendingIRFlag = ? "
                   ", ShearMajorIR = ? "
                   ", ShearMinorIR = ? "
                   ", ShearIRFlag = ? "
                   ", CombinedIR = ? "
                   ", CombinedFlag = ? "
                   ", CombinedStatus = ? "  
                   " WHERE LiftID = ? AND BeamElement = ?",
                   
                   _results.axial.allowable.convert('megapascal').value,
                   _results.bending.allowable_y.convert('megapascal').value,
                   _results.bending.allowable_z.convert('megapascal').value,                     
                   _results.shear.allowable_z.convert('megapascal').value,
                   _results.shear.allowable_y.convert('megapascal').value,                    

                   _results.axial.UR,                                                     
                   _results.axial.UR_flag,
                   _results.bending.URy,
                   _results.bending.URz,
                   _results.bending.UR_flag,
                   _results.shear.URz,
                   _results.shear.URy,
                   _results.shear.UR_flag,
                   _results.combined.UR, 
                   _results.combined.UR_flag,
                   _results.combined.status,
                   intLiftID, key)
    #
    cnxn.commit()
#
print('')
print("Beam IRs posted to cloud")
print('')

#
# Node displacements print here
#
# 1  element  2
# o-----------o
#
# ux = axial displacement
# uy = lateral displacement
# uz = vertical displacement
# torsion = angle of rotation (radians)
#
for x, key in enumerate(_beam_disp_node1.keys()):
    print('beam element :', key)
    _node1 = _beam_disp_node1[key]
    _node2 = _beam_disp_node2[key]

    print('flange element :', key)
    cursor.execute("UPDATE tblProjectBeamLiftOutputDisplacement SET "
                   " End1AxialDisplacement_mm = ? "
                   ", End1LateralDisplacement_mm = ? "
                   ", End1VerticalDisplacement_mm = ? "
                   ", End1Torsion_Radian = ? "
                   ", End2AxialDisplacement_mm = ? "
                   ", End2LateralDisplacement_mm = ? "
                   ", End2VerticalDisplacement_mm = ? "
                   ", End2Torsion_Radian = ? "
                   " WHERE LiftID = ? AND BeamElement = ?",
                   
                   _node1.ux.convert('millimetre').value,
                   _node1.uy.convert('millimetre').value,
                   _node1.uz.convert('millimetre').value,
                   _node1.torsion.convert('radian').value,
                   _node2.ux.convert('millimetre').value,
                   _node2.uy.convert('millimetre').value,
                   _node2.uz.convert('millimetre').value,
                   _node2.torsion.convert('radian').value,
                   intLiftID, key)
    #
    cnxn.commit()

#
# flange IRs, stresses, allowables, loads 
_flanges = {}
_flanges_stress = {}
_flanges_actions = {}

for key, _item in beam.flange.items():
    _flanges[key] = _item.code_check
    _flanges_stress[key] = _item.stress
    _flanges_actions[key] = _item.actions

#
# flange applied forces
for key, _results in _flanges_actions.items():
    print('flange element :', key)
    #
    cursor.execute("UPDATE tblProjectBeamLiftOutputIRsFlange SET "
                    " AxialLoad_kN = ? "
                    ", ShearMajorLoad_kN = ? "
                    ", ShearMinorLoad_kN = ? "
                    ", MajorAxisBending_kNm = ? "
                    ", MinorAxisBending_kNm = ? "
                    ", TorsionLoad_kNm = ? "
                    " WHERE LiftID = ? AND FlangeComponent = ?",

                    _results.bottom.Fx.convert('kilonewton').value, 
                    _results.bottom.Fy.convert('kilonewton').value, 
                    _results.bottom.Fz.convert('kilonewton').value,                   
                    _results.bottom.Mx.convert('kilonewton * metre').value,  
                    _results.bottom.My.convert('kilonewton * metre').value,   
                    _results.bottom.Mz.convert('kilonewton * metre').value,    

                   intLiftID, "Bottom Flange")
    #
    cnxn.commit()

    #
print('')
print("Flange internal loads posted to cloud")
print('')

#
# flange stresses
for key, _results in _flanges.items():
    print('flange element :', key)
    total_UR['flange_bottom'] = max(_results.bottom.combined.UR, 
                                    _results.bottom.shear.URz, _results.bottom.shear.URy)
    #
    cursor.execute("UPDATE tblProjectBeamLiftOutputIRsFlange SET "
                   " AllowAxial_Nmm2 = ? "
                   ", AllowBendingMajor_Nmm2 = ? "
                   ", AllowBendingMinor_Nmm2 = ? "
                   ", AllowShearMajor_Nmm2 = ? "
                   ", AllowShearMinor_Nmm2 = ? "

                   ", AxialIR = ? "
                   ", AxialIRFlag = ? "
                   ", BendingMajorIR = ? "
                   ", BendingMinorIR = ? "
                   ", BendingIRFlag = ? "
                   ", ShearMajorIR = ? "
                   ", ShearMinorIR = ? "
                   ", ShearIRFlag = ? "
                   ", CombinedIR = ? "
                   ", CombinedFlag = ? "
                   ", CombinedStatus = ? "  
                   " WHERE LiftID = ? AND FlangeComponent = ?",

                   _results.bottom.axial.allowable.convert('megapascal').value,
                   _results.bottom.bending.allowable_y.convert('megapascal').value,
                   _results.bottom.bending.allowable_z.convert('megapascal').value,                       
                   _results.bottom.shear.allowable_y.convert('megapascal').value,
                   _results.bottom.shear.allowable_z.convert('megapascal').value,                     

                   _results.bottom.axial.UR,
                   _results.bottom.axial.UR_flag,
                   _results.bottom.bending.URy,
                   _results.bottom.bending.URz,
                   _results.bottom.bending.UR_flag,
                   _results.bottom.shear.URz,
                   _results.bottom.shear.URy,
                   _results.bottom.shear.UR_flag,
                   _results.bottom.combined.UR,
                   _results.bottom.combined.UR_flag,
                   _results.bottom.combined.status,
                   intLiftID, "Bottom Flange")
    #
    cnxn.commit()

    #
print('')
print("Flange IR posted to cloud")
print('')

#
# bottom web IRs, stresses, allowables
_web = {}
_web_stress = {}
_web_actions = {}

#
# loop webs
for key, _item in beam.web.items():
    _web[key] = _item.code_check
    _web_stress[key] = _item.stress
    _web_actions[key] = _item.actions

########
########
# Web's applied forces
# for key, _results in _web_actions.items():
#     print('flange bottom element :', key)
#     _results.bottom.Fx.convert('kilonewton').value,            #### Salvador change to flange component axial load
#     _results.bottom.Fy.convert('kilonewton').value,        #### Salvador change to flange component major axis bending moment
#     _results.bottom.Fz.convert('kilonewton').value,        #### Salvador change to flange component minor axis bending moment                  
#     _results.bottom.Mx.convert('kilonewton * metre').value,          #### Salvador change to flange component torsion load
#     _results.bottom.My.convert('kilonewton * metre').value,          #### Salvador change to flange component major axis shaer load 
#     _results.bottom.Mz.convert('kilonewton * metre').value,          #### Salvador change to flange component minor axis shaer load
    #
#     print('flange top element :', key)
#     _results.top.Fx.convert('kilonewton').value,            #### Salvador change to flange component axial load
#     _results.top.Fy.convert('kilonewton').value,        #### Salvador change to flange component major axis bending moment
#     _results.top.Fz.convert('kilonewton').value,        #### Salvador change to flange component minor axis bending moment                  
#     _results.top.Mx.convert('kilonewton * metre').value,          #### Salvador change to flange component torsion load
#     _results.top.My.convert('kilonewton * metre').value,          #### Salvador change to flange component major axis shaer load 
#     _results.top.Mz.convert('kilonewton * metre').value,          #### Salvador change to flange component minor axis shaer load     


########
########
# Web's applied forces    
for key, _results in _web_actions.items():
    print('web bottom element :', key)
    #
    cursor.execute("UPDATE tblProjectBeamLiftOutputIRsWeb SET "
                   " AxialLoad_kN = ? "
                   ", ShearMajorLoad_kN = ? "
                   ", ShearMinorLoad_kN = ? "                   
                   ", MajorAxisBending_kNm = ? "
                   ", MinorAxisBending_kNm = ? "
                   ", TorsionLoad_kNm = ? "
                   " WHERE LiftID = ? AND WebComponent = ?",

                   _results.bottom.Fx.convert('kilonewton').value,          #### web bottom axial load
                   _results.bottom.Fy.convert('kilonewton').value,          #### web bottom major axis shaer load 
                   _results.bottom.Fz.convert('kilonewton').value,          #### web bottom minor axis shaer load                  
                   _results.bottom.My.convert('kilonewton * metre').value,  #### web bottom major axis bending moment 
                   _results.bottom.Mz.convert('kilonewton * metre').value,  #### web bottom minor axis bending moment 
                   _results.bottom.Mx.convert('kilonewton * metre').value,  #### web bottom torsion load 

                   intLiftID, "Web Bottom")
    #
    cnxn.commit()

    #
    print('web top element :', key)
    #
    cursor.execute("UPDATE tblProjectBeamLiftOutputIRsWeb SET "
                   " AxialLoad_kN = ? "
                   ", MajorAxisBending_kNm = ? "
                   ", MinorAxisBending_kNm = ? "
                   ", TorsionLoad_kNm = ? "
                   ", ShearMajorLoad_kN = ? "
                   ", ShearMinorLoad_kN = ? "
                   
                   " WHERE LiftID = ? AND WebComponent = ?",

                   _results.top.Fx.convert('kilonewton').value,             #### web top axial load
                   _results.top.Fy.convert('kilonewton').value,             #### web top major axis shaer load 
                   _results.top.Fz.convert('kilonewton').value,             #### web top minor axis shaer load                   
                   _results.top.My.convert('kilonewton * metre').value,     #### web top major axis bending moment 
                   _results.top.Mz.convert('kilonewton * metre').value,     #### web top minor axis bending moment  
                   _results.top.Mx.convert('kilonewton * metre').value,     #### web top torsion load 

                   intLiftID, "Web Top")
    #
    cnxn.commit()

#
print('')
print("Bottom/top web internal loads posted to cloud")
print('')

########
########
# Web's stresses, allows and IRs   
for key, _results in _web.items(): 
    print('bottom web beam element :', key)
    total_UR['web_bottom'] = max(_results.bottom.combined.UR, 
                                 _results.bottom.shear.URz, _results.bottom.shear.URy)
    #
    cursor.execute("UPDATE tblProjectBeamLiftOutputIRsWeb SET "
                   " AllowAxial_Nmm2 = ? "
                   ", AllowBendingMajor_Nmm2 = ? "
                   ", AllowBendingMinor_Nmm2 = ? "
                   ", AllowShearMajor_Nmm2 = ? "
                   ", AllowShearMinor_Nmm2 = ? "

                   ", AxialIR = ? "
                   ", AxialIRFlag = ? "
                   ", BendingMajorIR = ? "
                   ", BendingMinorIR = ? "
                   ", BendingIRFlag = ? "
                   ", ShearMajorIR = ? "
                   ", ShearMinorIR = ? "
                   ", ShearIRFlag = ? "
                   ", CombinedIR = ? "
                   ", CombinedFlag = ? "
                   ", CombinedStatus = ? "  
                   " WHERE LiftID = ? AND WebComponent = ?",

                   _results.bottom.axial.allowable.convert('megapascal').value,
                   _results.bottom.bending.allowable_y.convert('megapascal').value,
                   _results.bottom.bending.allowable_z.convert('megapascal').value,                      
                   _results.bottom.shear.allowable_y.convert('megapascal').value,
                   _results.bottom.shear.allowable_z.convert('megapascal').value,                     

                   _results.bottom.axial.UR,
                   _results.bottom.axial.UR_flag,
                   _results.bottom.bending.URy,
                   _results.bottom.bending.URz,
                   _results.bottom.bending.UR_flag,
                   _results.bottom.shear.URz,
                   _results.bottom.shear.URy,
                   _results.bottom.shear.UR_flag,
                   _results.bottom.combined.UR,
                   _results.bottom.combined.UR_flag,
                   _results.bottom.combined.status,
                   intLiftID, "Web Bottom")
    #
    cnxn.commit()
    #
    print('top web beam element :', key)
    total_UR['web_top'] = max(_results.top.combined.UR, 
                              _results.top.shear.URz, _results.top.shear.URy)
    #
    cursor.execute("UPDATE tblProjectBeamLiftOutputIRsWeb SET "
                   " AllowAxial_Nmm2 = ? "
                   ", AllowBendingMajor_Nmm2 = ? "
                   ", AllowBendingMinor_Nmm2 = ? "
                   ", AllowShearMajor_Nmm2 = ? "
                   ", AllowShearMinor_Nmm2 = ? "

                   ", AxialIR = ? "
                   ", AxialIRFlag = ? "
                   ", BendingMajorIR = ? "
                   ", BendingMinorIR = ? "
                   ", BendingIRFlag = ? "
                   ", ShearMajorIR = ? "
                   ", ShearMinorIR = ? "
                   ", ShearIRFlag = ? "
                   ", CombinedIR = ? "
                   ", CombinedFlag = ? "
                   ", CombinedStatus = ? "  
                   " WHERE LiftID = ? AND WebComponent = ?",

                   _results.top.axial.allowable.convert('megapascal').value,
                   _results.top.bending.allowable_y.convert('megapascal').value,
                   _results.top.bending.allowable_z.convert('megapascal').value,                    
                   _results.top.shear.allowable_y.convert('megapascal').value,
                   _results.top.shear.allowable_z.convert('megapascal').value,                    

                   _results.top.axial.UR,
                   _results.top.axial.UR_flag,
                   _results.top.bending.URy,
                   _results.top.bending.URz,
                   _results.top.bending.UR_flag,
                   _results.top.shear.URz,
                   _results.top.shear.URy,
                   _results.top.shear.UR_flag,
                   _results.top.combined.UR,
                   _results.top.combined.UR_flag,
                   _results.top.combined.status,
                   intLiftID, "Web Top")
    #
    cnxn.commit()

#
print('')
print("Bottom/top web IRs posted to cloud")
print('')

#
# section properties
sql_beam_area = Ibeam.area.convert('millimetre^2')
sql_beam_Iy = Ibeam.Iy.convert('millimetre^4')
sql_beam_Zy = Ibeam.Zey.convert('millimetre^3')

#
# section stress are defined as below:
#
#   0    1     2
#   +----+-----+
#   |____+_____| 3    ^ z
#   	 |            |
#   	 + 4          +--> y
#   	 |
#    ____+_____  5
#   |          |
#   +----+-----+      
#   6    7     8
#
# Stresses are defined for 4 FE elements in 
# _beam_stress created previosly
#
# _beam_stress[1] : sigma_x (in 9 locations)
#                   sigma_y (in 9 locations)
#                   sigma_z (in 9 locations)
#                   tau_x (in 9 locations)
#                   tau_y (in 9 locations)
#                   tau_z (in 9 locations)
#                   von_mises (in 9 locations)

# loop elements
for key, _stress in _beam_stress.items():
    print('element :', key)
    # calculate von mises
    _von_mises = _stress.von_mises()
    # loop 9 stress points in the element
    for x, _mises in enumerate(_von_mises):
        # Beam lift stresses - data update
        cursor.execute("UPDATE tblProjectBeamLiftOutputStressBeam SET "
                       "SigmaX_Nmm2 = ? "
                       ", SigmaY_Nmm2 = ? "
                       ", SigmaZ_Nmm2 = ? "
                       ", SigmaTauX_Nmm2 = ? "
                       ", SigmaTauY_Nmm2 = ? "
                       ", SigmaTauZ_Nmm2 = ? "
                       ", SigmaVonMises_Nmm2 = ? "
                       " WHERE LiftID = ? AND BeamElement = ? AND StressLocation = ?",
                       _stress.sigma_x[x].convert('megapascal').value, 
                       _stress.sigma_y[x].convert('megapascal').value, 
                       _stress.sigma_z[x].convert('megapascal').value,
                       _stress.tau_x[x].convert('megapascal').value, 
                       _stress.tau_y[x].convert('megapascal').value, 
                       _stress.tau_z[x].convert('megapascal').value,
                       _mises.convert('megapascal').value,
                       intLiftID, key, x + 1)

        cnxn.commit()
    print('element axial:', key, _stress.sigma_x[x].convert('megapascal').value)
#
print('')
print("Beam stresses posted to cloud")
print('')

#
# plate stress are defined as below:
#
#   0    1     2
#   +----------+
#   |    |     |
#   |	 |     |
# 3 +----+-----+ 5
#   |	 |     |
#   |    |     |
#   +----------+  
#   6    7     8
#

# flange stresses
# loop elements (bottom flange or top flange)
for key, _stress in _flanges_stress.items():
    print('flange bottom :', key)
    # calculate von mises
    _von_mises = _stress.bottom.von_mises()
    # loop 9 stress points in the element
    for x, _mises in enumerate(_von_mises):
        # Beam lift stresses - data update
        cursor.execute("UPDATE tblProjectBeamLiftOutputStressFlange SET "
                       "SigmaX_Nmm2 = ? "
                       ", SigmaY_Nmm2 = ? "
                       ", SigmaZ_Nmm2 = ? "
                       ", SigmaTauX_Nmm2 = ? "
                       ", SigmaTauY_Nmm2 = ? "
                       ", SigmaTauZ_Nmm2 = ? "
                       ", SigmaVonMises_Nmm2 = ? "
                       " WHERE LiftID = ? AND FlangeComponent = ? AND StressLocation = ?",
                       _stress.bottom.sigma_x[x].convert('megapascal').value, 
                       _stress.bottom.sigma_y[x].convert('megapascal').value, 
                       _stress.bottom.sigma_z[x].convert('megapascal').value,
                       _stress.bottom.tau_x[x].convert('megapascal').value, 
                       _stress.bottom.tau_y[x].convert('megapascal').value, 
                       _stress.bottom.tau_z[x].convert('megapascal').value,
                       _mises.convert('megapascal').value,
                       intLiftID, "Bottom Flange", x + 1)

        cnxn.commit()
    print('element axial:', key, _stress.bottom.sigma_x[x].convert('megapascal').value)
#
print('')
print("Bottom flange stresses posted to cloud")
print('')

# web stresses
# loop elements (web bottom, web middle or web top)
for key, _stress in _web_stress.items():
    print('web bottom :', key)
    # calculate von mises
    _von_mises = _stress.bottom.von_mises()
    # loop 9 stress points in the element
    for x, _mises in enumerate(_von_mises):
        # Beam lift stresses - data update
        cursor.execute("UPDATE tblProjectBeamLiftOutputStressWeb SET "
                       "SigmaX_Nmm2 = ? "
                       ", SigmaY_Nmm2 = ? "
                       ", SigmaZ_Nmm2 = ? "
                       ", SigmaTauX_Nmm2 = ? "
                       ", SigmaTauY_Nmm2 = ? "
                       ", SigmaTauZ_Nmm2 = ? "
                       ", SigmaVonMises_Nmm2 = ? "
                       " WHERE LiftID = ? AND WebComponent = ? AND StressLocation = ?",
                       _stress.bottom.sigma_x[x].convert('megapascal').value, 
                       _stress.bottom.sigma_y[x].convert('megapascal').value, 
                       _stress.bottom.sigma_z[x].convert('megapascal').value,
                       _stress.bottom.tau_x[x].convert('megapascal').value, 
                       _stress.bottom.tau_y[x].convert('megapascal').value, 
                       _stress.bottom.tau_z[x].convert('megapascal').value,
                       _mises.convert('megapascal').value,
                       intLiftID, "Web Bottom", x + 1)
        cnxn.commit()
    print('web bottom:', key, _stress.bottom.sigma_y[x].convert('megapascal').value)
    #
    #
    print('web top:', key)
    # calculate von mises
    _von_mises = _stress.top.von_mises()
    # loop 9 stress points in the element
    for x, _mises in enumerate(_von_mises):
        # Beam lift stresses - data update
        cursor.execute("UPDATE tblProjectBeamLiftOutputStressWeb SET "
                       "SigmaX_Nmm2 = ? "
                       ", SigmaY_Nmm2 = ? "
                       ", SigmaZ_Nmm2 = ? "
                       ", SigmaTauX_Nmm2 = ? "
                       ", SigmaTauY_Nmm2 = ? "
                       ", SigmaTauZ_Nmm2 = ? "
                       ", SigmaVonMises_Nmm2 = ? "
                       " WHERE LiftID = ? AND WebComponent = ? AND StressLocation = ?",
                       _stress.top.sigma_x[x].convert('megapascal').value, 
                       _stress.top.sigma_y[x].convert('megapascal').value, 
                       _stress.top.sigma_z[x].convert('megapascal').value,
                       _stress.top.tau_x[x].convert('megapascal').value, 
                       _stress.top.tau_y[x].convert('megapascal').value, 
                       _stress.top.tau_z[x].convert('megapascal').value,
                       _mises.convert('megapascal').value,
                       intLiftID, "Web Top", x + 1)
        cnxn.commit()
    print('web top:', key, _stress.top.sigma_y[x].convert('megapascal').value)    
#

#
print('')
print("Web bottom and top stresses posted to cloud")

#
final_UR = max([_item for _item in total_UR.values()])
print('Total UR = {:1.3f}'.format(final_UR))

##########################################
# Updated by HW 11-June-2019
##########################################
# level 2 assessment status
strLevel2Status = "Pending level 2"
strSafeLiftOK = "Level2Pending"

if final_UR is None:
    strLevel2Status = "Pending level 2"
    strSafeLiftOK = "Level2Pending"
else:
    if final_UR <= 1.0:
        strLevel2Status = "Passed level 2"
        strSafeLiftOK = "Level2Passed"
    else:
        strLevel2Status = "Failed level 2"
        strSafeLiftOK = "Level2Failed"
##########################################
##########################################

# iLift status change to complete
cursor.execute("UPDATE tblProjectBeamLiftInput SET "
               " Level2MaxIR = ? "
               ", AssessmentStatus = ? "
               ", SafeLiftOK = ? "
               " WHERE LiftID = ? ",
                final_UR,
                strLevel2Status,
                strSafeLiftOK,
                intLiftID)

cnxn.commit()

print('')
print("Status changed")

#
# Close database connection
cursor.close()
del cursor
cnxn.close()     #<--- Close the connectio

#
print('')
print('Finish')
