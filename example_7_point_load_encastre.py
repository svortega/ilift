#
import iLift as iLift
#
lifting = iLift.Lifting_Tool()
units = lifting.units
#
#lifting.lift_angle = 0 * units.deg
lifting.lift_angle.lateral = 0 * units.deg
lifting.lift_angle.vertical = 10 * units.deg
#
factors = lifting.factors
factors.DAF.vertical = 1.08
factors.DAF.lateral = 1.22
#
# vessel
vessel = lifting.vessel
vessel.acceleration.lateral = 0.05 * units.gravity
#
# Weight of lifting item
item = lifting.item
#item.weight = 15_000 * units.kg
item.height = 1.627  * units.m
item.length = 2.460  * units. m
item.width = 1.0 * units.m
#
wind = lifting.wind
wind.velocity = 35.65 * units.knot
wind.direction = 45 * units.degrees
#
load = lifting.load
print('Load')
print('wind lateral     = {: 1.3f} kN'
      .format(load.wind_load.lateral.convert('kilonewton').value))
print('wind horizontal  = {: 1.3f} kN'
      .format(load.wind_load.horizontal.convert('kilonewton').value))
print('lift load vertical = {: 1.3f} kN'
      .format(load.lift_load.vertical.convert('kilonewton').value))
print('lift load lateral  = {: 1.3f} kN'
      .format(load.lift_load.lateral.convert('kilonewton').value))
#
#
#
beam = lifting.beam
#beam.UR = 0.80  # current beam UR
# beam boundary condition setup
beam.span_between_supports = 4 * units.m
beam.fixed_both_ends
#
material = beam.material
material.Fy = 345 * units.MPa
material.Fu = 410 * units.MPa
material.E = 200_000 * units.MPa
material.G = 79_300 * units.MPa
material.density = 8050 * units.kg / units.m**3
#
# set section W18x55
Ibeam = beam.Isection
Ibeam.d = 18.11 * units.inch
Ibeam.tw = 0.39 * units.inch
Ibeam.bf = 7.53 * units.inch
Ibeam.tf = 0.63 * units.inch
Ibeam.r = 1.67 * units.inch
#
# expose beam's design parameters
beam_design = beam.design
beam_design.laterally_unbraced_length = 4 * units.m
beam_design.moment_modifier = 1.355
#
#
# input beam actions directly (i.e. instead crane)
beam_load = beam.point_load
beam_load.Fx = -15.0 * units.kN
beam_load.Fy = 20 * units.kN
beam_load.Fz = 100 * units.kN
beam_load.Mx = 15 * units.kN * units.m
beam_load.distance_to_beam_end_1 = 3.5 * units.m
#
# code check beam
beam.code_check()
print('Finish')