# iLift
iLift is a tool to assess uncertified beams for cranes type:
- Clamp
- Trolley

---
## Global Check
Global check is performed using AISC-360 2016

---
## Local Checks
Local checks are performed on:
- Bottom flange 
- Web bottom flange
- Web top flange

The following codes are used according to the crane selected:
- Clamp : AISC-360
- Trolley : EN 1993-6 April 2007

---
