#
import iLift as iLift
#
lifting = iLift.Lifting_Tool()
units = lifting.units
#
# Cross Haul
lifting.cross_hauling_angle.transverse = 15 * units.deg
lifting.cross_hauling_angle.longitudinal = 15 * units.deg
#
# DAF factors
factors = lifting.factors
factors.DAF.vertical = 1.10
factors.DAF.horizontal = 1.50
#
# vessel 
vessel = lifting.vessel
vessel.acceleration.vertical = 0.12 * units.gravity
vessel.acceleration.horizontal = 0.12 * units.gravity
#
# set wind speed
wind = lifting.wind
wind.velocity = 15 * units.m / units.s
wind.direction = 90 * units.deg
#
# Weight of lifting item
item = lifting.item
item.shape_factor = 1.6
item.height = 1.0 * units.m
item.length = 1.0 * units.m
#
# add beam section
beam = lifting.beam
beam.UR = 0.80  # current beam UR
beam.span_between_supports = 5 * units.m
# add material
material = beam.material
material.Fy = 355 * units.MPa
material.Fu = 410 * units.MPa
material.E = 200_000 * units.MPa
material.G = 79_300 * units.MPa
material.density = 7850 * units.kg / units.m**3
# set section
Ibeam = beam.Isection
Ibeam.d = 265.938 * units.mm
Ibeam.tw = 7.62 * units.mm
Ibeam.bf = 147.574 * units.mm
Ibeam.tf = 12.954 * units.mm
Ibeam.r = 10.922 * units.mm
#
# add clamp-crane
clamp = lifting.clamp
clamp.jaw_size = 100 * units.mm
clamp.weight = 10 * units.kg
clamp.eccentricity_to_flangle = 100 * units.mm
clamp.load_split = 0.50
#
# add item to be lifted
#door = lifting.item
#door.weight = 20 * units.tonne
#
# Weight of lifting equipment (assumed)
#rigging = lifting.rigging
#rigging.weight = 25.50 * units.kg
#
#print('Load')
#print('vertical = {: 1.3f} kN'.format(load.vertical.convert('kilonewton').value))
#print('lateral  = {: 1.3f} kN'.format(load.lateral.convert('kilonewton').value))
#
#beam.code_check()
#beam.local_code_check()
#
# Find safe lifting load 
lifting.safe_lifting_load(allowable_UR = 1.0,
                          actual_UR = 0.0)
#
print('Finish')