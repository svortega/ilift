#
import iLift as iLift
#
lifting = iLift.Lifting_Tool()
units = lifting.units
#
# Cross Haul
lifting.cross_hauling_angle.lateral = 15 * units.deg
lifting.cross_hauling_angle.horizontal = 0 * units.deg
#
# DAF factors
factors = lifting.factors
factors.DAF.vertical = 1.10
#
# add beam section
beam = lifting.beam
beam.UR = 0.0  # current beam UR
# beam boundary condition setup
beam.span_between_supports = 2.50 * units.m
# add material
material = beam.material
material.Fy = 275 * units.MPa
material.Fu = 410 * units.MPa
material.E = 200_000 * units.MPa
material.G = 79_300 * units.MPa
material.density = 8050 * units.kg / units.m**3
# set section
Ibeam = beam.Isection
Ibeam.d = 312.7 * units.mm
Ibeam.tw = 6.6 * units.mm
Ibeam.bf = 102.4 * units.mm
Ibeam.tf = 10.8 * units.mm
Ibeam.r = 7.6 * units.mm
# add beam distributed load
beam_udl = beam.udl
_live = 5 * units.kN / units.m**2
_dead = 0.6 * units.kN / units.m**2 
_width_load = 875 * units.mm
qb = (_live + _dead) * _width_load
beam_udl.vertical = qb 
#
#
# add clamp-crane
clamp = lifting.clamp
clamp.jaw_size = 70 * units.mm
clamp.weight = 10 * units.kg
clamp.eccentricity_to_flangle = 100 * units.mm
clamp.load_split = 0.60
#
# add item to be lifted
door = lifting.item
door.weight = 3.0 * units.kN / units.gravity
#
# Weight of lifting equipment (assumed)
rigging = lifting.rigging
rigging.weight = 25.50 * units.kg
#
# print load summary
load = lifting.load
print('Load')
print('vertical = {: 1.3f} kN'
      .format(load.lift_load.vertical.convert('kilonewton').value))
print('lateral  = {: 1.3f} kN'
      .format(load.lift_load.lateral.convert('kilonewton').value))
#
beam.code_check()
beam.local_code_check()

#
#
#
_beam_results = {}
for key, _element in beam.elements.items():
    _beam_results[key] = _element.code_check
#
# loop flanges
# loop elements
for key, _results in _beam_results.items():
    # print (key, "=>", _results)
    # beam
    print('beam element :', key)
    # axial
    # allowable
    sql_beam_axial_allow = _results.axial.allowable.convert('megapascal').value
    #
    # bending
    # allowable
    sql_beam_flexural_strength_major = _results.bending.allowable_y.convert('megapascal').value
    sql_beam_flexural_strength_minor = _results.bending.allowable_z.convert('megapascal').value
    #
    # shear
    # allowable
    sql_beam_shear_strength_major = _results.shear.allowable_y.convert('megapascal').value
    sql_beam_shear_strength_minor = _results.shear.allowable_z.convert('megapascal').value
    #
#
#    
print('Finish')
#
# section properties
# sql_beam_area = Ibeam.area.convert('millimetre^2')
# sql_beam_Iy = Ibeam.Iy.convert('millimetre^4')
# sql_beam_Zy = Ibeam.Zey.convert('millimetre^3')