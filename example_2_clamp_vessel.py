#
import iLift as iLift
#
lifting = iLift.Lifting_Tool()
units = lifting.units
#
lifting.cross_hauling_angle.transverse = 15 * units.deg
lifting.cross_hauling_angle.longitudinal = 0 * units.deg
#
factors = lifting.factors
factors.DAF.vertical = 1.10
factors.DAF.horizontal = 1.05
#
# Vessel 
vessel = lifting.vessel
vessel.acceleration.vertical = 0.15 * units.gravity
vessel.acceleration.horizontal = 0.20 * units.gravity
#
# set wind speed
wind = lifting.wind
wind.velocity = 15 * units.m / units.s
wind.direction = 45 * units.deg
#
# Weight of lifting equipment (assumed)
rigging = lifting.rigging
rigging.weight = 25.50 * units.kg
#
#
# add beam section
beam = lifting.beam
beam.UR = 0.80  # current beam UR
beam.span_between_supports = 2.0 * units.m
#beam.shape_factor = 0
#
# add material
material = beam.material
material.Fy = 250 * units.MPa
material.Fu = 410 * units.MPa
material.E = 200_000 * units.MPa
material.G = 79_300 * units.MPa
material.density = 8_050 * units.kg / units.m**3
#
# set section
Ibeam = beam.Isection
Ibeam.d = 312.7 * units.mm
Ibeam.tw = 6.6 * units.mm
Ibeam.bf = 102.4 * units.mm
Ibeam.tf = 10.8 * units.mm
Ibeam.r = 7.6 * units.mm
Ibeam.build = 'rolled'  # welded
Ibeam.shear_stress_type = 'maximum'  # average
#
# add clamp-crane
clamp = lifting.clamp
clamp.jaw_size = 70 * units.mm
clamp.weight = 10 * units.kg
clamp.eccentricity_to_flangle = 100 * units.mm
clamp.load_split = 0.50
#
#
door = lifting.item
door.weight = 2.44 * units.kN / units.gravity
door.height = 1.5 * units.m
door.width = 1.5 * units.m
door.shape_factor = 1.10
#
#
#
# Find safe lifting load 
lifting.safe_lifting_load(allowable_UR = 1.0,
                          actual_UR = 0.80)
#
#
#
load = lifting.load
print('Load')
print('wind lateral     = {: 1.3f} kN'
      .format(load.wind_load.lateral.convert('kilonewton').value))
print('wind horizontal  = {: 1.3f} kN'
      .format(load.wind_load.horizontal.convert('kilonewton').value))
print('lift load vertical = {: 1.3f} kN'
      .format(load.lift_load.vertical.convert('kilonewton').value))
print('lift load lateral  = {: 1.3f} kN'
      .format(load.lift_load.lateral.convert('kilonewton').value))
#
beam.code_check()
beam.local_code_check()
#
print('Finish')
#
_flanges = {}
_flanges_stress = {}

for key, _item in beam.flange.items():
    _flanges[key] = _item.code_check
    _flanges_stress[key] = _item.stress
#
#
print('')
# loop flanges
for key, _results in _flanges.items():
    print('flange element :', key)
    print(_results.bottom.axial.allowable.convert('megapascal').value)
    print(_results.bottom.bending.allowable_y.convert('megapascal').value)
    print(_results.bottom.bending.allowable_z.convert('megapascal').value)
    print(_results.bottom.shear.allowable_y.convert('megapascal').value)
    print(_results.bottom.shear.allowable_z.convert('megapascal').value)
    print(_results.bottom.axial.UR)
    print(_results.bottom.axial.UR_flag)
    print(_results.bottom.bending.URy)
    print(_results.bottom.bending.URz)
    print(_results.bottom.bending.UR_flag)
    print(_results.bottom.shear.URz)
    print(_results.bottom.shear.URy)
    print(_results.bottom.shear.UR_flag)
    print(_results.bottom.combined.UR)
    print(_results.bottom.combined.UR_flag)
    print(_results.bottom.combined.status)
#
#
# bottom web IRs, stresses, allowables
_web = {}
_web_stress = {}
# loop webs
for key, _item in beam.web.items():
    _web[key] = _item.code_check
    _web_stress[key] = _item.stress
#
print('')
#
for key, _results in _web.items(): 
    print('top web beam element :', key)
    print(_results.top.axial.allowable.convert('megapascal').value)
    print(_results.top.bending.allowable_y.convert('megapascal').value)
    print(_results.top.bending.allowable_z.convert('megapascal').value)
    print(_results.top.shear.allowable_y.convert('megapascal').value)
    print(_results.top.shear.allowable_z.convert('megapascal').value)
    print(_results.top.axial.UR)
    print(_results.top.axial.UR_flag)
    print(_results.top.bending.URy)
    print(_results.top.bending.URz)
    print(_results.top.bending.UR_flag)
    print(_results.top.shear.URz)
    print(_results.top.shear.URy)
    print(_results.top.shear.UR_flag)
    print(_results.top.combined.UR)
    print(_results.top.combined.UR_flag)
    print(_results.top.combined.status)
#
print('Finish')