#
import iLift as iLift
#
lift_check = iLift.Lifting_Tool()
units = lift_check.units

#
lift_check.cross_hauling_angle.transverse = 0 * units.deg
lift_check.cross_hauling_angle.longitudinal = 0 * units.deg
#
factors = lift_check.factors
factors.DAF.vertical = 1.25
factors.DAF.horizontal = 1.10
#
# vessel
vessel = lift_check.vessel
vessel.acceleration.vertical = 0.025 * units.gravity
#
# Weight of lifting item
item = lift_check.item
item.weight = 1_200 * units.kg
#item.height = 1.0 * units.m
#item.length = 1.0 * units.m
#
#
trolley = lift_check.trolley
trolley.Xw = 200 * units.mm
trolley.n =   10 * units.mm
#trolley.case = 'b' # see table 6.2 EN1993.6
trolley.weight = 250 * units.kg
#
#wind = lift_check.wind
#wind.velocity = 18.34 * units.m/units.s
#wind.direction = 90 * units.degrees
#
# Weight of lifting equipment (assumed)
rigging = lift_check.rigging
rigging.weight = 250 * units.kg
#
#
#
#
beam = lift_check.beam
beam.UR = 0.0  # current beam UR
beam.span_between_supports = 1.20 * units.m
#
material = beam.material
material.Fy = 345 * units.N/units.mm**2
material.E  = 200_000 * units.N/units.mm**2
material.G  = 79_300 * units.N/units.mm**2
material.density = 8_050 * units.kg/units.m**3
#
# set section
Ibeam = beam.Isection
Ibeam.d =  312 * units.mm
Ibeam.tw = 6.6 * units.mm
Ibeam.bf = 102 * units.mm
Ibeam.tf = 10.80 * units.mm
Ibeam.r = 10 * units.mm
#
load = lift_check.load
print('Load')
#print('wind lateral     = {: 1.3f} kN'
#      .format(load.wind_load.lateral.convert('kilonewton').value))
#print('wind horizontal  = {: 1.3f} kN'
#      .format(load.wind_load.horizontal.convert('kilonewton').value))
print('lift load vertical = {: 1.3f} kN'
      .format(load.lift_load.vertical.convert('kilonewton').value))
print('lift load lateral  = {: 1.3f} kN'
      .format(load.lift_load.lateral.convert('kilonewton').value))
#
#
beam.code_check()
beam.local_code_check()
print('Finish')
print('')

_beam_elements = beam.elements
_beam_stress = {}
_beam_results = {}
_beam_disp_node1 = {}
_beam_disp_node2 = {}
for key, _element in _beam_elements.items():
    _beam_results[key] = _element.code_check
    _beam_stress[key] = _element.stress
    _beam_disp_node1[key] = _element.node1
    _beam_disp_node2[key] = _element.node2

for x, key in enumerate(_beam_disp_node1.keys()):
    print('beam element :', key)
    _node1 = _beam_disp_node1[key]
    _node2 = _beam_disp_node2[key]
    print('Node end 1')
    print('ux = {:} mm'.format(_node1.ux.convert('millimetre').value))
    print('uy = {:} mm'.format(_node1.uy.convert('millimetre').value))
    print('uz = {:} mm'.format(_node1.uz.convert('millimetre').value))
    print('torsion = {:} rad'.format(_node1.torsion.convert('radian').value))
    print('Node end 2')
    print('ux = {:} mm'.format(_node2.ux.convert('millimetre').value))
    print('uy = {:} mm'.format(_node2.uy.convert('millimetre').value))
    print('uz = {:} mm'.format(_node2.uz.convert('millimetre').value))
    print('torsion = {:} rad'.format(_node2.torsion.convert('radian').value))
    print('')