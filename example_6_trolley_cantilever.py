#
import iLift as iLift
#
lift_check = iLift.Lifting_Tool()
units = lift_check.units

#
lift_check.cross_hauling_angle.transverse = 0 * units.deg
lift_check.cross_hauling_angle.longitudinal = 0 * units.deg
#
factors = lift_check.factors
factors.DAF.vertical = 1.08
factors.DAF.horizontal = 1.22
#
# vessel
vessel = lift_check.vessel
vessel.acceleration.horizontal = 0.05 * units.gravity
#
# Weight of lifting item
item = lift_check.item
item.weight = 1_000 * units.kg
item.height = 1.50 * units.m
item.length = 2.50 * units.m
#
wind = lift_check.wind
wind.velocity = 18.34 * units.m/units.s
wind.direction = 90 * units.degrees
#
load = lift_check.load
print('Load')
print('wind lateral     = {: 1.3f} kN'
      .format(load.wind_load.lateral.convert('kilonewton').value))
print('wind horizontal  = {: 1.3f} kN'
      .format(load.wind_load.horizontal.convert('kilonewton').value))
print('lift load vertical = {: 1.3f} kN'
      .format(load.lift_load.vertical.convert('kilonewton').value))
print('lift load lateral  = {: 1.3f} kN'
      .format(load.lift_load.lateral.convert('kilonewton').value))
#
#
#
beam = lift_check.beam
beam.UR = 0.80  # current beam UR
beam.span_cantilever = 1.5 * units.m
#
material = beam.material
material.Fy = 345 * units.N/units.mm**2
material.E  = 200_000 * units.N/units.mm**2
material.G  = 79_300 * units.N/units.mm**2
material.density = 8_050 * units.kg/units.m**3
#
# set section
Ibeam = beam.Isection
Ibeam.d = 459.74 * units.mm
Ibeam.tw = 9.91 * units.mm
Ibeam.bf = 191.262 * units.mm
Ibeam.tf = 16.00 * units.mm
Ibeam.r = 5 * units.mm
#
#
trolley = lift_check.trolley
trolley.Xw = 380 * units.mm
trolley.n = 5.0 * units.mm
#trolley.case = 'b' # see table 6.2 EN1993.6
#
#
beam.code_check()
beam.local_code_check()
print('Finish')
print('')

_beam_elements = beam.elements
_beam_stress = {}
_beam_results = {}
_beam_disp_node1 = {}
_beam_disp_node2 = {}
for key, _element in _beam_elements.items():
    _beam_results[key] = _element.code_check
    _beam_stress[key] = _element.stress
    _beam_disp_node1[key] = _element.node1
    _beam_disp_node2[key] = _element.node2

for x, key in enumerate(_beam_disp_node1.keys()):
    print('beam element :', key)
    _node1 = _beam_disp_node1[key]
    _node2 = _beam_disp_node2[key]
    print('Node end 1')
    print('ux = {:} mm'.format(_node1.ux.convert('millimetre').value))
    print('uy = {:} mm'.format(_node1.uy.convert('millimetre').value))
    print('uz = {:} mm'.format(_node1.uz.convert('millimetre').value))
    print('torsion = {:} rad'.format(_node1.torsion.convert('radian').value))
    print('Node end 2')
    print('ux = {:} mm'.format(_node2.ux.convert('millimetre').value))
    print('uy = {:} mm'.format(_node2.uy.convert('millimetre').value))
    print('uz = {:} mm'.format(_node2.uz.convert('millimetre').value))
    print('torsion = {:} rad'.format(_node2.torsion.convert('radian').value))
    print('')