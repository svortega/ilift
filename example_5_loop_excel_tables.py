#
from copy import copy
from math import isclose, copysign
#
import pandas as pd
import iLift as iLift
#
#
xl = pd.ExcelFile("Beam_Safe_Lift_Input_Data.xlsx")
df = xl.parse("SQL_Input")

# Add new columns
df["Local_Flange_IR"] = df["Global IR"]
df["Local_Web_Bottom_IR"] = df["Global IR"]
df["Local_Web_Top_IR"] = df["Global IR"]
#
#
for index, row in df.iterrows():
    # set object
    lifting = iLift.Lifting_Tool()
    units = lifting.units
    factors = lifting.factors
    wind = lifting.wind
    vessel = lifting.vessel
    #
    beam = lifting.beam
    material = beam.material
    Ibeam = beam.Isection
    #
    clamp = lifting.clamp
    item = lifting.item
    load = lifting.load    
    # print id
    section_name = row["Size"] +"_"+ row["Asset Type"]
    print('')
    print("Size: {:}, Length: {:}".format(section_name, row["Length"]))
    #
    # set lifting angles
    lifting.cross_hauling_angle.transverse = row["Perp Angle"] * units.deg
    lifting.cross_hauling_angle.longitudinal = row["Along Angle"] * units.deg
    #
    # set DAF factors
    factors.DAF.vertical = row["DAF Vertical"]
    factors.DAF.horizontal = row["DAF Horizontal"]
    #
    # set vessel factors
    vessel.acceleration.vertical = row["Inertia"] * units.gravity
    vessel.acceleration.horizontal = row["Inertia"] * units.gravity
    #
    # set wind data
    wind.velocity = row["Wind Speed"] * units.m / units.s
    wind.direction = row["Wind Direction"] * units.degrees
    #
    # set item to be lifted
    item.shape_factor = row["Shape Factor"]
    item.height = row["Equipment Height"] * units.m
    item.width = row["Equipment Width"] * units.m
    item.length = row["Equipment Length"] * units.m
    #
    # set beam 
    beam.span_between_supports = row["Length"] * units.m
    # set beam's material
    material.Fy = row["Fy"] * units.MPa
    material.Fu = row["Fu"] * units.MPa
    material.E = row["E"] * units.MPa
    material.G = row["G"] * units.MPa
    material.density = row["Density"] * units.kg / units.m**3
    # set beam's section
    Ibeam.d = row["d"] * units.mm
    Ibeam.tw = row["tw"] * units.mm
    Ibeam.bf = row["bf"]* units.mm
    Ibeam.tf = row["tf"] * units.mm
    Ibeam.r = row["r"] * units.mm
    #
    # add clamp-crane
    clamp.jaw_size = row["Clamp Jaw Size"] * units.mm
    clamp.weight = row["Clamp Weight"] * units.kg
    clamp.eccentricity_to_flangle = row["Clamp Eccentricity"] * units.mm
    clamp.load_split = row['Clamp Load Split'] / 100.0
    #
    # Find safe load
    _resulst = lifting.safe_lifting_load(allowable_UR = 1.0, 
                                         actual_UR = row["Global IR"])
    #
    # Update database
    df.at[index, "SafeLiftLoad_kg"] = _resulst.lift_safe_weight.convert('kilogram').value
    df.at[index, "Global IR"] = _resulst.UR_global
    df.at[index, "Local_Flange_IR"] = _resulst.UR_flange
    df.at[index, "Local_Web_Bottom_IR"] = _resulst.UR_web_bottom
    df.at[index, "Local_Web_Top_IR"] = _resulst.UR_web_top
    #print('--> End <--')    
    #
#
# print out update table
df.to_excel('Beam_Safe_Lift_results.xlsx')
print("Finish")