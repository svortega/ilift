#
import iLift as iLift
#
lifting = iLift.Lifting_Tool()
units = lifting.units
#
# Cross Haul
lifting.cross_hauling_angle.transverse = 15 * units.deg
lifting.cross_hauling_angle.longitudinal = 0 * units.deg
#
# DAF factors
factors = lifting.factors
factors.DAF.vertical = 1.10
#
# add beam section
beam = lifting.beam
beam.UR = 0.80  # current beam UR
# beam boundary condition setup
beam.span_between_supports = 4.66 * units.m
# add material
material = beam.material
material.Fy = 275 * units.MPa
material.Fu = 410 * units.MPa
material.E = 200_000 * units.MPa
material.G = 79_300 * units.MPa
material.density = 8050 * units.kg / units.m**3
# set section
Ibeam = beam.Isection
Ibeam.d = 254.1 * units.mm
Ibeam.tw = 6 * units.mm
Ibeam.bf = 146.1 * units.mm
Ibeam.tf = 8.6 * units.mm
Ibeam.r = 7.6 * units.mm
# add beam distributed load
beam_udl = beam.udl
_live = 5 * units.kN / units.m**2
_dead = 0.6 * units.kN / units.m**2 
_width_load = 875 * units.mm
qb = (_live + _dead) * _width_load
beam_udl.vertical = qb 
#
#
# add clamp-crane
clamp = lifting.clamp
clamp.jaw_size = 70 * units.mm
clamp.weight = 10 * units.kg
clamp.eccentricity_to_flangle = 100 * units.mm
clamp.load_split = 0.60
#
# add item to be lifted
door = lifting.item
door.weight = 340 * units.kg
#
# Weight of lifting equipment (assumed)
#rigging = lifting.rigging
#rigging.weight = 25.50 * units.kg
#
# print load summary
load = lifting.load
print('Load')
print('vertical = {: 1.3f} kN'
      .format(load.lift_load.vertical.convert('kilonewton').value))
print('lateral  = {: 1.3f} kN'
      .format(load.lift_load.lateral.convert('kilonewton').value))
#
beam.code_check()
beam.local_code_check()
#
# Results
#
#
# Adopted lengths
_flange_bottom = beam.flange[1].properties.bottom
_web_bottom = beam.web[1].properties.bottom
_web_top = beam.web[1].properties.top
#
print(_flange_bottom.Leff.convert('millimetre').value)
print(_web_bottom.Leff.convert('millimetre').value)
print(_web_top.Leff.convert('millimetre').value)
#
print(_flange_bottom.la.convert('millimetre').value)
print(_web_bottom.la.convert('millimetre').value)
print(_web_top.la.convert('millimetre').value)
#
#
print('')
_beam_elements = beam.elements
_beam_forces = {}
_beam_stress = {}
_beam_results = {}
_beam_disp_node1 = {}
_beam_disp_node2 = {}
for key, _element in _beam_elements.items():
    _beam_results[key] = _element.code_check
    _beam_forces[key] = _element.actions
    _beam_stress[key] = _element.stress
    _beam_disp_node1[key] = _element.node1
    _beam_disp_node2[key] = _element.node2

for x, key in enumerate(_beam_disp_node1.keys()):
    print('beam element :', key)
    _node1 = _beam_disp_node1[key]
    _node2 = _beam_disp_node2[key]
    print('Node end 1')
    print('ux = {:1.3f} mm'.format(_node1.ux.convert('millimetre').value))
    print('uy = {:1.3f} mm'.format(_node1.uy.convert('millimetre').value))
    print('uz = {:1.3f} mm'.format(_node1.uz.convert('millimetre').value))
    print('torsion = {:1.3e} rad'.format(_node1.torsion.convert('radian').value))
    print('Node end 2')
    print('ux = {:1.3f} mm'.format(_node2.ux.convert('millimetre').value))
    print('uy = {:1.3f} mm'.format(_node2.uy.convert('millimetre').value))
    print('uz = {:1.3f} mm'.format(_node2.uz.convert('millimetre').value))
    print('torsion = {:1.3e} rad'.format(_node2.torsion.convert('radian').value))
    print('')
#
# beam internal forces
for key, _results in _beam_forces.items():
    print('beam element :', key)
    print('Fx = {:1.3f} kN'.format(_results.Fx.convert('kilonewton').value))
    print('Fy = {:1.3f} kN'.format(_results.Fy.convert('kilonewton').value))
    print('Fz = {:1.3f} kN'.format(_results.Fz.convert('kilonewton').value))
    print('Mx = {:1.3f} kN-m'.format(_results.Mx.convert('kilonewton * metre').value))
    print('My = {:1.3f} kN-m'.format(_results.My.convert('kilonewton * metre').value))
    print('Mz = {:1.3f} kN-m'.format(_results.Mz.convert('kilonewton * metre').value))
#
print('Finish')