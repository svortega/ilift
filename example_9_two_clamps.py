
import iLift as iLift
#
#
lifting = iLift.Lifting_Tool()
units = lifting.units
# 
# Greater Plutonio - Removal and replacement of MP2 compressor 
# 
#set vertical lift angle
lifting.cross_hauling_angle.lateral = 30 * units.deg
lifting.cross_hauling_angle.longitudinal = 0 * units.deg
# 
# 3.1.- Lifting Factors  
factors = lifting.factors
factors.DAF.vertical = 1.10
#
# 3.2.- Lifting Item Weight Input
#
# Lifting weight 75% of the load applied in the middle
MP2 = lifting.item
MP2.weight = 10920 * units.kg
#
# Weight of lifting equipment
rigging = lifting.rigging
rigging.weight = 25.50 * units.kg
#
#
# 3.5.- Beam Section Properties
beam = lifting.beam
beam.span_between_supports = 4 * units.m
# set Beam Material
material = beam.material
material.Fy = 275 * units.MPa
material.Fu = 410 * units.MPa
material.E = 200_000 * units.MPa
material.G = 79_300 * units.MPa
material.density = 8050 * units.kg / units.m**3
# set section
Ibeam = beam.Isection
Ibeam.d = 1200 * units.mm
Ibeam.tw = 15 * units.mm
Ibeam.bf = 300 * units.mm
Ibeam.tf = 30 * units.mm
Ibeam.r = 10 * units.mm
# add beam distributed load
beam_udl = beam.udl
_live = 5 * units.kN / units.m**2
_dead = 0.6 * units.kN / units.m**2 
_width_load = 875 * units.mm
qb = (_live + _dead) * _width_load
beam_udl.vertical = qb 
#
# 3.6.- Horizontal crosshouling case 
# Techlock Clamp Geometry Data
#
clamp_1 = lifting.clamp
clamp_1.jaw_size = 70 * units.mm
clamp_1.weight = 110 * units.kg
clamp_1.eccentricity_to_flangle = 100 * units.mm
#clamp_1.distance_to_beam_end_1 = 0.5 * units.m
#
clamp_2 = lifting.clamp
clamp_2.jaw_size = 70 * units.mm
clamp_2.weight = 110 * units.kg
clamp_2.eccentricity_to_flangle = 100 * units.mm
#
# 3.7.- Print Report
#
beam.code_check()
beam.local_code_check()
#
print('-->')
print('')
_beam_elements = beam.elements
_beam_stress = {}
_beam_results = {}
_beam_disp_node1 = {}
_beam_disp_node2 = {}
for key, _element in _beam_elements.items():
    _beam_results[key] = _element.code_check
    _beam_stress[key] = _element.stress
    _beam_disp_node1[key] = _element.node1
    _beam_disp_node2[key] = _element.node2

for x, key in enumerate(_beam_disp_node1.keys()):
    print('beam element :', key)
    _node1 = _beam_disp_node1[key]
    _node2 = _beam_disp_node2[key]
    print('Node end 1')
    print('ux = {:1.3f} mm'.format(_node1.ux.convert('millimetre').value))
    print('uy = {:1.3f} mm'.format(_node1.uy.convert('millimetre').value))
    print('uz = {:1.3f} mm'.format(_node1.uz.convert('millimetre').value))
    print('torsion = {:1.3e} rad'.format(_node1.torsion.convert('radian').value))
    print('Node end 2')
    print('ux = {:1.3f} mm'.format(_node2.ux.convert('millimetre').value))
    print('uy = {:1.3f} mm'.format(_node2.uy.convert('millimetre').value))
    print('uz = {:1.3f} mm'.format(_node2.uz.convert('millimetre').value))
    print('torsion = {:1.3e} rad'.format(_node2.torsion.convert('radian').value))
    print('')

