#
import iLift as iLift
#
#
test = iLift.Lifting_Tool()
units = test.units
#
test.lift_angle = 0 * units.deg
# set factors
factors = test.factors
factors.DAF.vertical = 1.10
factors.DAF.horizontal = 1.22

factors.vessel.vertical = 1.30
#factors.vessel.horizontal = 0.05
factors.vessel.horizontal = 0.12

# set lifting item data
item_lifting = test.item
item_lifting.weight = 535 * units.kg
item_lifting.length = 1.5 * units.m
item_lifting.height = 1.5 * units.m
item_lifting.shape_factor = 1.0
#
# set wind
wind = test.wind
wind.speed = 18.5 * units.m/units.s
#
print('Load')
#print('wind     = {: 1.3f} kN'.format(test.wind.convert('kilonewton').value))
print('vertical = {: 1.3f} kN'.format(test.load.vertical.convert('kilonewton').value))
print('lateral  = {: 1.3f} kN'.format(test.load.lateral.convert('kilonewton').value))
#
#
clamp = test.clamp
clamp.sling_angle = 60 * units.deg
clamp.eccentricity_to_flangle = 122 * units.mm
clamp.jaw_size = 121 * units.mm
clamp.jaw_depth = 25 * units.mm
#
#
material = test.material
material.Fy = 248 * units.N/units.mm**2
material.E  = 200_000 * units.N/units.mm**2
material.G  = 79_300  * units.N/units.mm**2
#
#
beam = test.beam
beam.span_between_supports = 7.320 * units.m
# set section
Ibeam = beam.Isection
Ibeam.d = 602.7 * units.mm
Ibeam.tw = 10.50 * units.mm
Ibeam.bf = 228.0 * units.mm
Ibeam.tf = 14.68 * units.mm
Ibeam.r = 10 * units.mm
#
#
#
test.global_check()
test.local_check()
print('-->')