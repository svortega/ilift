print("Hello World!")

import pyodbc
import math
import pandas as pd
import iLift as iLift
from sqlalchemy import create_engine

#
# Build database connection string for pyodbc
server = 'zscugo0n18cimrsgsrv00.database.windows.net'
database = 'OFSiLiftPreProdDB'
username = 'SqlAdmin-ZSCUGO0N18CIMRSGsrv00'
password = 'OFScim_123'
driver= '{ODBC Driver 13 for SQL Server}'

sql_conn = pyodbc.connect('DRIVER='+driver+';SERVER='+server+';PORT=1433;DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = sql_conn.cursor()

#
# Build database connection string for sqlalchemy
DRIVER= 'ODBC Driver 13 for SQL Server'
conn = 'mssql+pyodbc://{uid}:{password}@{server}:1433/{database}?driver={driver}'.format(
uid=username,
password=password,
server=server,
database=database,
driver=DRIVER.replace(' ', '+'))

#
# Create SQL Alchemy engine and database connection
engine = create_engine(conn)
connection = engine.connect()
print("Database connection created")

#
# Delete existing safe lift data
connection.execute('DELETE FROM tblProjectBeamSafeLiftLoadsStagingAISCFloatingFY350IR08')
print("Existing data deleted")

#
# Retrieve input data from stored procedure call statement
sqlExecSP="{call spProjectBeamLiftScreeningTableAISCFloatingFY350IR08 ()}"

# Create panda dataframe (36 columns)
df = pd.read_sql(sqlExecSP, sql_conn)

# Data read column designations
# [0]   df['Region']                - BP Operating region 
# [1]   df['AssetType']             - Asset type, floating or fixed
# [2]   df['BeamCode']              - Beam code, AISC, UK or Euro
# [3]   df['BeamType']              - Beam code, AISC, UK or Euro
# [4]   df['BeamSize']              - Beam size designation e.g., 1016 UB 222
# [5]   df['d']                     - Beam depth, i.e., d                           mm
# [6]   df['tw']                    - Beam web thickness, i.e., tw                  mm
# [7]   df['bf']                    - Beam width, i.e., bf                          mm 
# [8]   df['tf']                    - Beam flange thickness, i.e., tf               mm
# [9]   df['r']                     - Beam r                                        mm
# [10]  df['Length']                - Beam length, 1 m to 10 m, increments of 1 m   m
# [11]  df['LateralDAF']            - Lift transverse DAF
# [12]  df['HorizontalDAF']         - Lift longitudinal DAF
# [13]  df['VerticalDAF']           - Lift vertical DAF
# [14]  df['LateralAngle']          - Lift angle lateral to beam                    deg
# [15]  df['HorizontalAngle']       - Lift angle horizontal to beam                 deg
# [16]  df['WindSpeed_ms']          - Lift wind speed                               m/s
# [17]  df['WindDirection']         - Lift wind direction                           deg
# [18]  df['BeamShapeFactor']       - Beam shape factor
# [19]  df['EquipmentShapeFactor']  - Equipment shape factor
# [20]  df['LateralAcceleration']   - Transeverse acceleration                      g            
# [21]  df['Horizontalacceleration']- Longitudinal acceleration                     g            
# [22]  df['Verticalacceleration']  - Vertical acceleration                         g
# [23]  df['VerticalLoadSplit']     - Clamp vertical load split                     %
# [24]  df['BeamFixity']            - Beam fixity, i.e., simply-supported
# [25]  df['BeamIR']                - Existing beam utilisation
# [26]  df['Fy']                    - Beam yield strength                           MPa 
# [27]  df['Fu']                    - Beam ultimat strength                         MPa 
# [28]  df['E']                     - Beam young's modulus                          MPa             
# [29]  df['G']                     - Beam                                          MPa  
# [30]  df['Density']               - Beam density                                  kg/m^3
# [31]  df['EquipmentHeight']       - Equipment height                              m
# [32]  df['EquipmentWidth']        - Equipment width                               m
# [33]  df['EquipmentLength']       - Equipment length                              m
# [34]  df['ClampJawSize']          - Lift clamp jaw size                           mm 
# [35]  df['ClampEccentricity']     - Lift clamp eccentricity                       mm

print("Data read")

print(df)

# Set beam flange widths
df['SectionWidthDec_mm'] = df['bf']
df['WidthRangeMin'] = df['bf'] * 0.975
df['WidthRangeMax'] = df['bf'] * 1.025

# Add new columns
df["GlobalIR"] = df['BeamIR']
df["LocalFlangeIR"] = df['BeamIR']
df["LocalWebBottomIR"] = df['BeamIR']
df["LocalWebTopIR"] = df['BeamIR']
df['SafeLiftLoad_kg'] = df['BeamIR']

# Rename dataframe column headers - array now has 43 columns (excluding index)
df.columns = ['Region',
              'AssetType',
              'BeamCode',
              'BeamType',
              'SectionSize',
              'd',
              'tw',
              'bf',
              'tf',
              'r',
              'Length_m',
              'LateralDAF',
              'HorizontalDAF',
              'VerticalDAF',
              'LateralAngle',
              'HorizontalAngle',
              'WindSpeed_ms',
              'WindDirection',
              'BeamShapeFactor',
              'EquipmentShapeFactor',
              'LateralAcceleration',
              'HorizontalAcceleration',
              'VerticalAcceleration',
              'VerticalLoadSplit',
              'BeamFixity',
              'BeamIR',
              'Fy_Mpa',
              'Fu_Mpa',
              'E_Mpa',
              'G_MPa',
              'Density_kgm3',
              'EquipmentHeight_m',
              'EquipmentWidth_m',
              'EquipmentLength_m', 
              'ClampJawSize_mm',
              'ClampEccentricity_mm', 
              'SectionWidthDec_mm',
              'SectionWidthRangeMin_mm',
              'SectionWidthRangeMax_mm',
              'GlobalIR',
              'LocalFlangeIR',
              'LocalWebBottomIR',
              'LocalWebTopIR',
              'SafeLiftLoad_kg']

print("Dataframe created")

#
for index, row in df.iterrows():
    # set object
    lifting = iLift.Lifting_Tool()
    units = lifting.units
    factors = lifting.factors
    vessel = lifting.vessel
    wind = lifting.wind
    #
    beam = lifting.beam
    material = beam.material
    Ibeam = beam.Isection
    #
    clamp = lifting.clamp
    item = lifting.item
    load = lifting.load    
    # print id
    section_name = row["SectionSize"] +"_"+ row["AssetType"]
    print('')
    print("Size: {:}, Length: {:}".format(section_name, row["Length_m"]))
    #
    # set lifting angles
    lifting.cross_hauling_angle.lateral = row["LateralAngle"] * units.deg
    lifting.cross_hauling_angle.horizontal = row["HorizontalAngle"] * units.deg
    #
    # set DAF factors
    factors.DAF.lateral = row["LateralDAF"]
    factors.DAF.horizontal = row["HorizontalDAF"]
    factors.DAF.vertical = row["VerticalDAF"]
    #
    # set equipment accelerations
    vessel.acceleration.lateral = row["LateralAcceleration"] * units.gravity
    vessel.acceleration.horizontal = row["HorizontalAcceleration"] * units.gravity
    vessel.acceleration.vertical = row["VerticalAcceleration"] * units.gravity
    #
    # set wind data
    wind.velocity = row["WindSpeed_ms"] * units.m / units.s
    wind.direction = row["WindDirection"] * units.degrees
    #
    # set item to be lifted
    # item.shape_factor = row["BeamShapeFactor"]
    item.shape_factor = row["EquipmentShapeFactor"]
    item.height = row["EquipmentHeight_m"] * units.m
    item.width = row["EquipmentWidth_m"] * units.m
    item.length = row["EquipmentLength_m"] * units.m
    #
    # set beam 
    beam.span_between_supports = row["Length_m"] * units.m
    #
    # set beam's material
    material.Fy = row["Fy_Mpa"] * units.MPa
    material.Fu = row["Fu_Mpa"] * units.MPa
    material.E = row["E_Mpa"] * units.MPa
    material.G = row["G_MPa"] * units.MPa
    material.density = row["Density_kgm3"] * units.kg / units.m**3
    #
    # set beam's section
    Ibeam.d = row["d"] * units.mm
    Ibeam.tw = row["tw"] * units.mm
    Ibeam.bf = row["bf"]* units.mm
    Ibeam.tf = row["tf"] * units.mm
    Ibeam.r = row["r"] * units.mm
    #
    # add clamp-crane
    clamp.jaw_size = row["ClampJawSize_mm"] * units.mm
    clamp.eccentricity_to_flangle = row["ClampEccentricity_mm"]* units.mm
    clamp.load_split = row['VerticalLoadSplit'] / 100.0
    #
    # Find safe load
    _resulst = lifting.safe_lifting_load(allowable_UR = 1.0, 
                                         actual_UR = row["BeamIR"])
    #
    # Update database
    df.at[index, "SafeLiftLoad_kg"] = _resulst.lift_safe_weight.convert('kilogram').value
    df.at[index, "GlobalIR"] = _resulst.UR_global
    df.at[index, "LocalFlangeIR"] = _resulst.UR_flange
    df.at[index, "LocalWebBottomIR"] = _resulst.UR_web_bottom
    df.at[index, "LocalWebTopIR"] = _resulst.UR_web_top
    #print('--> End <--')    
    #

#
print(df)

#
print("Safe lift load df complete")

#
# Write dataframe to SQL
df.to_sql('tblProjectBeamSafeLiftLoadsStagingAISCFloatingFY350IR08', engine, if_exists='append', index=True, index_label='SafeLiftID')

print("Complete")












